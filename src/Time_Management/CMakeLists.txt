##########################
# Time Management Module #
##########################

project(Time_Management)

# 1) Declare the module 
add_library(${PROJECT_NAME} STATIC
        src/Time_Management.cpp)

# 2) Declare header (include) dependencies (See note 1 from CMAKE_Notes.md)
target_include_directories(${PROJECT_NAME}
        PUBLIC
            ${CMAKE_CURRENT_SOURCE_DIR}/include
        PRIVATE
            ${CMAKE_CURRENT_SOURCE_DIR}/src)

# 3) Declare module's dependencies.
#    No dependencies

# 4) Install configuration:
install(TARGETS ${PROJECT_NAME}
        DESTINATION ${CMAKE_BINARY_DIR}/lib/${PROJECT_NAME})

install(DIRECTORY   ${CMAKE_CURRENT_SOURCE_DIR}/include/
        DESTINATION ${CMAKE_BINARY_DIR}/include/${PROJECT_NAME}
        FILES_MATCHING PATTERN "*.h*")

# 5) Subdirectories for testing and/or demos
#    No tests nor demos