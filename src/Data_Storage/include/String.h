/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                               DATA STORAGE                                 --
--                                                                            --
--                              String  Header                                --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#ifndef DATA_STORAGE_STRING_H
#define DATA_STORAGE_STRING_H


#include <cstring> // strlen
#include <cstdio>  // snprintf
#include <string_view>

namespace data_storage {
 
    /** 
     * String that allocates the array of characterss on the stack
     * and does not use dynamic memory. Similar to Ada's String.
     */
    template <size_t size_>
    class String {
        public:
            
            String () = default;

            String (const char *source) {
                move(source, size_);
            }
            
            // No explicit constructor/copy/destroy.

            const char& operator[] (size_t pos) const {
                return str_[pos];
            }

            char& operator[] (size_t pos) {
                return str_[pos];
            }

            size_t size() const {
                return size_ ? size_ : 1;
            }

            size_t length() const {
                return strlen(str_);
            }
            
            operator char * () {
                return str_;
            }

            operator std::string_view () {
                return std::string_view(str_);
            }
            
            const char* c_str() {
                return str_;
            }

        private:
            void move(const char * source, size_t size) {
                (void) snprintf(str_, size, "%s", source);
            }

            char str_ [size_ ? size_ : 1] {0};
    };

}

// References:
// [1] std::array implementation: https://gcc.gnu.org/onlinedocs/gcc-4.6.3/libstdc++/api/a00752_source.html

#endif // DATA_STORAGE_STRING_H
