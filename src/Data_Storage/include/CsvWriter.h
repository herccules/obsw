/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                               DATA STORAGE                                 --
--                                                                            --
--                             CSVWriter Header                               --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#ifndef DATA_STORAGE_CSV_WRITER_H
#define DATA_STORAGE_CSV_WRITER_H

#include <array>
#include <cstdint>
#include <fstream>
#include <string_view>

namespace data_storage {

    static const char *EMPTY_FIELD {""};    

    /**
     * Bit field that represents the status of the file
     * opened by CsvWriter.
     */
    struct FileStatus {
        // Set if the file was opened successfully
        uint8_t opened    : 1;

        // Set if the file was empty, it may have existed or not.
        uint8_t was_empty : 1;

        // Set if the file existed and did not end in new line.
        // So we had to add a new line.
        uint8_t add_ln    : 1;        
    };

    template <int COLUMN_LENGTH>
    class CsvWriter {
        public:            

            // -- Types  -------------------------------------------------------

            using Line = typename std::array <std::string_view, COLUMN_LENGTH>;
            // Typename Refs:
            // -   https://pages.cs.wisc.edu/~driscoll/typename.html
            // -   https://www.modernescpp.com/index.php/c-core-guidelines-template-interfaces

            // --  Constructors & Destructors  ---------------------------------            

            CsvWriter(const char *file_name, char separator, Line header)
                : separator_(separator)
            {               
                csv_.open(file_name, std::ios::out | std::ios::in | std::ios::app);
                
                if (csv_.is_open()) {
                    status_.opened = 1U;

                    analyze_file();

                    if (status_.add_ln == 1U) {
                        csv_.put('\n');
                    }

                    if (status_.was_empty == 1U) {
                        add_line(header);
                    }                    
                }                
            }

            ~CsvWriter() {
                csv_.close();
            }

            // --  Public operations  ------------------------------------------

            void add_line(Line line) {
                for (auto &col : line) {
                    csv_ << col << separator_;
                }
                csv_.put('\n');
            }

            FileStatus status() {
                return status_;
            }

            /**
             * Makes sure that any buffered data is written to disk,
             * the caller can still write into the file.
             */
            void save_file() {
                if (csv_.is_open()) {
                    csv_.flush();
                }
            }

        private:

            char separator_;
            std::fstream csv_;
            FileStatus status_ {};

            /**
             * Check if it is empty  AND  if needs a new line
             */
            void analyze_file() {
                auto begin  {csv_.tellg()};
                csv_.seekg(-1, std::ios::end); // read ptr: go the penultimate
                auto end    {csv_.tellg()};
                
                char last_char = csv_.peek();
                                
                bool is_empty      {(end - begin) < 0};
                bool needs_newline {!is_empty && last_char != '\n'};

                status_.add_ln    = needs_newline ? 1 : 0;
                status_.was_empty = is_empty ? 1 : 0;

                csv_.clear();
                csv_.seekp(0, std::ios::end);  // write ptr: go to last char
            }

    };

}

#endif // DATA_STORAGE_CSV_WRITER_H

// Notes:
// -----
//
// The following summarizes what happens if a file is opened in an specific mode
//
//    +--------------------+------------------------+------------------------+
//    | openmode           | If file already exists | If file does not exist |
//    +====================+========================+========================+
//    | in                 | Read from start        | Failure to open        |
//    +--------------------+------------------------+------------------------+
//    | out, out|trunc     | Destroy contents       | Create new             |
//    +--------------------+------------------------+------------------------+
//    | app, out|app       | Append to file         | Create new             |
//    +--------------------+------------------------+------------------------+
//    | in|out             | Read from start        | Error                  |
//    +--------------------+------------------------+------------------------+
//    | in|out|trunc       | Destroy contents       | Create new             |
//    +--------------------+------------------------+------------------------+
//    | in|out|app, in|app | Write to end           | Create new             |
//    +--------------------+------------------------+------------------------+ 
