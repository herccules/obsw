################################################################################
#                              H E R C C U L E S                               #
#            Copyright (C) 2022 Universidad Politécnica de Madrid              #
#                                                                              #
# This file have been developed by the real time systems group from UPM        #
################################################################################

##  Language standards  ########################################################

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_C_STANDARD 11)

set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_C_STANDARD_REQUIRED ON)

##  Compiler flags  ############################################################
set(CMAKE_COMMON_FLAGS "-Wall -Wextra -Wformat=2 -Wlogical-op -Wduplicated-cond -Wduplicated-branches -Wswitch-default -Wundef -Wcast-qual -Wparentheses -Wno-error=comment -pipe")
set(CMAKE_C_FLAGS      "${CMAKE_COMMON_FLAGS} -Wstrict-prototypes -Wwrite-strings")
set(CMAKE_CXX_FLAGS    "${CMAKE_COMMON_FLAGS}")

# Ref: https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html

set(CMAKE_C_FLAGS_DEBUG            "-g")
set(CMAKE_C_FLAGS_RELEASE          "-O3 -Werror -DNDEBUG")
set(CMAKE_C_FLAGS_MINSIZEREL       "-Os -Werror -DNDEBUG")
set(CMAKE_CXX_FLAGS_DEBUG          "${CMAKE_C_FLAGS_DEBUG}")
set(CMAKE_CXX_FLAGS_RELEASE        "${CMAKE_C_FLAGS_RELEASE}")
set(CMAKE_CXX_FLAGS_MINSIZEREL     "${CMAKE_C_FLAGS_MINSIZEREL}")
