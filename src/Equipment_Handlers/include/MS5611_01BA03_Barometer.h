/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                           EQUIPMENT  HANDLERS                              --
--                                                                            --
--                    MS5611_01BA03_Barometer  Header                         --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#ifndef HAL_MS5611_01BA03_BAROMETER_H
#define HAL_MS5611_01BA03_BAROMETER_H

#include "BusHandlers.h"
#include <array>
#include <exception>
#include <cstdint>

namespace equipment_handlers {

    class MS5611_01BA03_Barometer {
    public:

        static const uint8_t primaryI2CAddress   = 0b01110110U; // if CSB Enabled
        static const uint8_t secondaryI2CAddress = 0b01110111U; // if CSB Disabled

        struct Reading {
            // Calculated with second order temperature compensation (see datasheet fig. 3):
            float temperature_celsius;
            float pressure_milliBar;

            // Raw digital pressure and temperature data:
            /// AKA D1 from pg. 8
            int32_t temperature_raw;
            /// AKA D2 from pg. 8
            int32_t pressure_raw;
        };

        enum class OSR : uint8_t {
            OSR_256  = 0x00U << 1U,
            OSR_512  = 0x01U << 1U,
            OSR_1024 = 0x02U << 1U,
            OSR_2048 = 0x03U << 1U,
            OSR_4096 = 0x04U << 1U
        };

        explicit MS5611_01BA03_Barometer();

        ~MS5611_01BA03_Barometer();

        /// Initializes the pressure sensor with the passed configuration as input parameters
        /// \return A Boolean representing the result of the initialization
        bool initialize(bus_handlers::data::I2CBusID i2cBusID, uint8_t i2cAddress, OSR osr, bool enable2ndOrderCompensation = true);

        /// \return A Boolean representing the result of the reset operation
        bool reset();

        /// \param[out] data Read raw and compensated pressure and temperatures
        /// \return A Boolean representing the success (true) or failure (false) of the reading
        bool get(Reading &data);

    private:
        static const uint8_t RESET_REG_ADDR = 0x1EU;
        static const uint8_t ADC_READ_REG_ADDR = 0x00U;

        static const uint8_t CONVERT_D1_BASE_ADDR = 0x40U;
        static const uint8_t CONVERT_D2_BASE_ADDR = 0x50U;

        // Coefficients (C) registers addresses:
        static const uint8_t C1_REG_ADDR = 0xA2U;
        static const uint8_t C2_REG_ADDR = 0xA4U;
        static const uint8_t C3_REG_ADDR = 0xA6U;
        static const uint8_t C4_REG_ADDR = 0xA8U;
        static const uint8_t C5_REG_ADDR = 0xAAU;
        static const uint8_t C6_REG_ADDR = 0xACU;

        bus_handlers::data::I2CBusID m_i2cBusID;
        uint8_t m_i2cAddress;
        OSR m_osr;
        bool secondOrderCompensationEnabled = true;

        /// Serves as an index for C, table of coefficients
        enum CoefficientIndex : int32_t {
            _1 = 0, // SENS_T1: Pressure sensitivity
            _2 = 1, // OFF_T1: Pressure offset
            _3 = 2, // TCS: Temperature coefficient of SENS_T1
            _4 = 3, // TCO: Temperature coefficient of OFF_T1
            _5 = 4, // T_REF: Reference temperature
            _6 = 5, // TEMPSENS: Temperature coefficient of T_REF
        };
        /**
         * Contains the coefficients C. C.at(_1) or C[_1] means coefficient C1
         */
        std::array <int32_t , 6U> C;
        bool setupCoefficients();

        bool readDigitalPressure (int32_t &D1) const;

        bool readDigitalTemperature (int32_t &D2) const;

        void calculateTemperature
            (int32_t D2,
             int32_t &dT, int32_t &TEMP);

        void calculateTemperatureCompensatedPressure
            (int32_t D1, int32_t dT,
             int32_t &TEMP, int32_t &P);

        static void apply2ndOrderTemperatureCompensation
            (int32_t dT,
             int64_t &OFF, int64_t &SENS, int32_t &TEMP);

        class PROM_Lecture_Error : public std::exception { };
        uint16_t readPROM(uint8_t address) const;

        bool readADC(int32_t &raw_reading) const;
    };

}

#endif // HAL_MS5611_01BA03_BAROMETER_H