/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                           EQUIPMENT  HANDLERS                              --
--                                                                            --
--                                TC74  Header                                --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#ifndef HAL_TC74_H
#define HAL_TC74_H

#include "BusHandlers_Data.h"
#include <cstdint>  // uint8_t

namespace bh    = bus_handlers;
namespace bh_dt = bus_handlers::data;

namespace equipment_handlers {
    class TC74 {
    public:

        // ------------------------
        // Data types and constants
        // ------------------------

        static constexpr uint8_t TC74_DEFAULT_IIC_ADDR = 0x49U;

        /**
         * Represents the reading operational mode of the device, NOTE that their
         * values are equal to the expected config register for the desired mode.
         */
        enum Mode : uint8_t {
            Standby = 0b1000'0000U,
            Normal  = 0b0000'0000U
        };


        // ------------
        // Constructors
        // ------------

        TC74() = default;

        bool initialize(bh_dt::I2CBusID i2CBusId, uint8_t address,
                        Mode mode = Standby);


        // ------------
        // Manipulators
        // ------------

        bool setMode(Mode mode);


        // ---------
        // Accessors
        // ---------

        Mode getMode();

        bool isTheDataReady() const;

        bool readTemperature(int8_t &temperature);

        /**
         * This operation is dependant on the sensor operating mode.
         *
         * If the current mode is Stand-By,
         * the function waits actively the data availability for at most max_msecs_timeout.
         */
        bool readTemperatureBlocking(int8_t   &temperature,
                                     const int max_msecs_timeout = 250);

    private:
        bh_dt::I2CBusID m_i2cBusID;
        uint8_t         m_i2cAddress  = TC74_DEFAULT_IIC_ADDR;
        Mode            m_currentMode = Mode::Standby;

        // -----------------
        // Private accessors
        // -----------------

        uint8_t readConfigRegister() const;

        bool waitForTemperature(int max_msecs_timeout) const;
    };
}

#endif // HAL_TC74_H
