/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                           EQUIPMENT  HANDLERS                              --
--                                                                            --
--                      PiGPIO_Initialization  Source                         --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "pigpio_initialization.h"
#include <pigpio.h>
#include <iostream>

namespace {
    bool initialized = gpioInitialise() != PI_INIT_FAILED;
}

namespace equipment_handlers::pigpio_initialization {

    bool initialize()
    {
        if(!initialized) {
            std::cout << "[PiGPIO] Initializing the PiGPIO library";
            initialized = gpioInitialise() != PI_INIT_FAILED;
        } else {
            std::cout << "[PIGPIO] Successful initialization!" << std::endl;
        }

        return initialized;
    }
}
