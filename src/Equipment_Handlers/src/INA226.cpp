/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                           EQUIPMENT  HANDLERS                              --
--                                                                            --
--                              INA226  Source                                --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "INA226.h"
#include "BusHandlers.h"
#include <cmath>

namespace {

    // See the datasheet section 7.6 for further info:
    constexpr uint8_t CONFIGURATION_REG_ADDR = 0x00U;
    constexpr uint8_t SHUNT_VOLTAGE_REG_ADDR = 0x01U;
    constexpr uint8_t BUS_VOLTAGE_REG_ADDR   = 0x02U;
    constexpr uint8_t POWER_REG_ADDR         = 0x03U;
    constexpr uint8_t CURRENT_REG_ADDR       = 0x04U;
    constexpr uint8_t CALIBRATION_REG_ADDR   = 0x05U;
    constexpr uint8_t MASK_ENABLE_REG_ADDR   = 0x06U;
    constexpr uint8_t ALERT_LIMIT_REG_ADDR   = 0x07U;

    // Masks for the configuration register:
    constexpr uint16_t CONF_RESET_MASK   = 0x8000U;
    constexpr uint16_t CONF_AVG_MASK     = 0x0E00U;
    constexpr uint16_t CONF_BUS_VC_MASK  = 0x01C0U;
    constexpr uint16_t CONF_SHUNTVC_MASK = 0x0038U;
    constexpr uint16_t CONF_MODE_MASK    = 0x0007U;

    // Masks for the mask/enable register:
    constexpr uint16_t MASK_EN_CNVR_MASK = 0x0400U;
    constexpr uint16_t MASK_EN_CVRF_MASK = 0x0008U;
}

namespace equipment_handlers {

    INA226::INA226() {

    }

    bool INA226::initialize(bus_handlers::data::I2CBusID i2cBusID, uint8_t i2cAddress, OperatingMode mode) {
        m_i2cBusID   = i2cBusID;
        m_i2cAddress = i2cAddress;

        if (bus_handlers::initialize()) {
            setMode(mode);
            return true;
        }

        return false;
    }

    // Get readings:

    float INA226::getBusVoltage() {
        uint16_t raw {readRegister(BUS_VOLTAGE_REG_ADDR)};
        return static_cast<float>(raw) * 1.25E-3F;
    }

    float INA226::getShuntVoltage() {
        uint16_t raw {readRegister(SHUNT_VOLTAGE_REG_ADDR)};
        return static_cast<float>(raw) * 2.5E-6F;
    }

    float INA226::getCurrent() {
        uint16_t raw {readRegister(CURRENT_REG_ADDR)};
        return static_cast<float>(raw) * current_LSB;
    }

    float INA226::getPower() {
        uint16_t raw {readRegister(POWER_REG_ADDR)};
        return static_cast<float>(raw) * power_LSB;
    }

    void INA226::requestData() {
        if (m_mode == ShuntTrigger ||
            m_mode == BusTrigger   ||
            m_mode == ShuntBusTrigger)
        {
            setMode(m_mode);
        }
    }

    // -----------------
    // Commands & config
    // -----------------

    void INA226::reset() {
        uint16_t config {readRegister(CONFIGURATION_REG_ADDR)};
        config |= CONF_RESET_MASK;
        writeRegister(CONFIGURATION_REG_ADDR, config);

        current_LSB = 0.0F;
        power_LSB   = 0.0F;
    }

    // Conversion ready flag:

    void INA226::enableConversionReadyAlert() {
        auto config {readRegister(MASK_ENABLE_REG_ADDR)};
        config |= MASK_EN_CNVR_MASK;
        writeRegister(MASK_ENABLE_REG_ADDR, config);
    }

    bool INA226::conversionsReady() {
        auto config {readRegister(MASK_ENABLE_REG_ADDR)};
        return config & MASK_EN_CVRF_MASK;
    }


    // Averages:

    void INA226::setAverage(INA226::Average_Samples avg) {
        uint16_t mask = readRegister(CONFIGURATION_REG_ADDR);
        mask &= ~CONF_AVG_MASK;
        mask |= avg;
        writeRegister(CONFIGURATION_REG_ADDR, mask);
    }

    INA226::Average_Samples INA226::getAverage(){
        uint16_t mask = readRegister(CONFIGURATION_REG_ADDR);
        mask &= CONF_AVG_MASK;
        return static_cast<Average_Samples>(mask);
    }

    // Conversion times for Shunt and Bus voltages:

    void INA226::setBusVoltageConversionTime(INA226::Conversion_Time_ms bvct) {
        uint16_t mask = readRegister(CONFIGURATION_REG_ADDR);
        mask &= ~CONF_BUS_VC_MASK;
        mask |= (bvct << 6);
        writeRegister(CONFIGURATION_REG_ADDR, mask);
    }

    INA226::Conversion_Time_ms INA226::getBusVoltageConversionTime() {
        uint16_t mask = readRegister(CONFIGURATION_REG_ADDR);
        mask &= CONF_BUS_VC_MASK;
        mask >>= 6;
        return static_cast<INA226::Conversion_Time_ms>(mask);
    }

    void INA226::setShuntVoltageConversionTime(INA226::Conversion_Time_ms svct) {
        uint16_t mask = readRegister(CONFIGURATION_REG_ADDR);
        mask &= ~CONF_SHUNTVC_MASK;
        mask |= (svct << 3);
        writeRegister(CONFIGURATION_REG_ADDR, mask);
    }

    INA226::Conversion_Time_ms INA226::getShuntVoltageConversionTime() {
        uint16_t mask = readRegister(CONFIGURATION_REG_ADDR);
        mask &= CONF_SHUNTVC_MASK;
        mask >>= 3;
        return static_cast<Conversion_Time_ms>(mask);
    }

    /**
     *  Constants from the datasheet @ section 7.5:
     *  -   (1): 0.00512 is an internal fixed value used to ensure
     *           scaling is maintained properly
     *  -   (2): 2^15 = 32768
     */
    void INA226::setMaxCurrentShunt(float maxCurrent, float shunt) {
        current_LSB = maxCurrent / 32768.0F;
        power_LSB   = current_LSB * 25.0F;

        auto calib = static_cast<uint16_t>(round(0.00512F / (current_LSB * shunt))); // (1)
        writeRegister(CALIBRATION_REG_ADDR, calib);

        m_maxCurrent = current_LSB * 32768.0F; // (2) 2^15
        m_shunt      = shunt;
    }

    float INA226::getShunt() {
        return m_shunt;
    }

    float INA226::getMaxCurrent() {
        return m_maxCurrent;
    }

    bool INA226::isCalibrated() {
        return current_LSB != 0.0;
    }

    void INA226::setMode(OperatingMode mode) {
        uint16_t config = readRegister(CONFIGURATION_REG_ADDR);
        config &= ~CONF_MODE_MASK;
        config |= mode;
        writeRegister(CONFIGURATION_REG_ADDR, config);

        // if the write op fails, the mode might not be updated:
        m_mode = getMode();
    }

    INA226::OperatingMode INA226::getMode() {
        uint16_t mode = readRegister(CONFIGURATION_REG_ADDR);
        mode &= CONF_MODE_MASK;
        return static_cast<OperatingMode>(mode);
    }

    // Helpers:

    uint16_t INA226::readRegister(uint8_t reg) const {
        uint16_t value;
        (void) bus_handlers::readWordRegister(m_i2cBusID, m_i2cAddress,
                                              reg, value,
                                              bus_handlers::data::ByteOrdering::Big);
        return value;
    }

    void INA226::writeRegister(uint8_t reg, uint16_t value) const {
        (void) bus_handlers::writeWordRegister(m_i2cBusID, m_i2cAddress,
                                               reg, value,
                                               bus_handlers::data::ByteOrdering::Big);
    }

} // equipment_handlers
