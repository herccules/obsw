/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                           EQUIPMENT  HANDLERS                              --
--                                                                            --
--                      PiGPIO_Initialization  Header                         --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#ifndef OBSW_PIGPIO_INITIALIZATION_H
#define OBSW_PIGPIO_INITIALIZATION_H

/**
 * This module initializes the PiGPIO library automatically when it is imported
 * in another project.
 *
 * Additionally, it provides the initialize operation that initializes
 * the library if (by any reason) it could not be initialized when it was imported.
 *
 * The term imported means that the client module contains the following:
 * ```C++
 * #include "pigpio_management"
 * ```
 */
namespace equipment_handlers::pigpio_initialization {
    bool initialize();
}

#endif // OBSW_PIGPIO_INITIALIZATION_H
