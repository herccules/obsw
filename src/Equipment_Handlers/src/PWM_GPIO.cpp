/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                           EQUIPMENT  HANDLERS                              --
--                                                                            --
--                             PWM_GPIO Source                                --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "PWM_GPIO.h"

#include <pigpio.h>
#include <iostream>

namespace equipment_handlers {

    PWM_GPIO::PWM_GPIO() = default;

    PWM_GPIO::PWM_GPIO(unsigned int pin) : Switch::Switch(pin) {

    }

    bool PWM_GPIO::initialize(unsigned int pin) {
        bool success = Switch::initialize(pin);
        std::cout << "My pin is: " << Switch::m_pin;
        return success;
    }

    PWM_GPIO::~PWM_GPIO() {
        // destructed in base class.
    }

    bool PWM_GPIO::setPWMFrequency(unsigned int frequency) {
        m_pwmFreq   = gpioSetPWMfrequency(Switch::m_pin, frequency);
        std::cout << "PWM Freq:   " << m_pwmFreq << std::endl;
        return m_pwmFreq != PI_BAD_USER_GPIO;
    }

    bool PWM_GPIO::setDutyCycleRange(unsigned int range) {
        int rc = gpioSetPWMrange(Switch::m_pin, range);
        m_pwmRange     = static_cast<unsigned int>(gpioGetPWMrange(Switch::m_pin));
        m_pwmRealRange = static_cast<unsigned int>(gpioGetPWMrealRange(Switch::m_pin));

        std::cout << "Range:      " << m_pwmRange << std::endl;
        std::cout << "Real range: " << m_pwmRealRange << std::endl;

        return rc != PI_BAD_USER_GPIO && rc != PI_BAD_DUTYRANGE;
    }

    bool PWM_GPIO::setDutyCycle(float dutyCycle) {
        auto dutyCycleInRange = static_cast<unsigned int>(dutyCycle *  static_cast<float>(m_pwmRange) / 100.F);
        dutyCycleInRange = dutyCycleInRange > m_pwmRange ? m_pwmRange : dutyCycleInRange;
        std::cout << "Pin:                 " << m_pin << std::endl;
        std::cout << "Range:               " << m_pwmRange << std::endl;
        std::cout << "Duty Cycle in %:     " << dutyCycle << std::endl;
        std::cout << "Duty Cycle in range: " << dutyCycleInRange << std::endl;
        int rc = gpioPWM(Switch::m_pin, dutyCycleInRange);
        std::cout << (rc == PI_BAD_DUTYCYCLE ? "Bad Duty Cycle\n" : "");
        std::cout << (rc == PI_BAD_USER_GPIO ? "Bad User GPIO\n" : "");

        return rc == 0; // 0 = OK
    }

    float PWM_GPIO::dutyCycle() {
        auto dutyCycleInRange {gpioGetPWMdutycycle(Switch::m_pin)};
        return dutyCycleInRange * 100.F / static_cast<float>(m_pwmRange);
    }

}

// Notes:
// =====
//
// Sample rate
// -----------
// The PiGPIO library samples the GPIO pins at a rate set when the library is
// started. The numbers of samples/second is given bellow:
//
//                 samples
//                 per sec
//
//           1:  1,000,000
//           2:    500,000
//  sample   4:    250,000
//  rate     5:    200,000   <-- Default
//  (us)     8:    125,000
//          10:    100,000
//
// PWM Frequency
// -------------
// The selectable PWM frequencies depend upon the sample rate. The frequencies for
// each sample rate are given bellow:
//
//                                Hertz
//
//         1: 40000 20000 10000 8000 5000 4000 2500 2000 1600
//             1250  1000   800  500  400  250  200  100   50
//
//         2: 20000 10000  5000 4000 2500 2000 1250 1000  800
//              625   500   400  250  200  125  100   50   25
//
//         4: 10000  5000  2500 2000 1250 1000  625  500  400
//              313   250   200  125  100   63   50   25   13
//  sample
//   rate
//   (us)  5:  8000  4000  2000 1600 1000  800  500  400  320
//              250   200   160  100   80   50   40   20   10
//
//         8:  5000  2500  1250 1000  625  500  313  250  200
//              156   125   100   63   50   31   25   13    6
//
//        10:  4000  2000  1000  800  500  400  250  200  160
//              125   100    80   50   40   25   20   10    5
//
// PWM Real range
// --------------
// The real range, the number of steps between fully off and fully on for each
// PWM frequency, is given in the following table.
//
//               25    50   100  125  200  250  400   500   625
//              800  1000  1250 2000 2500 4000 5000 10000 20000
//
// E.g.:
// -----
//   A GPIO configured with
//      - Sampling rate = 4 microseconds
//      - PWM frequency = 500 Hz
//      - PWM cycle     = 2 milliseconds
//   would have 500 steps between fully on and fully off
//      Duty cycle [%]  : 100   75    50   25    0
//      Duty cycle [ms] : 2     1.5   1    0.5   0
