#include <iostream>
#include "INA226.h"

namespace {
    equipment_handlers::INA226 ina;
}

int main () {
    int casos = 0;

    while(true){
        switch(casos) {
            case 1: {
                bool ok = ina.initialize(bus_handlers::data::I2CBusID::BUS1);
                std::cout << (ok ? "success" : "failed") << " initialization" << std::endl;
                break;
            }

            case 2: {
                ina.setMode(equipment_handlers::INA226::ShuntBusTrigger);
                ina.setBusVoltageConversionTime(equipment_handlers::INA226::CONV_TIME_4156);
                ina.setShuntVoltageConversionTime(equipment_handlers::INA226::CONV_TIME_4156);
                ina.setAverage(equipment_handlers::INA226::AVERAGE_512);
                break;
            }

            case 3: {
                ina.setMaxCurrentShunt(2.048F, 0.0399F);
                std::cout << "Max current = " << ina.getMaxCurrent() << "\n"
                          << "Shunt value = " << ina.getShunt() << std::endl;
                break;
            }

            case 4: {
                ina.requestData();
                std::cout << "Shunt voltage = " << ina.getShuntVoltage() << " V\n"
                          << "Bus voltage   = " << ina.getBusVoltage()   << " V\n"
                          << "Current = " << ina.getCurrent() << " A\n"
                          << "Power   = " << ina.getPower() << " W\n";
                break;
            }

            case 5: {
                ina.reset();
                std::cout << "After reset\n";
                break;
            }

            case 6: {
                std::cout << "CLOSED\n";
                return 0;
            }

            default : {
                printf("\nIntroduzca una opcion:\n"
                       "1. Inicializar el bus.\n"
                       "2. Establecer configuracion.\n"
                       "3. Calibrar R_Shunt & Max_Current\n"
                       "4. Leer datos.\n"
                       "5. Reiniciar dispositivo.\n"
                       "6. Salir\n\n");
            }
        }

        casos = 0;
        if (scanf("%d", &casos)) {perror("scanf");}
    }
}