#include "MS5611_01BA03_Barometer.h"
#include "iostream"
#include "unistd.h"

namespace eh = equipment_handlers;
namespace bh_dt = bus_handlers::data;

int main() {

    eh::MS5611_01BA03_Barometer barometer;
    bool initialized = barometer.initialize(bh_dt::I2CBusID::BUS0,
                                            0x77U,
                                            eh::MS5611_01BA03_Barometer::OSR::OSR_256,
                                            true);
    if (initialized) {
        eh::MS5611_01BA03_Barometer::Reading r {};
        while (true) {
            bool ok = barometer.get(r);
            if (ok) {
                std::cout << "T  ºC: " << r.temperature_celsius << ". P mBar: " << r.pressure_milliBar << std::endl;
                std::cout << "T raw: " << r.temperature_raw << ". P raw: " << r.pressure_raw << std::endl <<
                std::endl;
            } else {
                std::cout << "[BAROMETER DEMO] Not available" << std::endl;
            }

            (void) sleep(1U);
        }
    } else {
        std::cerr << "[BAROMETER DEMO] Not initialized!" << std::endl;
    }

    return 0;
}