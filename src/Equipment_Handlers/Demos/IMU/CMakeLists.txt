set(BINARY_NAME IMUDemo)
# 1st param: executable
# 2nd param: sources need to generate the executable
add_executable(${BINARY_NAME} "IMUDemo.cpp")

target_link_libraries(${BINARY_NAME} LINK_PRIVATE Equipment_Handlers)
target_link_libraries(${BINARY_NAME} LINK_PRIVATE Bus_Handlers)
target_link_libraries(${BINARY_NAME} LINK_PRIVATE pthread)

install(TARGETS ${BINARY_NAME} DESTINATION ${CMAKE_BINARY_DIR}/bin/${PROJECT_NAME})
