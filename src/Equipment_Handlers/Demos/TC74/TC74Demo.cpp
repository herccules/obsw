//////////////////////////////////////////////////////////////////////////////
//                                                                          //
//          Copyright (C) 2022 Universidad Politécnica de Madrid            //
//                                                                          //
// This is free software;  you can redistribute it  and/or modify it  under //
// terms of the  GNU General Public License as published  by the Free Soft- //
// ware  Foundation;  either version 3,  or (at your option) any later ver- //
// sion.  This software is distributed in the hope  that it will be useful, //
// but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- //
// TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public //
// License for  more details.  You should have  received  a copy of the GNU //
// General  Public  License  distributed  with  this  software;   see  file //
// COPYING3.  If not, go to http://www.gnu.org/licenses for a complete copy //
// of the license.                                                          //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

#include "TC74.h"
#include <iostream>

using equipment_handlers::TC74;
using std::cout;

namespace {
    TC74 tc74;

    int setup() {
        bool initialized = tc74.initialize(bh_dt::I2CBusID::BUS1, 0x49U);
        return initialized ? 0 : 1;
    }

    int userOption() {
        cout << "\nIntroduzca una opcion:"
                "\n  1. Establecer modo de configuracion."
                "\n  2. ver estado del TC74."
                "\n  3. Leer temperatura."
                "\n  4. Salir.\n\n" << std::endl;
        int option = 0;
        if (scanf("%d", &option)) {perror("scanf");}
        return option;
    }

    bool console() {
        switch(userOption()){
            case 1: {
                // 1) Get sub-option:
                cout << "Choose a config mode: 1->NORMAL, 2->STANDBY" << std::endl;
                int option = 0;
                if (scanf("%d", &option)) {perror("scanf");}

                // 2) Print value:
                TC74::Mode mode = (option == 1) ? TC74::Mode::Normal : TC74::Mode::Standby;
                cout << (tc74.setMode(mode) ? "Escrito correctamente.\n" : "Error de escritura.");
            } break;

            case 2 : {
                auto mode = tc74.getMode() == TC74::Mode::Normal ? "Normal" : "Standby";
                cout << "The TC74 mode is = " << mode << std::endl;
            } break;

            case 3 : {
                int8_t temperature = 0;
                bool success = tc74.readTemperature(temperature);
                if (success) {
                    cout << "Temperature is = " << static_cast<int16_t>(temperature) << " ºC" << std::endl;
                } else {
                    cout << "Could not read temperature" << std::endl;
                }
            } break;

            case 4  :
            default :
                return false;
        }
        return true;
    }
}

int main() {
    int rc = setup();
    if (rc != 0) {
        std::cerr << "rc != 0" << std::endl;
    }

    while (true) {
        // Forever
        console();
    }

    return 0;
}