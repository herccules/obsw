/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                             NADS_Manager State                            --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

// Purpose:
// --------
// -   This class gathers the NADS::Manager context data (internal state).
// -   All the global variables are independent per function instance.

#include "dataview-uniq.h"
#include "Context-nads-manager.h"
#include "CsvWriter.h"

class nads_manager_state {
public:
    uint8_t cycle {1U};

    bool end_of_mission {false};

    asn1SccNADS_Data nads_data;

    using Logfile_type = data_storage::CsvWriter<28U>;

    Logfile_type::Line header {
        // == Timestamps & modes ==
        "Snapshot time [secs]" ,
        "Snapshot time [usecs]",
        "Mission time [secs]",
        "EL Mode",

        // == IMU data - raw sensors ==
        "Accel-X", "Accel-Y", "Accel-Z",
        "Gyro-X" , "Gyro-Y" , "Gyro-Z" ,
        "Mgm-X"  ,  "Mgm-Y" , "Mgm-Z"  ,

        // == IMU data - raw sensors ==
        "Euler orient-X", "Euler orient-Y", "Euler orient-Z",
        "Linear accel-X", "Linear accel-Y", "Linear accel-Z",
        "Gravity-X"     , "Gravity-Y"     , "Gravity-Z"     ,
        "Quat-W"        , "Quat-X"        , "Quat-Y"        , "Quat-Z",

        "Temperature Accel", "Temperature Gyro"
    };

    Logfile_type logfile {nads_manager_ctxt.logfile_name, ';', header};

    static const char * image(const asn1SccNADS_Mode x) {
        switch (x) {
            case asn1SccNADS_Mode_on:
                return "NADS-ON";
            case asn1SccNADS_Mode_off:
                return "NADS-OFF";
        }
        return "Mode-Error";
    }
};
