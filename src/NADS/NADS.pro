TEMPLATE = lib
CONFIG -= qt
CONFIG += generateC

DISTFILES +=  $(HOME)/tool-inst/share/taste-types/taste-types.asn
DISTFILES += NADS.msc
DISTFILES += interfaceview.xml
DISTFILES += work/binaries/*.msc
DISTFILES += work/binaries/coverage/index.html
DISTFILES += work/binaries/filters
DISTFILES += work/system.asn

DISTFILES += /home/taste/Repositories/herccules/obsw/scripts/../src/tmtc-interface/Data_Types/src/DataTypes.acn
DISTFILES += /home/taste/Repositories/herccules/obsw/scripts/../src/tmtc-interface/Data_Types/src/DataTypes.asn
DISTFILES += /home/taste/Repositories/herccules/obsw/scripts/../src/tmtc-interface/Data_Types/src/DataTypes-Events.acn
DISTFILES += /home/taste/Repositories/herccules/obsw/scripts/../src/tmtc-interface/Data_Types/src/DataTypes-Events.asn
DISTFILES += /home/taste/Repositories/herccules/obsw/scripts/../src/tmtc-interface/Data_Types/src/DataTypes-OperatingModes.acn
DISTFILES += /home/taste/Repositories/herccules/obsw/scripts/../src/tmtc-interface/Data_Types/src/DataTypes-OperatingModes.asn
DISTFILES += /home/taste/Repositories/herccules/obsw/scripts/../src/tmtc-interface/Data_Types/src/DataTypes-Subsystems.asn
DISTFILES += /home/taste/Repositories/herccules/obsw/scripts/../src/tmtc-interface/Data_Types/src/DataTypes-Subsystems.acn
DISTFILES += /home/taste/Repositories/herccules/obsw/scripts/../src/tmtc-interface/Data_Types/src/DataTypes-Telecommands.acn
DISTFILES += /home/taste/Repositories/herccules/obsw/scripts/../src/tmtc-interface/Data_Types/src/DataTypes-Telecommands.asn
DISTFILES += /home/taste/Repositories/herccules/obsw/scripts/../src/tmtc-interface/Data_Types/src/DataTypes-Telemetries.acn
DISTFILES += /home/taste/Repositories/herccules/obsw/scripts/../src/tmtc-interface/Data_Types/src/DataTypes-Telemetries.asn
DISTFILES += deploymentview.dv.xml
DISTFILES += deploymentview_distributed.dv.xml
DISTFILES += NADS.asn
DISTFILES += NADS.acn
include(work/taste.pro)
message($$DISTFILES)

