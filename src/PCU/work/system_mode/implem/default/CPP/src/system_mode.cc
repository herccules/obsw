/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                           PCU_System_Mode Source                           --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "system_mode.h"
#include "system_mode_state.h"

namespace  {
    system_mode_state ctxt;
}

void system_mode_startup(void)
{
    ctxt.current_mode = asn1SccBalloon_Mode_ground_await;
}

void system_mode_PI_Get_Mode
    (asn1SccBalloon_Mode *OUT_system_mode)
{
    *OUT_system_mode = ctxt.current_mode;
}


void system_mode_PI_Notify_Mode_Change_PCU
    (const asn1SccBalloon_Mode *IN_new_mode)
{
    ctxt.current_mode = *IN_new_mode;
}


