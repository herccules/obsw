--  This file was generated automatically - do not modify it manually
--  If you wish to modify the template used to create this file, it is located
--  in ~/tool-src/misc/space-creator/dv2aadl/deploymentview.tmplt
--  After modifications, install it by running ~/tool-src/install/90_misc
--  Template written by Maxime Perrotin (maxime.perrotin@esa.int) 2021-09
package deploymentview::DV::OBC
public

 with Deployment;
 with TASTE;
 with TASTE_DV_Properties;

 WITH ocarina_buses;
 WITH ocarina_drivers;

 --  Declare partition(s) of node "OBC" (aka "Raspberry_PI_Linux_POHIC_1").
 process Partition_1
 end Partition_1;

 process implementation Partition_1.others
 end Partition_1.others;

    DEVICE generic_sockets_ip_pohic
    EXTENDS ocarina_drivers::generic_sockets_ip
    FEATURES
      link : REFINED TO REQUIRES BUS ACCESS ocarina_buses::ip.i;
    PROPERTIES
      Deployment::Help => "Write your ASN.1 configuration here";
      Deployment::Configuration => "{devname ""wlan0"", address ""172.20.10.3"", port 5489}";
      Deployment::Config => "/home/taste/tool-inst/include/ocarina/runtime/polyorb-hi-c/src/drivers/configuration/ip.asn";
      Deployment::Version => "0.1beta";
    END generic_sockets_ip_pohic;

    DEVICE IMPLEMENTATION generic_sockets_ip_pohic.others
    EXTENDS ocarina_drivers::generic_sockets_ip.pohic
    END generic_sockets_ip_pohic.others;

end deploymentview::DV::OBC;
--  This file was generated automatically - do not modify it manually
--  If you wish to modify the template used to create this file, it is located
--  in ~/tool-src/misc/space-creator/dv2aadl/deploymentview.tmplt
--  After modifications, install it by running ~/tool-src/install/90_misc
--  Template written by Maxime Perrotin (maxime.perrotin@esa.int) 2021-09
package deploymentview::DV::GS
public

 with Deployment;
 with TASTE;
 with TASTE_DV_Properties;

 WITH ocarina_buses;
 WITH ocarina_drivers;


 --  Declare partition(s) of node "GS" (aka "x86_Linux_POHIC_1").
 process Partition_2
 end Partition_2;

 process implementation Partition_2.others
 end Partition_2.others;

    DEVICE generic_sockets_ip_pohic
    EXTENDS ocarina_drivers::generic_sockets_ip
    FEATURES
      link : REFINED TO REQUIRES BUS ACCESS ocarina_buses::ip.i;
    PROPERTIES
      Taste::Interface_Coordinates => "152359 183988" APPLIES TO link;
      Deployment::Help => "Write your ASN.1 configuration here";
      Deployment::Configuration => "{devname ""enp0s3"", address ""172.20.10.4"", port 5488}";
      Deployment::Config => "/home/taste/tool-inst/include/ocarina/runtime/polyorb-hi-c/src/drivers/configuration/ip.asn";
      Deployment::Version => "0.1beta";
    END generic_sockets_ip_pohic;

    DEVICE IMPLEMENTATION generic_sockets_ip_pohic.others
    EXTENDS ocarina_drivers::generic_sockets_ip.pohic
    END generic_sockets_ip_pohic.others;


end deploymentview::DV::GS;

package deploymentview::DV
public

 with TASTE;
 with Deployment;
 with Interfaceview::IV;
 with TASTE_DV_Properties;

 --  Dependencies of node OBC
 with interfaceview::IV::Tester;
 with interfaceview::IV::PCU::Power_Supply_Lines;
 with interfaceview::IV::PCU::PCU_Manager;
 with interfaceview::IV::PCU::System_Mode;
 with deploymentview::DV::OBC;
 with ocarina_processors_arm;
 --  Dependencies of node GS
 with interfaceview::IV::Tester_Interface;
 with deploymentview::DV::GS;
 with ocarina_processors_x86;

 WITH ocarina_buses;
 WITH ocarina_drivers;

 --  Node OBC
 system OBC
 FEATURES
   generic_sockets_ip_pohic_ip_i : REQUIRES BUS ACCESS ocarina_buses::ip.i;
 end OBC;

 system implementation OBC.others
 subcomponents
    IV_Tester : system Interfaceview::IV::Tester::Tester.others {
       Taste::ImplementationName => "";
    };
    IV_Power_Supply_Lines : system Interfaceview::IV::PCU::Power_Supply_Lines::Power_Supply_Lines.others {
       Taste::ImplementationName => "";
    };
    IV_PCU_Manager : system Interfaceview::IV::PCU::PCU_Manager::PCU_Manager.others {
       Taste::ImplementationName => "";
    };
    IV_System_Mode : system Interfaceview::IV::PCU::System_Mode::System_Mode.others {
       Taste::ImplementationName => "";
    };
    Partition_1 : process deploymentview::DV::OBC::Partition_1.others { 
       TASTE_DV_Properties::CoverageEnabled => false;
       Deployment::Port_Number => 0;
    };
    p1 : processor ocarina_processors_arm::rpi.posix;

    generic_sockets_ip_pohic : DEVICE deploymentview::DV::OBC::generic_sockets_ip_pohic.others;
CONNECTIONS
    generic_sockets_ip_pohic_ip_i_link : BUS ACCESS generic_sockets_ip_pohic_ip_i -> generic_sockets_ip_pohic.link;
 properties
    TASTE::APLC_Binding => (reference (Partition_1)) applies to IV_Tester;
    TASTE::APLC_Binding => (reference (Partition_1)) applies to IV_Power_Supply_Lines;
    TASTE::APLC_Binding => (reference (Partition_1)) applies to IV_PCU_Manager;
    TASTE::APLC_Binding => (reference (Partition_1)) applies to IV_System_Mode;
    Actual_Processor_Binding => (reference (p1)) applies to Partition_1;
    Actual_Processor_Binding => (reference (p1)) APPLIES TO generic_sockets_ip_pohic;
 end OBC.others;

 --  Node GS
 system GS
 FEATURES
   generic_sockets_ip_pohic_ip_i : REQUIRES BUS ACCESS ocarina_buses::ip.i;
 end GS;

 system implementation GS.others
 subcomponents
    IV_Tester_Interface : system Interfaceview::IV::Tester_Interface::Tester_Interface.others {
       Taste::ImplementationName => "";
    };
    Partition_2 : process deploymentview::DV::GS::Partition_2.others { 
       TASTE_DV_Properties::CoverageEnabled => false;
    };
    p1 : processor ocarina_processors_x86::x86.linux;
    generic_sockets_ip_pohic : DEVICE deploymentview::DV::GS::generic_sockets_ip_pohic.others;
CONNECTIONS
   generic_sockets_ip_pohic_ip_i_link : BUS ACCESS generic_sockets_ip_pohic_ip_i -> generic_sockets_ip_pohic.link;

 properties
    TASTE::APLC_Binding => (reference (Partition_2)) applies to IV_Tester_Interface;
    Actual_Processor_Binding => (reference (p1)) applies to Partition_2;
    Actual_Processor_Binding => (reference (p1)) APPLIES TO generic_sockets_ip_pohic;
 end GS.others;


system deploymentview
end deploymentview;

system implementation deploymentview.others
subcomponents
   interfaceview : system interfaceview::IV::interfaceview.others;
   OBC : system OBC.others;
   GS : system GS.others;
   ip_i : BUS ocarina_buses::ip.i;

CONNECTIONS
  Connection1 : BUS ACCESS ip_i -> OBC.generic_sockets_ip_pohic_ip_i;
  Connection2 : BUS ACCESS ip_i -> GS.generic_sockets_ip_pohic_ip_i;
PROPERTIES
  Actual_Connection_Binding => (reference (ip_i)) APPLIES TO interfaceview.Tester_Interface_RI_Deactivate_PSL_Tester_PI_Deactivate_PSL;
  Actual_Connection_Binding => (reference (ip_i)) APPLIES TO interfaceview.Tester_Interface_RI_Activate_PSL_Tester_PI_Activate_PSL;
  Actual_Connection_Binding => (reference (ip_i)) APPLIES TO interfaceview.Tester_Interface_RI_Notify_Mode_Change_Tester_PI_Notify_Mode_Change;
  Actual_Connection_Binding => (reference (ip_i)) APPLIES TO interfaceview.Tester_RI_Update_PCU_Data_Async_Tester_Interface_PI_Update_PCU_Data_Async;
  Actual_Connection_Binding => (reference (ip_i)) APPLIES TO interfaceview.PCU_RI_Notify_Event_Tester_Interface_PI_Notify_Event;

end deploymentview.others;

end deploymentview::DV;
