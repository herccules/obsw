/* C++ Function Type
 * Header file for function System_Mode_2 in CPP language
 * Generated by TASTE (kazoo/templates/skeletons/cpp-header-type/function.tmplt)
 * Provided interfaces : Get_Mode, Set_Mode
 * Required interfaces : 
 * DO NOT EDIT THIS FILE, IT WILL BE OVERWRITTEN DURING THE BUILD
 */

#pragma once

#include "dataview-uniq.h"
#include "system_mode_state.h"

class system_mode {
   // These are the required interfaces you can call from application code:
   

   // the ctxt member allows you to define your per-instance global data
   system_mode_state ctxt;


public:
   system_mode() {}
   void startup();
   /* Provided interfaces */
   void Get_Mode( asn1SccBalloon_Mode * );
   
   
   void Set_Mode( const asn1SccBalloon_Mode * );
};
