// C+++ body file for function NVMem
// Generated by TASTE (kazoo/templates/skeletons/cpp-body/function.tmplt)
// You can edit this file, it will not be overwritten

#include "nvmem.h"
#include "nvmem_state.h"
//#include <iostream>

// Define and use function state inside this context structure
// avoid defining global/static variable elsewhere
nvmem_state ctxt_nvmem;


void nvmem_startup(void)
{
   // Write your initialisation code
   // You may call sporadic interfaces and start timers
   // std::cout << "[NVMem] Startup" << std::endl;
}

void nvmem_PI_Retreive_Context
      (const asn1SccBalloon_Mode *IN_system_mode,
       const asn1SccRelative_Time_Type *IN_time_mission,
       const asn1SccAbsolute_Time_Type *IN_time_absolute)

{
   // Write your code here
}


void nvmem_PI_Save_Context
      (const asn1SccBalloon_Mode *IN_system_mode,
       const asn1SccRelative_Time_Type *IN_time_mission,
       const asn1SccAbsolute_Time_Type *IN_time_absolute)

{
   // Write your code here
}


