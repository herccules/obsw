// Fill in this class with your context data (internal state):
// list all the variables you want global (per function instance)
#include "dataview-uniq.h"

class sdpu_manager_state {
public:
    asn1SccBoolean_Type EL_needs_measurements  {false};
    asn1SccBoolean_Type ATL_needs_measurements {false};
};
