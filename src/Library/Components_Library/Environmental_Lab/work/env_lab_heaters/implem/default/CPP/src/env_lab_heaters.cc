/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                           Env_Lab_Heaters Source                           --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "env_lab_heaters.h"
#include "env_lab_heaters_state.h"

#include "PCU.h" // Hardware I/F for PCU

/*******************************************************************************
 * Private variables and constants:
 ******************************************************************************/

namespace {
    env_lab_heaters_state ctxt;
}

/*******************************************************************************
 * Private functions:
 ******************************************************************************/

namespace {
    void control_heater(const asn1SccEnvLab_Heater_ID  IN_heater_id,
                           const asn1SccHeater_Power_Type IN_power)
    {
        auto heater_ID = IN_heater_id == EnvLab_Heater_ID_upwards_heater
                       ? board_support::pcu::UpwardsELHeater
                       : board_support::pcu::DownwardsELHeater;
        auto pwm {IN_power > 0.0F ? 50.0F : 0.0F}; // TODO: transform POWER to PWM duty cycle:
        board_support::pcu::setEnvLabPWMDutyCycle(heater_ID, pwm);
    }
}

/*******************************************************************************
 * Public functions:
 ******************************************************************************/

void env_lab_heaters_startup(void)
{
    board_support::pcu::initialize_switches();
}

void env_lab_heaters_PI_Control_Heater_Auto
    (const asn1SccEnvLab_Heater_ID *IN_heater_id,
     const asn1SccHeater_Power_Type *IN_power)
{
    if (ctxt.heaters_mode == asn1SccActuator_Control_Mode_autonomous_control) {
        control_heater(*IN_heater_id, *IN_power);
    }
}

void env_lab_heaters_PI_Control_Heater_Manual
    (const asn1SccEnvLab_Heater_ID  *IN_heater_id,
     const asn1SccHeater_Power_Type *IN_power)
{
    if (ctxt.heaters_mode == asn1SccActuator_Control_Mode_manual_control) {
        control_heater(*IN_heater_id, *IN_power);
    }
}

void env_lab_heaters_PI_Get_Heaters_Status
    (asn1SccEnv_Lab_Heaters_Mode *OUT_heaters_mode,
     asn1SccHeater_Power_Type    *OUT_heater_uel,
     asn1SccHeater_Power_Type    *OUT_heater_del)
{
    float uel, del;
    board_support::pcu::getEnvLabPWMDutyCycle(uel, del);

    *OUT_heater_uel = uel;
    *OUT_heater_del = del;
    *OUT_heaters_mode = ctxt.heaters_mode;
}


void env_lab_heaters_PI_Set_Heaters_Mode
    (const asn1SccEnv_Lab_Heaters_Mode *IN_new_heaters_mode)
{
    ctxt.heaters_mode = *IN_new_heaters_mode;
}
