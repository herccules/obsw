/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                           Env_Lab_Manager State                            --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "dataview-uniq.h"

#include "Context-env-lab-manager.h" // Context parameters: env_lab_manager_ctxt
#include "CsvWriter.h"

/**
 * @brief Contains the context data (internal state) of this TASTE-function
 *
 * There is a set of global variables per TASTE-function instance
 */
class env_lab_manager_state {
public:

    /***************************************************************************
     * Context Operations:
     **************************************************************************/

    constexpr int16_t celsius_to_raw(float celsius) {
        constexpr float m { 34.18F};
        constexpr float b {-80.24F};
        float volts = (static_cast<float>(celsius) - b) / m;
        float raw = volts / 125.0F;

        return raw;
    }

    static const char * image(const asn1SccEnv_Lab_Mode_mode& x) {
        switch (x) {
            case asn1SccEnv_Lab_Mode_mode_on:
                return "Mode-ON";
            case asn1SccEnv_Lab_Mode_mode_off:
                return "Mode-OFF";
        }
        return "Mode-ERROR";
    }

    static const char * image(const asn1SccEnv_Lab_Heaters_Mode& x) {
        switch (x) {
            case asn1SccActuator_Control_Mode_autonomous_control:
                return "AUTO";
            case asn1SccActuator_Control_Mode_manual_control:
                return "MANUAL";
        }
        return "Heaters-ERROR";
    }

    /***************************************************************************
     * Context data:
     **************************************************************************/

    asn1SccBalloon_Mode balloon_mode {asn1SccBalloon_Mode_ground_await};

    bool end_of_mission {false};

    /**
     * Minimum temperature in raw format supported by
     * pyranometers and pyrgeometers
     */
    const asn1SccAnalogue_Raw_Data temperature_min_raw {
        celsius_to_raw(env_lab_manager_ctxt.temperature_min)
    };

    /**
     * @brief Acquired data from the EL equipment.
     * It includes analogue sensors and heaters from UEL, DEL;
     * and also the differential and absolute barometers data.
     */
    asn1SccEnvLab_Data data;

    /**
     * Objects and types for the data logging
     * @{
     */
    using Logfile_type = data_storage::CsvWriter<27U>;
    const Logfile_type::Line header {
        // == Timestamps & modes (5 cols) ==
        "Snapshot time [secs]",
        "Snapshot time [msecs]",
        "Mission time [secs]",
        "EL Mode",
        "Heaters Mode",

        // == UEL Data (5 cols) ==
        "UEL heater [W]",
        "UEL pyranometer reading [Raw]",
        "UEL pyrgeometer reading [Raw]",
        "UEL pyranometer temperature [Raw]",
        "UEL pyrgeometer temperature [Raw]",

        // == DEL Data (5 cols) ==
        "DEL heater [W]",
        "DEL pyranometer reading [Raw]",
        "DEL pyrgeometer reading [Raw]",
        "DEL pyranometer temperature [Raw]",
        "DEL pyrgeometer temperature [Raw]",

        // == Barometers Data (12 cols) ==
        "Dif Barom 0 [Raw]",
        "Dif Barom 1 [Raw]",
        "Dif Barom 2 [Raw]",
        "Dif Barom 3 [Raw]",

        "Abs Barom 0 reading [Raw]",
        "Abs Barom 0 reading [mBar]",
        "Abs Barom 0 temperature [Raw]",
        "Abs Barom 0 temperature [Celsius]",

        "Abs Barom 1 reading [Raw]",
        "Abs Barom 1 reading [mBar]",
        "Abs Barom 1 temperature [Raw]",
        "Abs Barom 1 temperature [Celsius]"
    };
    Logfile_type logfile {env_lab_manager_ctxt.logfile_name, ';', header};

    /** @} */
};
