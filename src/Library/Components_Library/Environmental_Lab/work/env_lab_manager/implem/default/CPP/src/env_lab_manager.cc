/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                           Env_Lab_Manager Source                           --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

// C++ body file for function Env_Lab_Manager

#include "env_lab_manager.h"
#include "env_lab_manager_state.h"   // Local parameters:   ctxt
#include "Context-env-lab-manager.h" // Context parameters: env_lab_manager_ctxt

#include "String.h" // String
#include "Images.h" // Image

#include "Time_Management.h"

/*******************************************************************************
 * Private variables and constants:
 ******************************************************************************/

namespace {
    env_lab_manager_state ctxt;
}

/*******************************************************************************
 * Private functions:
 ******************************************************************************/

namespace {

    /**
     * @brief Controls the radiation sensors based on their temperature value.
     */
    inline void control_rad_sensors (const asn1SccEnvLab_Heater_ID lab_id)
    {
        const auto &analogue_data {
              lab_id == EnvLab_Heater_ID_upwards_heater
            ? ctxt.data.payload.upwards.analogue_data
            : ctxt.data.payload.downwards.analogue_data
        };

        bool low_temperature {
            analogue_data.pyranometer_temperature < ctxt.temperature_min_raw ||
            analogue_data.pyrgeometer_temperature < ctxt.temperature_min_raw
        };

        // How much? 50% duty cycle (6V), 100% (12V), depends on batery reqs:
        const asn1SccHeater_Power_Type POWER_TBD { low_temperature ? 1.0F : 0.0F };
        env_lab_manager_RI_Control_Heater(&lab_id, &POWER_TBD);
    }

    inline void process_pressure() {
        auto pressure {
            (ctxt.data.payload.pressure_data.abs_barometers.arr[0].pressure_mbar +
             ctxt.data.payload.pressure_data.abs_barometers.arr[1].pressure_mbar) / 2.0
        };

        switch (ctxt.balloon_mode) {
            case asn1SccBalloon_Mode_ground_await:
            case asn1SccBalloon_Mode_ground_pre_launch:
                if (pressure < env_lab_manager_ctxt.pressure_mbar_ascent) {
                    asn1SccBalloon_Events ascent_altitude {asn1SccBalloon_Events_ascent_altitude};
                    env_lab_manager_RI_Notify_Event(&ascent_altitude);
                }
                break;
            case asn1SccBalloon_Mode_flight_ascent:
                if (pressure < env_lab_manager_ctxt.pressure_mbar_floating) {
                    asn1SccBalloon_Events float_altitude {asn1SccBalloon_Events_float_altitude};
                    env_lab_manager_RI_Notify_Event(&float_altitude);
                }
                break;
            case asn1SccBalloon_Mode_flight_float:
                if (pressure > env_lab_manager_ctxt.pressure_mbar_descent) {
                    asn1SccBalloon_Events descent_altitude {asn1SccBalloon_Events_descent_altitude};
                    env_lab_manager_RI_Notify_Event(&descent_altitude);
                }
                break;
        }
    }

    inline void update_time() {
        auto && [secs, usecs]   = time_management::absolute_time();
        ctxt.data.snapshot_time = {secs, usecs};
        ctxt.data.mission_time  = time_management::mission_time();
    }

    inline void store_data() {
        using data_storage::basic_images::image;
        ctxt.logfile.add_line({
            // == Timestamps & modes (5 cols) ==
            image(static_cast<int32_t>(ctxt.data.snapshot_time.secs)),
            image(static_cast<int32_t>(ctxt.data.snapshot_time.usecs)),
            image(static_cast<float>(ctxt.data.mission_time)),
            env_lab_manager_state::
            image(ctxt.data.mode.mode),
            env_lab_manager_state::
            image(ctxt.data.mode.heaters_mode),

            // == UEL Data (5 cols) ==
            image(static_cast<float>(ctxt.data.payload.upwards.heater)),
            image(static_cast<int16_t>(ctxt.data.payload.upwards.analogue_data.pyranometer_reading)),
            image(static_cast<int16_t>(ctxt.data.payload.upwards.analogue_data.pyrgeometer_reading)),
            image(static_cast<int16_t>(ctxt.data.payload.upwards.analogue_data.pyranometer_temperature)),
            image(static_cast<int16_t>(ctxt.data.payload.upwards.analogue_data.pyrgeometer_temperature)),

            // == DEL Data (5 cols) ==
            image(static_cast<float>(ctxt.data.payload.downwards.heater)),
            image(static_cast<int16_t>(ctxt.data.payload.downwards.analogue_data.pyranometer_reading)),
            image(static_cast<int16_t>(ctxt.data.payload.downwards.analogue_data.pyrgeometer_reading)),
            image(static_cast<int16_t>(ctxt.data.payload.downwards.analogue_data.pyranometer_temperature)),
            image(static_cast<int16_t>(ctxt.data.payload.downwards.analogue_data.pyrgeometer_temperature)),

            // == Barometers Data (12 cols) ==
            image(static_cast<int16_t>(ctxt.data.payload.pressure_data.dif_barometers.arr[0])),
            image(static_cast<int16_t>(ctxt.data.payload.pressure_data.dif_barometers.arr[1])),
            image(static_cast<int16_t>(ctxt.data.payload.pressure_data.dif_barometers.arr[2])),
            image(static_cast<int16_t>(ctxt.data.payload.pressure_data.dif_barometers.arr[3])),

            image(static_cast<int16_t>(ctxt.data.payload.pressure_data.abs_barometers.arr[0].pressure_raw)),
            image(static_cast<float>  (ctxt.data.payload.pressure_data.abs_barometers.arr[0].pressure_mbar)),
            image(static_cast<int16_t>(ctxt.data.payload.pressure_data.abs_barometers.arr[0].temperature_raw)),
            image(static_cast<float>  (ctxt.data.payload.pressure_data.abs_barometers.arr[0].temperature_celsius)),

            image(static_cast<int16_t>(ctxt.data.payload.pressure_data.abs_barometers.arr[1].pressure_raw)),
            image(static_cast<float>  (ctxt.data.payload.pressure_data.abs_barometers.arr[1].pressure_mbar)),
            image(static_cast<int16_t>(ctxt.data.payload.pressure_data.abs_barometers.arr[1].temperature_raw)),
            image(static_cast<float>  (ctxt.data.payload.pressure_data.abs_barometers.arr[1].temperature_celsius))
        });
    }

    inline void update_sensors_data
        (const asn1SccEnvLab_Experiment_Data_Sensors * IN_data_uel,
         const asn1SccEnvLab_Experiment_Data_Sensors * IN_data_del,
         const asn1SccEnvLab_Pressure_Data           * IN_data_pressure)
    {
        ctxt.data.payload.upwards.analogue_data   = *IN_data_uel;
        ctxt.data.payload.downwards.analogue_data = *IN_data_del;
        ctxt.data.payload.pressure_data = *IN_data_pressure;
    }

    inline void update_heaters_data() {
        env_lab_manager_RI_Get_Heaters_Status
            (&ctxt.data.mode.heaters_mode,
             &ctxt.data.payload.upwards.heater,
             &ctxt.data.payload.downwards.heater);
    }

    inline void publish_data() {
        env_lab_manager_RI_Update_EL_Data(&ctxt.data);
    }

    inline void finalize() {
        static bool finalized {false};
        if (!finalized) {
            ctxt.logfile.save_file();

            // Notify the Manager that this subsystem has finished:
            static asn1SccBalloon_Events finish_event {asn1SccBalloon_Events_el_finished};
            env_lab_manager_RI_Notify_Event(&finish_event);

            finalized = true;
        }
    }

}

/*******************************************************************************
 * Public functions:
 ******************************************************************************/

void env_lab_manager_startup(void) {
    // NOTHING
}

/**
 * Sets OUT_needs_measurements to true if this subsystems requires SDPU's measurements
 *
 * The decision is based on the current balloon mode (IN_balloon_mode).
 *
 * @param IN_balloon_mode Current balloon mode.
 * @param OUT_needs_measurements True if measurements are required.
 */
void env_lab_manager_PI_Needs_Measurements
    (const asn1SccBalloon_Mode * IN_balloon_mode,
           asn1SccBoolean_Type * OUT_needs_measurements)
{
    ctxt.balloon_mode = *IN_balloon_mode;
    switch (ctxt.balloon_mode) {
        case asn1SccBalloon_Mode_ground_await:
            ctxt.data.mode.mode = asn1SccEnv_Lab_Mode_mode_off;
            break;

        case asn1SccBalloon_Mode_ground_pre_launch:
        case asn1SccBalloon_Mode_flight_ascent:
        case asn1SccBalloon_Mode_flight_float:
            ctxt.data.mode.mode = asn1SccEnv_Lab_Mode_mode_on;
            break;

        case asn1SccBalloon_Mode_off:
            ctxt.data.mode.mode = asn1SccEnv_Lab_Mode_mode_off;
            ctxt.end_of_mission = true;
            break;
    }

    *OUT_needs_measurements = (ctxt.data.mode.mode == asn1SccEnv_Lab_Mode_mode_on);
}

/**
 * Receives SDPU's measurements.
 *
 * @note SDPU's measurements SHOULD be received ONLY when needed.
 *
 * Therefore, the caller needs to invoke the "Needs_Measurements" operation
 * before invoking this one.
 */
void env_lab_manager_PI_Send_Env_Lab_Measurements
    (const asn1SccEnvLab_Experiment_Data_Sensors * IN_data_uel,
     const asn1SccEnvLab_Experiment_Data_Sensors * IN_data_del,
     const asn1SccEnvLab_Pressure_Data           * IN_data_pressure)
{
    if (!ctxt.end_of_mission) {
        /**
         * @addtogroup Nominal activity
         * @{
         */
        // Gather data from sensors:
        update_time();
        update_sensors_data(IN_data_uel, IN_data_del, IN_data_pressure);

        // Contol both heaters based on their temperature:
        control_rad_sensors(asn1SccEnvLab_Heater_ID_upwards_heater);
        control_rad_sensors(asn1SccEnvLab_Heater_ID_downwards_heater);
        update_heaters_data();

        process_pressure();

        // Store in persistent memory and publish in data pool:
        store_data();
        publish_data();
        /** @} */
    } else {
        finalize();
    }
}
