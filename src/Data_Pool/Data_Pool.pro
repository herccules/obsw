TEMPLATE = lib
CONFIG -= qt
CONFIG += generateC

DISTFILES += \
    $(OBSW_DATA_TYPES)/DataTypes.acn \
    $(OBSW_DATA_TYPES)/DataTypes.asn \
    $(OBSW_DATA_TYPES)/DataTypes-Events.acn \
    $(OBSW_DATA_TYPES)/DataTypes-Events.asn \
    $(OBSW_DATA_TYPES)/DataTypes-OperatingModes.acn \
    $(OBSW_DATA_TYPES)/DataTypes-OperatingModes.asn \
    $(OBSW_DATA_TYPES)/DataTypes-Subsystems.asn \
    $(OBSW_DATA_TYPES)/DataTypes-Subsystems.acn \
    $(OBSW_DATA_TYPES)/DataTypes-Telecommands.acn \
    $(OBSW_DATA_TYPES)/DataTypes-Telecommands.asn \
    $(OBSW_DATA_TYPES)/DataTypes-Telemetries.acn \
    $(OBSW_DATA_TYPES)/DataTypes-Telemetries.asn \

DISTFILES += Data_Pool.asn
DISTFILES += Data_Pool.acn
DISTFILES += Data_Pool.msc
DISTFILES += interfaceview.xml
DISTFILES += work/binaries/*.msc
DISTFILES += work/binaries/coverage/index.html
DISTFILES += work/binaries/filters
#include(handleAsn1AcnBuild.pri)
include(work/taste.pro)
message($$DISTFILES)

