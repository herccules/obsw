// C+++ body file for function ATL_Data

#include "atl_data.h"
#include "atl_data_state.h"

namespace {
    atl_data_state ctxt_atl_data;
}

void atl_data_startup(void)
{
    asn1SccAtt_Lab_Data_Initialize(&ctxt_atl_data.atl_data);
}

void atl_data_PI_Get
      (asn1SccAtt_Lab_Data *OUT_atl_data)
{
   *OUT_atl_data = ctxt_atl_data.atl_data;
}


void atl_data_PI_Put
      (const asn1SccAtt_Lab_Data *IN_atl_data)
{
   ctxt_atl_data.atl_data = *IN_atl_data;
}


