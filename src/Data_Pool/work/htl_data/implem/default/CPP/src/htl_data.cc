// C+++ body file for function HTL_Data

#include "htl_data.h"
#include "htl_data_state.h"

namespace {
    htl_data_state ctxt_htl_data;
}


void htl_data_startup(void)
{
    asn1SccHTL_Data_Initialize(&ctxt_htl_data.htl_data);
}

void htl_data_PI_Get
      (asn1SccHTL_Data *OUT_htl_data)
{
   *OUT_htl_data = ctxt_htl_data.htl_data;
}


void htl_data_PI_Put
      (const asn1SccHTL_Data *IN_htl_data)
{
   ctxt_htl_data.htl_data = *IN_htl_data;
}


