/*****************************************************************************
**                                                                          **
**                    HERCCULES Software Components                         **
**                                                                          **
**                         Equipment_Handlers                                **
**                                                                          **
**                             TMU Tests                                    **
**                                                                          **
**          Copyright (C) 2022 Universidad Politécnica de Madrid            **
**                                                                          **
** This is free software;  you can redistribute it  and/or modify it  under **
** terms of the  GNU General Public License as published  by the Free Soft- **
** ware  Foundation;  either version 3,  or (at your option) any later ver- **
** sion.  This software is distributed in the hope  that it will be useful, **
** but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- **
** TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public **
** License for  more details.  You should have  received  a copy of the GNU **
** General  Public  License  distributed  with  this  software;   see  file **
** COPYING3.  If not, go to http://www.gnu.org/licenses for a complete copy **
** of the license.                                                          **
**                                                                          **
*****************************************************************************/

#include "TMU.h"
#include <gtest/gtest.h>
#include <cmath>    // fabs
#include "ExpectedValuesLoader.h"

namespace bs_tmu = board_support::tmu;

// ==============
// TMUTests class
// ==============

class TMUTests : public ::testing::Test {
protected:

    static void SetUpTestCase() {
        (void) bs_tmu::initialize();
        (void) expected_values_loader::initialize("./ExpectedValues.csv");
    }

    static constexpr float maximumDeviationRaw = 0.1 * 1E6 / 125; // Means 0.1 micro volts

    // ------------------------------------
    // Auxiliary methods for the unit tests
    // ------------------------------------

    static void ExpectedReadingFromOneLine(bs_tmu::PT1000_ID pt1000Id) {
        int16_t reading  = bs_tmu::readPT1000LineFrom(pt1000Id);
        int16_t expected = expected_values_loader::expectedValueFrom(pt1000Id);

        EXPECT_NEAR(reading, expected, maximumDeviationRaw);
    }

    static void ExpectReadingsFromAllADCChannels(bs_tmu::MUXChannel muxChannel) {
        auto readings = bs_tmu::readAllADCChannels(static_cast<bs_tmu::MUXChannel>(muxChannel));
        int adcChannel = 0;
        auto expectedValues = expected_values_loader::expectedValuesFrom(muxChannel);
        for (auto reading : readings) {
            SCOPED_TRACE("Test for ADC Channel [0 --> 3] = " + std::to_string(adcChannel));
            EXPECT_NEAR(reading, expectedValues.at(adcChannel), maximumDeviationRaw);
            adcChannel++;
        }
    }

    /// Violates MISRA 18-0-4, but `clock_nanosleep` is recommended in the Burns & Wellings RTS&PL book.
    void waitNSecs(int transitionTimeNSecs) {
        struct timespec wait {
                .tv_sec = 0,
                .tv_nsec = transitionTimeNSecs
        };

        if(clock_nanosleep(CLOCK_MONOTONIC, 0, &wait, nullptr) < 0) {
            perror("clock_nanosleep");
        }
    }
};

// --------------
// All unit tests
// --------------

TEST_F(TMUTests, InitializationSuccess) {
    ASSERT_TRUE(bs_tmu::initialize()) <<
        "----------------ooOOoo----------------\n"
        "Could NOT initialize the TMU subsystem\n"
        "----------------ooOOoo----------------";
}

TEST_F(TMUTests, CheckAllADCChannels) {
    auto firstChannel = static_cast<int>(bs_tmu::MUXChannel::CH0);
    auto lastChannel  = static_cast<int>(bs_tmu::MUXChannel::CH6);
    for (auto channel = firstChannel; channel <= lastChannel; ++channel) {
        SCOPED_TRACE("Test for MUX channel [0 --> 6] = " + std::to_string(channel));
        ExpectReadingsFromAllADCChannels(static_cast<bs_tmu::MUXChannel>(channel));
    }
}

TEST_F(TMUTests, CheckIndividualReadingsFromEachAnalogueLine) {
    auto firstLine = static_cast<int>(bs_tmu::PT1000_ID::PT1000_1);
    auto lastLine  = static_cast<int>(bs_tmu::PT1000_ID::PT1000_28);
    for (auto line = firstLine; line <= lastLine; ++line) {
        SCOPED_TRACE("Test for analogue line [0 --> 27] = " + std::to_string(line));
        ExpectedReadingFromOneLine(static_cast<bs_tmu::PT1000_ID>(line));

        static constexpr auto periodNSecs = static_cast<int>((1.0F / 8.0F) * 1E+9);
        waitNSecs(periodNSecs);
    }
}

TEST_F(TMUTests, CompareIndividualReadingsWithCompleteReadings) {
    constexpr auto firstMuxChannel = static_cast<int>(bs_tmu::MUXChannel::CH0);
    constexpr auto lastMuxChannel  = static_cast<int>(bs_tmu::MUXChannel::CH6);

    std::array<bs_tmu::PT1000_ID, 28U> pt1000IDs {
            bs_tmu::PT1000_1, bs_tmu::PT1000_8,  bs_tmu::PT1000_15, bs_tmu::PT1000_22, // MUX Ch0
            bs_tmu::PT1000_2, bs_tmu::PT1000_9,  bs_tmu::PT1000_16, bs_tmu::PT1000_23, // MUX CH1
            bs_tmu::PT1000_3, bs_tmu::PT1000_10, bs_tmu::PT1000_17, bs_tmu::PT1000_24, // MUX CH2
            bs_tmu::PT1000_4, bs_tmu::PT1000_11, bs_tmu::PT1000_18, bs_tmu::PT1000_25, // MUX CH3
            bs_tmu::PT1000_5, bs_tmu::PT1000_12, bs_tmu::PT1000_19, bs_tmu::PT1000_26, // MUX CH4
            bs_tmu::PT1000_6, bs_tmu::PT1000_13, bs_tmu::PT1000_20, bs_tmu::PT1000_27, // MUX CH5
            bs_tmu::PT1000_7, bs_tmu::PT1000_14, bs_tmu::PT1000_21, bs_tmu::PT1000_28  // MUX CH6
    };
    uint pt1000Id = 0U;

    for (auto muxChannel = firstMuxChannel; muxChannel <= lastMuxChannel; ++muxChannel) {
        SCOPED_TRACE("- Test for MUX channel [0 --> 6] = " + std::to_string(muxChannel));

        auto completeReadings =
                bs_tmu::readAllADCChannels(static_cast<bs_tmu::MUXChannel>(muxChannel));

        for (auto completeReadingsAt : completeReadings) {
            auto individualReading = bs_tmu::readPT1000LineFrom(pt1000IDs.at(pt1000Id));
            pt1000Id++;
            EXPECT_NEAR(individualReading, completeReadingsAt, maximumDeviationRaw);
        }
    }
}