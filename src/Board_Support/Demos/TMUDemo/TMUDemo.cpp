//////////////////////////////////////////////////////////////////////////////
//                                                                          //
//          Copyright (C) 2022 Universidad Politécnica de Madrid            //
//                                                                          //
// This is free software;  you can redistribute it  and/or modify it  under //
// terms of the  GNU General Public License as published  by the Free Soft- //
// ware  Foundation;  either version 3,  or (at your option) any later ver- //
// sion.  This software is distributed in the hope  that it will be useful, //
// but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- //
// TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public //
// License for  more details.  You should have  received  a copy of the GNU //
// General  Public  License  distributed  with  this  software;   see  file //
// COPYING3.  If not, go to http://www.gnu.org/licenses for a complete copy //
// of the license.                                                          //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

#include "TMU.h"
#include "ADS1115_ADC.h"
#include "TMU_Conf.h"

#include <cstdint>
#include <iostream>
#ifdef DEBUG
#include <fstream> // std::ofstream
#endif
#include <cmath>
#include <ctime>
#include <functional>
#include <signal.h>

namespace bs = board_support;
namespace eh = equipment_handlers;

namespace {
    #ifdef DEBUG
    std::ofstream file ("log.txt", std::ofstream::out | std:ofstream::app);
    #endif

    int32_t rawToMicroVolts(int16_t raw) {
        const int8_t LSB = 125;
        return (raw * LSB);
    }

    struct timespec secondsToTimespec(float seconds) {
        struct timespec ts;

        float intPart;
        ts.tv_nsec = static_cast<int> (modff(seconds, &intPart) * 1.0E09);
        ts.tv_sec  = static_cast<int> (intPart);

        return ts;
    }

    template <typename Activity>
    void periodicActivity(struct timespec period, int nLoops, Activity activity)
    {
        std::cout << "Period: "<< period.tv_sec << " secs, "
                  << period.tv_nsec << "nsecs" << std::endl;

        struct timespec next;
        if(clock_gettime(CLOCK_MONOTONIC, &next) < 0) {
            perror("clock_gettime");
        }

        for(int i = 0; i < nLoops; ++i) {
            if(clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &next, 0) < 0) {
                perror("clock_nanosleep");
            }

            activity();

            next.tv_sec  += period.tv_sec;
            next.tv_nsec += period.tv_nsec;
        }
    }

    float dataRateToFloat(eh::ADS1115_ADC::DataRate dr) {
        switch (dr) {
            case eh::ADS1115_ADC::DataRate::_8SPS:
                return 8.0F;
            case eh::ADS1115_ADC::DataRate::_16SPS:
                return 16.0F;
            case eh::ADS1115_ADC::DataRate::_32SPS:
                return 32.0F;
            case eh::ADS1115_ADC::DataRate::_64SPS:
                return 64.0F;
            case eh::ADS1115_ADC::DataRate::_128SPS:
                return 128.0F;
            case eh::ADS1115_ADC::DataRate::_250SPS:
                return 250.0F;
            case eh::ADS1115_ADC::DataRate::_475SPS:
                return 475.0F;
            case eh::ADS1115_ADC::DataRate::_860SPS:
                return 860.0F;
            default:
                return 0.0F;
        }
        return 0.0F;
    }

    /**
     * Periodic activity
     */
    void printReadings() {

        for (int muxChannel = 0; muxChannel < 7; ++muxChannel) {
            auto readings = bs::tmu::readAllADCChannels(static_cast<bs::tmu::MUXChannel>(muxChannel));

            int adcChannel = 0;
            for (auto r : readings) {
                float volts = static_cast<float>(rawToMicroVolts(r)) / static_cast<float>(1.0E6);
                std::cout << "Channel: " << muxChannel << " - " << adcChannel << " = " << volts << std::endl;
		#ifdef DEBUG
                file << r << "," << volts;
		#endif
                adcChannel++;
            }
        }
        std::cout << "---------------------------------------" << std::endl;
	#ifdef DEBUG
        file << std::endl;
	#endif
    }

    float samplePeriodFromConsole() {
        float samplePeriod = 1.0F;
        std::cout << "Please, introduce the desired sample period: " << std::endl;
        std::cin >> samplePeriod;
        return samplePeriod;
    }

    void exitHandler(int s) {
        std::cout << "[TMUDemo] End Of Program after signal: " << s << std::endl;
	#ifdef DEBUG
        file.close();
	#endif
        exit(1);
    }
}

int main() {

    // Handler ctrl-c:
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = exitHandler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;

    sigaction(SIGQUIT, &sigIntHandler, NULL);

    // Begin demo:
    bool tmuInitialized = bs::tmu::initialize();
    if (tmuInitialized) {
        std::cout << "[TMUDemo] tmu initialized!" << std::endl;
    }

    float dr = dataRateToFloat(bs::tmu::conf::adcMode.dataRate);
    std::cout << "[TMUDemo] reading at " << dr << " samples/second" <<  std::endl;

    // struct timespec period = secondsToTimespec(1.0F / dr);
    struct timespec period = secondsToTimespec(samplePeriodFromConsole());
    periodicActivity(period, 999, printReadings);

    return 0;
}
