//////////////////////////////////////////////////////////////////////////////
//                                                                          //
//          Copyright (C) 2022 Universidad Politécnica de Madrid            //
//                                                                          //
// This is free software;  you can redistribute it  and/or modify it  under //
// terms of the  GNU General Public License as published  by the Free Soft- //
// ware  Foundation;  either version 3,  or (at your option) any later ver- //
// sion.  This software is distributed in the hope  that it will be useful, //
// but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- //
// TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public //
// License for  more details.  You should have  received  a copy of the GNU //
// General  Public  License  distributed  with  this  software;   see  file //
// COPYING3.  If not, go to http://www.gnu.org/licenses for a complete copy //
// of the license.                                                          //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

#include "SDPU.h"
#include <iostream>
#include <cmath>
#include <ctime>
#include <functional>
#include <csignal>
#include <queue>
#include <numeric>  // accumulate

namespace bs = board_support;

namespace {
    // -------------------
    // Auxiliary variables
    // -------------------

    unsigned int meanFilterLength = 1U;

    using PhotoReadings = std::array<int32_t , 4U>;
    using HistoryType   = std::deque<PhotoReadings>;
    HistoryType history;

    // -------------------
    // Auxiliary functions
    // -------------------

    int32_t rawToMicroVolts(int16_t raw) {
        const int8_t LSB = 125;
        return (raw * LSB);
    }

    struct timespec secondsToTimespec(float seconds) {
        struct timespec ts;

        float intPart;
        ts.tv_nsec = static_cast<int> (modff(seconds, &intPart) * 1.0E09);
        ts.tv_sec  = static_cast<int> (intPart);

        return ts;
    }

    template <typename Activity>
    void periodicActivity(struct timespec period, int nLoops, Activity activity)
    {
        std::cout << "Period: "<< period.tv_sec << " secs, "
                  << period.tv_nsec << "nsecs" << std::endl;

        struct timespec next;
        if(clock_gettime(CLOCK_MONOTONIC, &next) < 0) {
            perror("clock_gettime");
        }

        for(int i = 0; i < nLoops; ++i) {
            if(clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &next, 0) < 0) {
                perror("clock_nanosleep");
            }

            activity();

            next.tv_sec  += period.tv_sec;
            next.tv_nsec += period.tv_nsec;
        }
    }

    // ----------------------------------------
    // Cyclic payload and its auxiliary methods
    // ----------------------------------------

    void pushReadingToHistory(const PhotoReadings &readings) {
        history.push_back(readings);
        if (history.size() > meanFilterLength) {
            history.pop_front();
        }
    }

    PhotoReadings applyMeanFilter() {
        auto addPhotodiodes = [](PhotoReadings a, PhotoReadings b) {
            return PhotoReadings {a.at(0U) + b.at(0U),
                                  a.at(1U) + b.at(1U),
                                  a.at(2U) + b.at(2U),
                                  a.at(3U) + b.at(3U)};
        };

        constexpr auto initial = PhotoReadings {0, 0, 0, 0};
        PhotoReadings filteredReadings =
                std::accumulate(history.begin(), history.end(), initial, addPhotodiodes);
        for (auto & photodiode : filteredReadings) {
            photodiode = photodiode / meanFilterLength;
        }
        return filteredReadings;
    }

    void printReadings() {

        // 1) read all photo diodes from SDPU:
        PhotoReadings readings {
            rawToMicroVolts(bs::sdpu::readRawFrom(bs::sdpu::PHOTODIODE_1)),
            rawToMicroVolts(bs::sdpu::readRawFrom(bs::sdpu::PHOTODIODE_2)),
            rawToMicroVolts(bs::sdpu::readRawFrom(bs::sdpu::PHOTODIODE_3)),
            rawToMicroVolts(bs::sdpu::readRawFrom(bs::sdpu::PHOTODIODE_4))
        };

        // 2) insert readings in history:
        pushReadingToHistory(readings);

        // 3) compute mean:
        PhotoReadings filtered = applyMeanFilter();

        // 4) print in CSV format:
        for (auto r : readings) {
            std::cout << r << ", ";
        }
        for (auto f : filtered) {
            std::cout << f << ", ";
        }
        std::cout << std::endl;
    }

    // ----------------------------------
    // Auxiliary functions for user input
    // ----------------------------------

    float samplePeriodFromConsole() {
        float samplePeriod = 1.0F;
        std::cout << "Please, introduce the desired sample period: " << std::endl;
        std::cin >> samplePeriod;
        return samplePeriod;
    }

    int numberOfIterationsFromConsole() {
        int nOfIters = 1000;
        std::cout << "Please, introduce the desired number of iterations: " << std::endl;
        std::cin >> nOfIters;
        return nOfIters;
    }

    void getMeanFilterLengthFromConsole() {
        std::cout << "Please, introduce the # of samples for mean filter: " << std::endl;
        std::cin >> meanFilterLength;
    }

    void exitHandler(int s) {
        std::cout << "[TMUDemo] End Of Program after signal: " << s << std::endl;
        exit(1);
    }
}

int main() {

    // Handler ctrl-c:
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = exitHandler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;

    sigaction(SIGQUIT, &sigIntHandler, NULL);

    // Begin demo:

    bool sdpuInitialized = bs::sdpu::initialize();
    if (sdpuInitialized) {
        std::cout << "[Nadir Sensor Demo] SDPU initialized!" << std::endl;
    }

    struct timespec period = secondsToTimespec(samplePeriodFromConsole());
    getMeanFilterLengthFromConsole();
    periodicActivity(period, numberOfIterationsFromConsole(), printReadings);

    return 0;
}
