//////////////////////////////////////////////////////////////////////////////
//                                                                          //
//          Copyright (C) 2022 Universidad Politécnica de Madrid            //
//                                                                          //
// This is free software;  you can redistribute it  and/or modify it  under //
// terms of the  GNU General Public License as published  by the Free Soft- //
// ware  Foundation;  either version 3,  or (at your option) any later ver- //
// sion.  This software is distributed in the hope  that it will be useful, //
// but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- //
// TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public //
// License for  more details.  You should have  received  a copy of the GNU //
// General  Public  License  distributed  with  this  software;   see  file //
// COPYING3.  If not, go to http://www.gnu.org/licenses for a complete copy //
// of the license.                                                          //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

#include "PCU.h"
#include <iostream>

using namespace board_support;
using std::cout;

namespace {
    int userOption() {
        cout << "\nChoose an operation:"
                "\n  1. activatePowerSupplyLine."
                "\n  2. deactivatePowerSupplyLine."
                "\n  3. setEnvLabPWMDutyCycle."
                "\n  4. setHTLPWMDutyCycle."
                "\n  5. readTC74."
                "\n  6. QUIT from the program\n\n" << std::endl;
        int option = 0;
        if (scanf("%d", &option)) {perror("scanf");}
        return option;
    }

    bool console() {
        switch(userOption()){
            case 1: {
                // 1) Get sub-option:
                cout << "Choose a Power supply line: 0->SDPU, 1->TMU, 2->AL" << std::endl;
                int option = 0;
                if (scanf("%d", &option)) {perror("scanf");}

                // 2) Print result:
                bool ok = pcu::activatePowerSupplyFor(static_cast<pcu::PowerSupplyLines>(option));
                cout << (ok ? "success.\n" : "failure.");
            } break;

            case 2 : {
                // 1) Get sub-option:
                cout << "Choose a Power supply line: 0->SDPU, 1->TMU, 2->AL" << std::endl;
                int option = 0;
                if (scanf("%d", &option)) {perror("scanf");}

                // 2) Print result:
                bool ok = pcu::deactivatePowerSupplyFor(static_cast<pcu::PowerSupplyLines>(option));
                cout << (ok ? "success.\n" : "failure.");
            } break;

            case 3 : {
                // 1) Get sub-option:
                cout << "Choose the environmental lab: 0->upwards, 1->downwards" << std::endl;
                int option = 0;
                if (scanf("%d", &option)) {perror("scanf");}

                cout << "Choose the PWM duty cycle %. 0 .. 100" << std::endl;
                float pwmPercentage = 0.0F;
                std::cin >> pwmPercentage;

                // 2) Print result:
                bool ok = pcu::setEnvLabPWMDutyCycle(static_cast<pcu::EnvLabPWMHeaters>(option), pwmPercentage);
                cout << (ok ? "success.\n" : "failure.");
            } break;

            case 4 : {
                // 1) Get sub-options:
                cout << "Choose the Heater ID. E.g.: 1 for HEATER 1" << std::endl;
                int option = 0;
                std::cin >> option;
                auto heaterId = static_cast<pcu::HTLPWMHeater>(option - 1);

                cout << "Choose the PWM duty cycle %. 0 .. 100" << std::endl;
                float pwmPercentage = 0.0F;
                std::cin >> pwmPercentage;

                // 2) Print result:
                bool ok = pcu::setHTLPWMDutyCycle(heaterId, pwmPercentage);
                cout << (ok ? "success.\n" : "failure.");
            } break;

            case 5 : {
                int8_t temperature;
                bool success = pcu::readTC74(temperature);
                if (success) {
                    std::cout << "Temperature = " << static_cast<int16_t>(temperature) << std::endl;
                } else {
                    std::cout << "Failure";
                }
            } break;

            default : {
                return false;
            }
        }
        return true;
    }
}

int main() {
    if (pcu::initialize_switches()) {
        pcu::activatePowerSupplyFor(pcu::PowerSupplyLines::AL_LINE);
        pcu::activatePowerSupplyFor(pcu::PowerSupplyLines::TMU_LINE);
        pcu::activatePowerSupplyFor(pcu::PowerSupplyLines::SDPU_LINE);

        pcu::initialize_sensors();
    }

    while (console()) {
        // Forever ...
    }

    return 0;
}
