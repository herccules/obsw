//////////////////////////////////////////////////////////////////////////////
//                                                                          //
//          Copyright (C) 2022 Universidad Politécnica de Madrid            //
//                                                                          //
// This is free software;  you can redistribute it  and/or modify it  under //
// terms of the  GNU General Public License as published  by the Free Soft- //
// ware  Foundation;  either version 3,  or (at your option) any later ver- //
// sion.  This software is distributed in the hope  that it will be useful, //
// but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- //
// TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public //
// License for  more details.  You should have  received  a copy of the GNU //
// General  Public  License  distributed  with  this  software;   see  file //
// COPYING3.  If not, go to http://www.gnu.org/licenses for a complete copy //
// of the license.                                                          //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

#include "TMU.h"
#include "iostream"
#include "unistd.h"

namespace bs = board_support;

int32_t rawToMicroVolts(int16_t raw) {
    const int8_t LSB = 125;
    return (raw * LSB);
}

/**
 * Pseudo-periodic activity
 */
void printReadings() {
    for (int muxChannel = 0; muxChannel < 7; ++muxChannel) {
        auto readings = bs::tmu::readAllADCChannels(static_cast<bs::tmu::MUXChannel>(muxChannel));

        int adcChannel = 0;
        for (auto r : readings) {
            float volts = static_cast<float>(rawToMicroVolts(r)) / static_cast<float>(1.0e6);
            std::cout << "Channel @ " << muxChannel << " - " << adcChannel << " = " << volts << std::endl;
            adcChannel++;
        }

        (void)sleep(1U);
    }
}

int main() {

    bool tmuInitialized = bs::tmu::initialize();
    if (tmuInitialized) {
        std::cout << "[TMUPrototypeDemo] tmu initialized!" << std::endl;
    }

    std::cout << "[TMUPrototypeDemo] reading at 8SPS" << std::endl;
    for (int i = 0; i < 10; ++i) {
        printReadings();
    }
    std::cout << std::endl;

    return 0;
}