/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                              BOARD  SUPPORT                                --
--                                                                            --
--                               SDPU Header                                  --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#ifndef HAL_SDPU_H
#define HAL_SDPU_H

#include <array>
#include <cstdint>

namespace board_support::sdpu {

    bool initialize();

    void finalize();

    enum AnalogDevice : uint8_t {
        UP_PYRANOMETER = 0U,
        UP_PYRGEOMETER,
        DOWN_PYRANOMETER,
        DOWN_PYRGEOMETER,

        THERM_1 = 4U,
        THERM_2,
        THERM_3,
        THERM_4,
        PT1000_1,
        PT1000_2,

        DIFF_BAROM_1 = 10U,
        DIFF_BAROM_2,
        DIFF_BAROM_3,
        DIFF_BAROM_4,

        PHOTODIODE_1 = 14U,
        PHOTODIODE_2,
        PHOTODIODE_3,
        PHOTODIODE_4
    };

    /// @return -1 in case of failure. Results from 0 up to 0x7FFF are expected
    int16_t readRawFrom(AnalogDevice analogDevice, uint8_t n_retries = 0U);

    /**
     * In the SDPU it does not make sense to read  the four channels of the ADC when the MUXs' select lines are in
     * channels 6 or 7. Hence, the upper limit for the channel is 5.
     */
    enum MUXChannel : uint8_t {
        CH0 = 0U,
        CH1 = 1U,
        CH2 = 2U,
        CH3 = 3U,
        CH4 = 4U,
        CH5 = 5U
    };

    /// @return -1s in case of failure. Results from 0 up to 0x7FFF are expected
    std::array<int16_t, 4U> readAllADCChannels(MUXChannel muxChannel, uint8_t n_retries = 0U);

    struct BarometerReading {
        // Calculated with second order temperature compensation (see datasheet fig. 3):
        float temperature_celsius;
        float pressure_milliBar;

        // Raw digital pressure and temperature data:
        /// AKA D1 from pg. 8
        int32_t temperature_raw;
        /// AKA D2 from pg. 8
        int32_t pressure_raw;
    };

    enum PressureSensor {
        PS1 = 0U,
        PS2 = 1U
    };

    bool getPressureFrom(PressureSensor ps, BarometerReading &reading);

    bool resetPressureSensor(PressureSensor ps);

    bool resetADC();

    float pt1000ToCelsius(int16_t raw);

    float thermistorToCelsius(int16_t raw);

    float pyranometersTomV(int16_t raw);

    float pyrgeometersTomV(int16_t raw);

    float diffBarometersToPa(int16_t raw);

}
/*
 * SDPU Simplified block diagram:
 * ------------------------------
 *
 *     .-----------------------.     .---------.      .---------------.
 *     | 4 Pyrano/pyrgeometers |=====| MUX  M1 |--00--|               |
 *     '-----------------------'     '-+--+--+-'      |               |
 *                                     |  |  |        |               |
 *     .-----------------------.     .-+--+--+-.      |               |
 *     | 4 Therms. & 2 PT1000s |=====| MUX  M2 |--01--|               |
 *     '-----------------------'     '-+--+--+-'      |  Delta Sigma  |
 *                                     |  |  |        |               |
 *     .-----------------------.     .-+--+--+-.      |     A D C     |
 *     | Diff Barometer 1 -> 4 |=====| MUX  M3 |--10--|               |
 *     '-----------------------'     '-+--+--+-'      |               |
 *                                     |  |  |        |               |
 *     .-----------------------.     .-+--+--+-.      |               |
 *     | Photo-diodes 1 -> 4   |=====| MUX  M4 |--11--|               |
 *     '-----------------------'     '-+--+--+-'      '---------------'
 *                                     |  |  |
 *                                   Select pins
 */

#endif //HAL_SDPU_H
