/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                              BOARD  SUPPORT                                --
--                                                                            --
--                                TMU Header                                  --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#ifndef HAL_TMU_H
#define HAL_TMU_H

#include <cstdint>
#include <array>

namespace board_support::tmu {
    enum PT1000_ID : uint8_t {
        PT1000_1 = 0U, PT1000_2,  PT1000_3,  PT1000_4,  PT1000_5,  PT1000_6,  PT1000_7,
        PT1000_8,      PT1000_9,  PT1000_10, PT1000_11, PT1000_12, PT1000_13, PT1000_14,
        PT1000_15,     PT1000_16, PT1000_17, PT1000_18, PT1000_19, PT1000_20, PT1000_21,
        PT1000_22,     PT1000_23, PT1000_24, PT1000_25, PT1000_26, PT1000_27, PT1000_28,
    };

    enum MUXChannel : uint8_t {
        CH0 = 0U,
        CH1 = 1U,
        CH2 = 2U,
        CH3 = 3U,
        CH4 = 4U,
        CH5 = 5U,
        CH6 = 6U,
    };

    bool initialize();

    void finalize();

    /// @return -1 in case of failure. Results from 0 up to 0x7FFF are expected
    int16_t readPT1000LineFrom(PT1000_ID pt1000ID, uint8_t n_retries = 0U);

    /**
     * @example if muxInput is CH0 you will select the first channel for all the multiplexers (M1 to M4), which
     * corresponds to the PT1000_1, PT1000_8, PT1000_15, and PT1000_22 thermistors.
     * @param muxInput
     * @return array with four readings, where the index i represents the ith channel from the ADC.
     *         -1s are returned in case of failure.
     *         Results from 0 up to 0x7FFF are expected.
     */
    std::array<int16_t, 4U> readAllADCChannels(MUXChannel muxInput, uint8_t n_retries = 0U);

    bool resetADC();

    /**
     * @brief Transfer function for PT100s
     */
    float pt1000ToCelsius(int16_t raw);

}

/*
 * TMU Simplified block diagram:
 * -----------------------------
 *
 *     .-----------------------.     .---------.      .---------------.
 *     | PT1000_1  ->  PT10007 |=====| MUX  M1 |--00--|               |
 *     '-----------------------'     '-+--+--+-'      |               |
 *                                     |  |  |        |               |
 *     .-----------------------.     .-+--+--+-.      |               |
 *     | PT1000_8  -> PT100014 |=====| MUX  M2 |--01--|               |
 *     '-----------------------'     '-+--+--+-'      |  Delta Sigma  |
 *                                     |  |  |        |               |
 *     .-----------------------.     .-+--+--+-.      |     A D C     |
 *     | PT1000_15 -> PT100021 |=====| MUX  M3 |--10--|               |
 *     '-----------------------'     '-+--+--+-'      |               |
 *                                     |  |  |        |               |
 *     .-----------------------.     .-+--+--+-.      |               |
 *     | PT1000_22 -> PT100028 |=====| MUX  M4 |--11--|               |
 *     '-----------------------'     '-+--+--+-'      '---------------'
 *                                     |  |  |
 *                                   Select pins
 */

#endif //HAL_TMU_H
