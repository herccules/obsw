/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                              BOARD  SUPPORT                                --
--                                                                            --
--                                TMU Source                                  --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "TMU.h"
#include "TMU_Conf.h"

#include "BusHandlers.h"
#include "ADS1115_ADC.h"
#include "MUX.h"

#include <iostream>
#include <cstdio>  // perror
#include <cstdlib> // atexit

namespace bs = equipment_handlers;
namespace bh = bus_handlers;
namespace bh_dt = bus_handlers::data;

namespace {

    using namespace board_support::tmu;

    bool initialized = false;

    bs::ADS1115_ADC adc;
    bs::MUX mux;

    inline std::pair<int, bs::ADS1115_ADC::Channel> toMuxChannelPair(const PT1000_ID pt1000ID) {
        int muxChannel = 0;
        bs::ADS1115_ADC::Channel adcChannel = bs::ADS1115_ADC::Channel::Channel0;

        if (pt1000ID <= PT1000_7) {
            // 1 to 7
            muxChannel = pt1000ID - PT1000_1;
            adcChannel = bs::ADS1115_ADC::Channel::Channel0;
        } else if (pt1000ID <= PT1000_14) {
            // 8 to 14
            muxChannel = pt1000ID - PT1000_8;
            adcChannel = bs::ADS1115_ADC::Channel::Channel1;
        } else if (pt1000ID <= PT1000_21) {
            // 15 to 21
            muxChannel = pt1000ID - PT1000_15;
            adcChannel = bs::ADS1115_ADC::Channel::Channel2;
        } else if (pt1000ID <= PT1000_28) {
            // 22 to 28
            muxChannel = pt1000ID - PT1000_22;
            adcChannel = bs::ADS1115_ADC::Channel::Channel3;
        } else {
            // TODO: Log error
            std::cerr << "PT1000 identifier out of range, GT 28" << std::endl;
        }

        return std::make_pair(muxChannel, adcChannel);
    }

    inline int16_t readOneChannel(int muxInput, equipment_handlers::ADS1115_ADC::Channel adcChannel, uint8_t n_retries) {
        mux.selectInput(muxInput);
        return adc.readOneChannel(adcChannel, n_retries);
    }
}

namespace board_support::tmu {

    bool initialize() {
        static bool atexit_success {atexit(finalize) == 0};
        if (!atexit_success) {
            perror("[TMU] Could not register Finalize atexit");
            atexit_success = atexit(finalize) == 0;
        }

        if (!bh::initialize()) {
            perror("[TMU] Could not initialize Bus Handlers");
            return false;
        }

        if (!initialized) {
            bool muxInitialized = mux.initialize(tmu::conf::muxPin0, tmu::conf::muxPin1, tmu::conf::muxPin2);
            bool adcInitialized = adc.initialize(tmu::conf::i2cBusId, tmu::conf::i2cAddress, tmu::conf::adcMode);

            initialized = adcInitialized && muxInitialized;
        }

        return initialized;
    }

    void finalize() {        
        mux.finalize();
        initialized = false;
    }

    int16_t readPT1000LineFrom(PT1000_ID pt1000ID, uint8_t n_retries) {
        auto [muxInput, adcChannel] = toMuxChannelPair(pt1000ID);
        return readOneChannel(muxInput, adcChannel, n_retries);
    }

    std::array<int16_t, 4U> readAllADCChannels(MUXChannel muxInput, uint8_t n_retries) {
        mux.selectInput(muxInput);
        return adc.readAllChannels(n_retries);
    }

    bool resetADC() {
        if (adc.reset()) {
            return adc.initialize(tmu::conf::i2cBusId, tmu::conf::i2cAddress, tmu::conf::adcMode);
        }
        return false;
    }

    float pt1000ToCelsius(int16_t raw) {
        float volts = static_cast<float>(raw) * 125.0F;
        return 34.18F * static_cast<float>(volts) - 80.24F;
    }

}