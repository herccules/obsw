/* CIF PROCESS (250, 150), (150, 75) */
process Event_Handler;
    /* CIF TEXT (264, 59), (381, 233) */
    --     M a n a g e r   E v e n t    H a n d l e r
    --    Purpose:
    --       This component processes the synchronous notified event
    --       and invokes the corresponding required interface from the
    --       mode manager.
    
    -- Local variables:
    DCL Received_Event Balloon_Events;
    DCL
       HTL_Has_Finished,
       EL_Has_Finished,
       ATL_Has_Finished,
       PCU_Has_Finished,
       NADS_Has_Finished,
       TTC_Has_Finished   T_Boolean   :=   False;
    /* CIF ENDTEXT */
    /* CIF procedure (268, 406), (129, 35) */
    procedure All_Have_Finished
    /* CIF comment (427, 404), (225, 38) */
    comment 'Returns true if all the experiments
have finished';
        /* CIF TEXT (388, 0), (267, 140) */
        -- Text area for declarations and comments
        
        DCL All_Finished T_Boolean := False;
        
        RETURNS T_Boolean;
        /* CIF ENDTEXT */
        /* CIF START (494, 155), (70, 35) */
        START;
            /* CIF task (277, 210), (502, 98) */
            task EL_Has_Finished         :=  EL_Has_Finished        OR  Received_Event = el_finished,
ATL_Has_Finished      :=  ATL_Has_Finished      OR  Received_Event = atl_finished,
HTL_Has_Finished      :=  HTL_Has_Finished     OR  Received_Event = htl_finished,
PCU_Has_Finished     :=  PCU_Has_Finished     OR  Received_Event = pcu_finished,
NADS_Has_Finished  :=  NADS_Has_Finished  OR  Received_Event = nads_finished,
TTC_Has_Finished      := TTC_Has_Finished       OR Received_Event = ttc_finished;
            /* CIF task (438, 323), (180, 113) */
            task All_Finished :=
             EL_Has_Finished
   AND ATL_Has_Finished
   AND HTL_Has_Finished
   AND PCU_Has_Finished
   AND NADS_Has_Finished
   AND TTC_Has_Finished;
            /* CIF return (511, 466), (35, 35) */
            return All_Finished;
    endprocedure;
    /* CIF procedure (270, 352), (104, 35) */
    procedure Handle_Event
    /* CIF comment (415, 350), (242, 38) */
    comment 'Updates Received_Event
and triggers the Handle_Event signal.';
        /* CIF TEXT (412, 125), (230, 140) */
        -- Handle Event:
        FPAR
           IN Balloon_Event Balloon_Events;
        /* CIF ENDTEXT */
        /* CIF START (786, 128), (70, 35) */
        START;
            /* CIF task (712, 183), (217, 35) */
            task Received_Event := Balloon_Event;
            /* CIF alternative (770, 238), (100, 50) */
            alternative DEBUG_MGR;
                /* CIF ANSWER (707, 308), (70, 23) */
                (True):
                    /* CIF PROCEDURECALL (553, 351), (377, 35) */
                    call writeln ('[Manager-Event_Handler] handle', Received_Event);
                /* CIF ANSWER (941, 308), (70, 23) */
                (False):
            endalternative;
            /* CIF return (803, 402), (35, 35) */
            return ;
    endprocedure;
    /* CIF procedure (268, 462), (248, 35) */
    procedure Set_All_Experiments_To_Autonomous;
        /* CIF START (220, 69), (70, 35) */
        START;
            /* CIF PROCEDURECALL (128, 124), (252, 53) */
            call Set_EL_Heaters_Mode
   ({ upwards        autonomous_control,
       downwards  autonomous_control });
            /* CIF PROCEDURECALL (121, 207), (266, 83) */
            call set_HTL_Heaters_Mode
   ({ experiment_1 autonomous_control,
       experiment_2 autonomous_control,
       experiment_3 autonomous_control,
       experiment_4 autonomous_control  });
            /* CIF return (237, 310), (35, 35) */
            return ;
    endprocedure;
    /* CIF START (1028, 90), (70, 35) */
    START;
        /* CIF PROCEDURECALL (912, 145), (300, 35) */
        call writeln("[Manager - Event Handler] Initialized");
        /* CIF NEXTSTATE (1018, 195), (89, 35) */
        NEXTSTATE Wait_Event;
    /* CIF state (1018, 195), (89, 35) */
    state Wait_Event;
        /* CIF input (1010, 250), (104, 35) */
        input Handle_Event;
            /* CIF decision (1004, 305), (116, 50) */
            decision Received_Event;
                /* CIF ANSWER (789, 375), (87, 23) */
                (lost_comm):
                    /* CIF PROCEDURECALL (709, 413), (248, 35) */
                    call Set_All_Experiments_To_Autonomous;
                /* CIF ANSWER (968, 375), (115, 23) */
                (restored_comm):
                /* CIF ANSWER (1106, 375), (104, 23) */
                (float_altitude):
                    /* CIF PROCEDURECALL (1095, 413), (126, 35) */
                    call Floating_Altitude;
                /* CIF ANSWER (1367, 375), (114, 23) */
                (ascent_altitude):
                    /* CIF PROCEDURECALL (1365, 413), (118, 35) */
                    call Ascent_Altitude;
                /* CIF ANSWER (1232, 375), (122, 23) */
                (descent_altitude):
                    /* CIF PROCEDURECALL (1258, 414), (71, 35) */
                    call Cut_Off;
                /* CIF ANSWER (1550, 375), (107, 98) */
                (el_finished,
atl_finished,
htl_finished,
pcu_finished,
nads_finished,
ttc_finished):
                    /* CIF decision (1527, 503), (154, 50) */
                    decision call All_Have_Finished
                    /* CIF comment (1499, 300), (195, 53) */
                    comment 'This big branch is entered
if the Received_Event
matches any of these options';
                        /* CIF ANSWER (1620, 573), (70, 23) */
                        (FALSE):
                        /* CIF ANSWER (1530, 573), (70, 23) */
                        (TRUE):
                            /* CIF PROCEDURECALL (1517, 611), (94, 35) */
                            call All_Finished;
                    enddecision;
            enddecision;
            /* CIF NEXTSTATE (1018, 677), (89, 35) */
            NEXTSTATE Wait_Event;
    endstate;
endprocess Event_Handler;