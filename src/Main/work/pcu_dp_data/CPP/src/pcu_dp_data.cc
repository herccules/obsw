// C+++ body file for function PCU_DP_Data

#include "pcu_dp_data.h"
#include "pcu_dp_data_state.h"

// Define and use function state inside this context structure
// avoid defining global/static variable elsewhere
namespace {
    pcu_dp_data_state ctxt_pcu_dp_data;
}

void pcu_dp_data_startup(void)
{
   asn1SccPCU_Data_Initialize(&ctxt_pcu_dp_data.data);
}

void pcu_dp_data_PI_Get
      (asn1SccPCU_Data *OUT_pcu_data)
{
    *OUT_pcu_data = ctxt_pcu_dp_data.data;
}

void pcu_dp_data_PI_Put
      (const asn1SccPCU_Data *IN_pcu_data)
{
    ctxt_pcu_dp_data.data = *IN_pcu_data;
}
