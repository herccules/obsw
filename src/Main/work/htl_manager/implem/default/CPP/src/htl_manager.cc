/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                             HTL_Manager Source                             --
--                                                                            --
--            Copyright (C) 2023 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

// C++ body file for function HTL_Manager

#include "htl_manager.h"
#include "htl_manager_state.h" // Local parameters: ctxt
#include "Context-htl-manager.h"

#include "debug_messages.h"

#include "TMU.h" // HW I/F for the TMU board

#include "String.h" // String
#include "Images.h" // Image

#include "Time_Management.h"
#include <iostream>

/*******************************************************************************
 * Private variables and constants:
 ******************************************************************************/

namespace {
    /**
     * Define and use function state inside this context structure
     * avoid defining global/static variable elsewhere
     */
    htl_manager_state ctxt;
}

/*******************************************************************************
 * Private functions:
 ******************************************************************************/

namespace {
    inline void process_balloon_mode() {
        auto prev_balloon_mode {ctxt.balloon_mode};

        htl_manager_RI_Get_Mode(&ctxt.balloon_mode);
        switch (ctxt.balloon_mode) {
        case asn1SccBalloon_Mode_ground_await:
            ctxt.data.mode.mode = asn1SccHTL_Mode_mode_off;
            break;

        case asn1SccBalloon_Mode_ground_pre_launch:
            ctxt.data.mode.mode = asn1SccHTL_Mode_mode_pre_launch;
            break;

        case asn1SccBalloon_Mode_flight_ascent:
            if (ctxt.data.mode.mode != asn1SccHTL_Mode_mode_ascent_mode1 &&
                ctxt.data.mode.mode != asn1SccHTL_Mode_mode_ascent_mode2 ) {
                ctxt.data.mode.mode = asn1SccHTL_Mode_mode_ascent_mode1;
            }
            break;

        case asn1SccBalloon_Mode_flight_float:
            if (ctxt.data.mode.mode != asn1SccHTL_Mode_mode_float_mode1 &&
                ctxt.data.mode.mode != asn1SccHTL_Mode_mode_float_mode2) {
                ctxt.data.mode.mode = asn1SccHTL_Mode_mode_float_mode1;
            }
            break;

        case asn1SccBalloon_Mode_off:
            ctxt.data.mode.mode = asn1SccHTL_Mode_mode_off;
            ctxt.finalize = true;
            break;

        default:
            debug_printf(LVL_WARN, "[HTL-Manager] Unknown mode");
            break;
        }

        if (prev_balloon_mode != asn1SccBalloon_Mode_flight_float &&
            ctxt.balloon_mode == asn1SccBalloon_Mode_flight_float) {
            ctxt.float_mode_start_time_secs = int (time_management::mission_time());
            std::cout << "-- START TIME in FLOAT -------- " << ctxt.float_mode_start_time_secs << std::endl;
        }
    }

    inline void process_air_pressure() {
        asn1SccEnvLab_Data el_data;
        htl_manager_RI_Get_EL_Data(&el_data);
        auto p_air {
            (el_data.payload.pressure_data.abs_barometers.arr[0].pressure_mbar +
             el_data.payload.pressure_data.abs_barometers.arr[1].pressure_mbar) / 2.0F
        };

        constexpr float p_ref1 = {200};

        switch (ctxt.data.mode.mode) {
            case asn1SccHTL_Mode_mode_ascent_mode1:
            case asn1SccHTL_Mode_mode_ascent_mode2:
                ctxt.data.mode.mode = p_air < p_ref1 ? asn1SccHTL_Mode_mode_ascent_mode2 : asn1SccHTL_Mode_mode_ascent_mode1;
                break;

            case asn1SccHTL_Mode_mode_float_mode1: {
                int currt_time_secs {int (time_management::mission_time())};
                auto delta_time {currt_time_secs - ctxt.float_mode_start_time_secs};
                std::cout << "Delta time: " << delta_time << "\n";
                if (delta_time > htl_manager_ctxt.delta_time) {
                    std::cout << "-- TO Float MODE 2 ----\n";
                    ctxt.data.mode.mode = asn1SccHTL_Mode_mode_float_mode2;
                }
            } break;

            case asn1SccHTL_Mode_mode_float_mode2:
                std::cout << "-- IN Float MODE 2 ----\n";
                break;

            default:
                break;
        }
    }

    inline void update_time() {
        auto && [secs, usecs]   = time_management::absolute_time();
        ctxt.data.snapshot_time = {secs, usecs};
        ctxt.data.mission_time  = time_management::mission_time();
    }

    inline void update_thermistors_data() {
        using namespace board_support::tmu;
        for (uint8_t pt1000 = PT1000_1; pt1000 <= PT1000_28; pt1000++) {
            ctxt.data.payload.thermistors.arr[pt1000] = readPT1000LineFrom(static_cast<PT1000_ID>(pt1000), 5U);
            if (ctxt.data.payload.thermistors.arr[pt1000] == -1) {
                debug_printf(LVL_ERROR, "[HTL-Manager] FAILED to read PT1000\n");
                break;
            }
        }
        debug_printf(LVL_INFO, "[HTL-Manager] PT1000 data OK\n");
    }

    inline void control_heaters() {
        /**
         * Control the four heaters based on HTL mode:
         */
        for (size_t heater  = asn1SccHTL_Heater_ID_experiment1_heater;
                    heater <= asn1SccHTL_Heater_ID_experiment4_heater;
                    heater++)
        {
            auto h_id {static_cast<asn1SccHTL_Heater_ID>(heater)};
            htl_manager_RI_Control_Heater(&h_id, &ctxt.data.mode);
        }
    }

    inline void update_heaters_status() {
        htl_manager_RI_Get_Heaters_Status
            (&ctxt.data.payload.heaters.arr[asn1SccHTL_Heater_ID_experiment1_heater],
             &ctxt.data.payload.heaters.arr[asn1SccHTL_Heater_ID_experiment2_heater],
             &ctxt.data.payload.heaters.arr[asn1SccHTL_Heater_ID_experiment3_heater],
             &ctxt.data.payload.heaters.arr[asn1SccHTL_Heater_ID_experiment4_heater],
             &ctxt.data.mode.heaters_mode);
    }

    /**
     * Saves the logged data to disk (uSD) flushing internal buffers.
     * This activity is performed every 6 cycles,
     * and 1 cycle has a period of 10 second.
     * Thus, data is safeguarded every 60 seconds.
     */
    inline void safeguard_data() {
        static uint8_t cycle {1U};
        if (cycle == 6U) {
            ctxt.logfile.save_file();
            cycle = 1U;
        }
        cycle++;
    }

    inline void store_data() {
        using data_storage::basic_images::image;
        using namespace board_support::tmu;

        ctxt.logfile.add_line({
            // == Timestamps & modes (8 cols) ==
            image(static_cast<int32_t>(ctxt.data.snapshot_time.secs)),  // Snapshot time [secs]
            image(static_cast<int32_t>(ctxt.data.snapshot_time.usecs)), // Snapshot time [msecs]
            image(static_cast<float>(ctxt.data.mission_time)),          // Mission  time [secs]
            ctxt.image(ctxt.data.mode.mode),                            // HTL Mode
            ctxt.image(ctxt.data.mode.heaters_mode.experiment_1),       // Heater 1 Mode
            ctxt.image(ctxt.data.mode.heaters_mode.experiment_2),       // Heater 2 Mode
            ctxt.image(ctxt.data.mode.heaters_mode.experiment_3),       // Heater 3 Mode
            ctxt.image(ctxt.data.mode.heaters_mode.experiment_4),       // Heater 4 Mode

            // == HTL sensor Data (28 cols) ==
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_1])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_2])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_3])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_4])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_5])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_6])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_7])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_8])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_9])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_10])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_11])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_12])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_13])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_14])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_15])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_16])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_17])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_18])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_19])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_20])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_21])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_22])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_23])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_24])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_25])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_26])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_27])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[PT1000_28])),

            // == HTL heaters Data (4 cols) ==
            image(static_cast<float>(ctxt.data.payload.heaters.arr[0])),
            image(static_cast<float>(ctxt.data.payload.heaters.arr[1])),
            image(static_cast<float>(ctxt.data.payload.heaters.arr[2])),
            image(static_cast<float>(ctxt.data.payload.heaters.arr[3])),
        });
        safeguard_data();
    }

    inline void finalize() {
        board_support::tmu::finalize(); // ALWAYS finalize TMU

        static bool finalized {false};
        if (!finalized) {
            update_time();
            control_heaters(); // To switch them OFF
            update_heaters_status();

            store_data();
            ctxt.logfile.save_file();

            htl_manager_RI_Put_HTL_Data(&ctxt.data);

            static asn1SccBalloon_Events i_have_finished {asn1SccBalloon_Events_htl_finished};
            htl_manager_RI_Notify_Event(&i_have_finished);

            finalized = true;
            debug_printf(LVL_INFO, "[HTL-Manager] Finished!");
        }
    }
}

/*******************************************************************************
 * Public functions:
 ******************************************************************************/

void htl_manager_startup(void) {
   ctxt.data.mode.mode = asn1SccHTL_Mode_mode_off;
   debug_printf(LVL_INFO, "[HTL-Manager] Initialized");
}

void htl_manager_PI_Measure_And_Handle(void) {
    if (!ctxt.finalize) {
        process_balloon_mode();
        switch (ctxt.data.mode.mode) {
            case asn1SccHTL_Mode_mode_off:
                debug_printf(LVL_INFO, "[HTL-Manager] Mode-OFF\n");
                update_time();
                control_heaters();
                update_heaters_status();
                break;

            case asn1SccHTL_Mode_mode_pre_launch:
                debug_printf(LVL_INFO, "[HTL-Manager] Mode-PRE-LAUNCH\n");
                board_support::tmu::initialize();
                update_time();
                update_thermistors_data();
                control_heaters();
                update_heaters_status();
                break;

            default: // NOMINAL activity for ASCENT and FLOAT modes
                debug_printf(LVL_INFO, "[HTL-Manager] Mode-[ASCENT | FLOAT]\n");
                board_support::tmu::initialize();
                update_time();
                update_thermistors_data();
                process_air_pressure();
                control_heaters();
                update_heaters_status();
                store_data();
                break;
        }
        htl_manager_RI_Put_HTL_Data(&ctxt.data);
    }

    if (ctxt.finalize) {
        finalize();
    }
}

void htl_manager_PI_Reset_HTL_I2C_Device
    (const asn1SccRestartable_Device_ID *IN_device_id)
{
    if (*IN_device_id == Restartable_Device_ID_tmu_adc) {
        board_support::tmu::resetADC();
    } else {
        debug_printf(LVL_WARN, "[HTL-Manager] Unknown device ID for RESET\n");
    }
}
