/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                              COMMUNICATION_IF                              --
--                                                                            --
--                              TM_Sender Source                              --
--                                                                            --
--            Copyright (C) 2023 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "communication_if_tm_sender.h"

// TASTE headers:

#include "dataview-uniq.h"    // DV
#include "debug_messages.h"   // debug_printf
#include "PrintTypesAsASN1.h" // PrintASN1*
#include "Context-communication-if.h"

// Linux headers:

#include <stdio.h>
#include <netinet/in.h> // socket
#include <unistd.h>     // close
#include <netdb.h>      // gethostbyname
#include <time.h>       // time

/*******************************************************************************
 * Local constants: Connection configuration, etc.
 ******************************************************************************/

#define GS_TM_PORT    (communication_if_ctxt.gs_tm_port)
#define GS_IP_ADDRESS (communication_if_ctxt.gs_ip_address)

#define NAME "[COMM-IF - TM Sender] "

/*******************************************************************************
 * Internal state
 ******************************************************************************/

static int write_socket = -1;
static struct sockaddr_in gs_address;

/*******************************************************************************
 * Local operations
 ******************************************************************************/

static void create_udp_socket() {
    write_socket = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (write_socket < 0) {
        debug_printf(LVL_ERROR, NAME"FAILED to OPEN UDP socket\n");
    } else {
        debug_printf(LVL_INFO, NAME"CREATED UDP socket\n");
    }
}

static void setup_gs_address() {
    struct hostent *host_info = gethostbyname(GS_IP_ADDRESS);
    gs_address.sin_addr = *(struct in_addr *)host_info->h_addr;
    gs_address.sin_port = htons(GS_TM_PORT);
    gs_address.sin_family = PF_INET;
}

static bool send_tm(const unsigned char* tm, size_t tm_len) {
    size_t sent_bytes = sendto(
        write_socket, tm, tm_len, 0,
        (struct sockaddr *) &gs_address, sizeof(gs_address)
    );

    if (sent_bytes <= 0) {
        debug_printf(LVL_ERROR, NAME"FAILED to SEND TM\n");
        return false;
    }

    return true;
}

/*******************************************************************************
 * Provided Interfaces (Public operations)
 ******************************************************************************/

bool tm_sender_initialize() {
    static bool initialized = false;

    if (!initialized) {
        setup_gs_address();
        create_udp_socket();
        initialized = write_socket > 0;
    }

    return initialized;
}

bool tm_sender_send_sc_tm(const char * sc_tm)
{
    tm_sender_initialize();

    static unsigned char buffer [asn1SccSC_TM_Type_REQUIRED_BYTES_FOR_ACN_ENCODING];

    BitStream bitstream;
    BitStream_Init(&bitstream, buffer, asn1SccSC_TM_Type_REQUIRED_BYTES_FOR_ACN_ENCODING);

    int err = 0;
    asn1SccSC_TM_Type *sc = (asn1SccSC_TM_Type *) sc_tm;
    sc->timestamp_secs = time(NULL);

    printf(NAME"Sending SC TM # %d\n", (int)(sc->sequence_number));

    asn1SccSC_TM_Type_ACN_Encode(sc, &bitstream, &err, true);
    return send_tm(buffer, asn1SccSC_TM_Type_REQUIRED_BYTES_FOR_ACN_ENCODING);
}

bool tm_sender_send_hk_tm(const char * hk_tm)
{
    tm_sender_initialize();

    static unsigned char buffer [asn1SccHK_TM_Type_REQUIRED_BYTES_FOR_ACN_ENCODING];

    BitStream bitstream;
    BitStream_Init(&bitstream, buffer, asn1SccHK_TM_Type_REQUIRED_BYTES_FOR_ACN_ENCODING);

    int err = 0;
    asn1SccHK_TM_Type *hk = (asn1SccHK_TM_Type *) hk_tm;
    hk->timestamp_secs = time(NULL);

    printf(NAME"Sending HK TM # %d\n", (int)(hk->sequence_number));

    asn1SccHK_TM_Type_ACN_Encode(hk, &bitstream, &err, true);
    return send_tm(buffer, asn1SccHK_TM_Type_REQUIRED_BYTES_FOR_ACN_ENCODING);
}
