/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                              COMMUNICATION_IF                              --
--                                                                            --
--                             TC_Receiver Source                             --
--                                                                            --
--            Copyright (C) 2023 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/
#include "communication_if_tc_receiver.h"

// Tasking headers:
#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1 // See feature_test_macros(7)
#endif
#include <sched.h>    // CPU_ZERO, CPU_SET, affinity
#include <pthread.h>  // pthread_*
#include <stdio.h>    // perror
#include <sys/time.h> // timespec
#include <math.h>     // modff

// TASTE headers:

#include "dataview-uniq.h"
#include "debug_messages.h"
#include "PrintTypesAsASN1.h"
#include "Context-communication-if.h"

#include "partition_obsw_polyorb_interface.h"

// Networking headers:

#include <netinet/in.h> // socket
#include <unistd.h>     // close
#include <netdb.h>      // gethostbyaddr

/*******************************************************************************
 * Local constants: Connection configuration, etc.
 ******************************************************************************/

#define TC_PORT (communication_if_ctxt.my_tc_port)

#define TC_TASK_PRIO (1)
#define TC_TASK_CORE (1)
#define TC_TASK_MIAT (1.0F) // SECONDS

#define NAME "[COMM_IF - TC Receiver] "


/*******************************************************************************
 * Required interfaces
 ******************************************************************************/

/** ASN.1 Type and encoding of the parameters:
 *  IN_tmtc_mode type: TMTC-Mode ; encoding: NATIVE
 */
extern void vm_communication_if_set_internal_mode
    (asn1SccPID dest_pid,
     const char *IN_tmtc_mode, size_t IN_tmtc_mode_len);

/**
 * ASN.1 Type and encoding of the parameters:
 * tc type: TC-Type ; encoding: NATIVE
 *
 * We cannot call vm_communication_if_process_tc since
 * it checks the invoker thread to call the Manager_Facade.
 *
 * This function is based on vm_communication_if_process_tc
 * but removes the constraints so that the sporadic call is not discarded.
 */
static void process_tc(const asn1SccTC_Type * tc) {
    PrintASN1TC_Type(">>> [process_tc] Received TC: ", tc);
    debug_printf(LVL_INFO, "\n"NAME"Forwarding TC to Manager");

    const asn1SccTC_Type *IN_buf_tc = tc;
    const size_t size_IN_buf_tc = sizeof(*tc);

    __po_hi_request_t *request =
        __po_hi_get_request (tm_sender_send_tm_global_outport_communication_if_process_tc_manager_facade_process_tc
    );

    __po_hi_copy_array(
        &(request->vars.tm_sender_send_tm_global_outport_communication_if_process_tc_manager_facade_process_tc.tm_sender_send_tm_global_outport_communication_if_process_tc_manager_facade_process_tc.buffer),
        (void *)IN_buf_tc, size_IN_buf_tc
    );

    request->vars.tm_sender_send_tm_global_outport_communication_if_process_tc_manager_facade_process_tc.tm_sender_send_tm_global_outport_communication_if_process_tc_manager_facade_process_tc.length =
    size_IN_buf_tc;

    request->port = tm_sender_send_tm_global_outport_communication_if_process_tc_manager_facade_process_tc;

    __po_hi_gqueue_store_out(partition_obsw_tm_sender_send_tm_k, tm_sender_send_tm_local_outport_communication_if_process_tc_manager_facade_process_tc, request);
    __po_hi_send_output(partition_obsw_tm_sender_send_tm_k, tm_sender_send_tm_global_outport_communication_if_process_tc_manager_facade_process_tc);
}

/**
 * ASN.1 Type and encoding of the parameters:
 * event type: Balloon-Events ; encoding: NATIVE
 *
 * We cannot call vm_communication_if_process_tc since
 * it checks the invoker thread to call the Manager_Facade.
 *
 * This function is based on vm_communication_if_notify_event
 * but removes the constraints so that the sporadic call is not discarded.
 */
static void notify_event(const asn1SccBalloon_Events * event) {
    const asn1SccBalloon_Events *IN_buf_balloon_event = event;
    const size_t size_IN_buf_balloon_event = sizeof(*event);

    PrintASN1Balloon_Events(">>> [notify_event] Received Event: ", event);
    debug_printf(LVL_INFO, "\n"NAME"Forwarding Event to Manager\n");

    __po_hi_request_t *request = __po_hi_get_request (tm_sender_send_tm_global_outport_communication_if_notify_event_manager_facade_notify_event);
    __po_hi_copy_array(&(request->vars.tm_sender_send_tm_global_outport_communication_if_notify_event_manager_facade_notify_event.tm_sender_send_tm_global_outport_communication_if_notify_event_manager_facade_notify_event.buffer),
    (void *)IN_buf_balloon_event, size_IN_buf_balloon_event);
    request->vars.tm_sender_send_tm_global_outport_communication_if_notify_event_manager_facade_notify_event.tm_sender_send_tm_global_outport_communication_if_notify_event_manager_facade_notify_event.length =
    size_IN_buf_balloon_event;
    request->port = tm_sender_send_tm_global_outport_communication_if_notify_event_manager_facade_notify_event;
    __po_hi_gqueue_store_out(partition_obsw_tm_sender_send_tm_k, tm_sender_send_tm_local_outport_communication_if_notify_event_manager_facade_notify_event, request);
    __po_hi_send_output(partition_obsw_tm_sender_send_tm_k, tm_sender_send_tm_global_outport_communication_if_notify_event_manager_facade_notify_event);
}

/*******************************************************************************
 * Local types
 ******************************************************************************/

typedef enum Socket_Status {
    SS_OK = 0,
    SS_NOT_CREATED  = 1,
    SS_UNCONFIGURED = 2,
    SS_DISCONNECTED = 3
} Socket_Status;

/*******************************************************************************
 * Local operations
 ******************************************************************************/

void timespec_add(struct timespec *t1, struct timespec *t2, struct timespec *result) {
    const long BILLION = 1000000000;
    long sec  = t2->tv_sec + t1->tv_sec;
    long nsec = t2->tv_nsec + t1->tv_nsec;
    if (nsec >= BILLION) {
        nsec -= BILLION;
        sec++;
    }
    result->tv_sec = sec;
    result->tv_nsec = nsec;
}

struct timespec seconds_to_timespec(float seconds) {
    struct timespec ts;
    float intPart;
    ts.tv_nsec = (int) (modff(seconds, &intPart) * 1.0E09);
    ts.tv_sec = intPart;
    return ts;
}

/**
 * @brief creates a TCP socket.
 * @param sock output variable that contains the created socket.
 */
static int create_tcp_socket(int * sock) {
    *sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

    // Return inmediately if failed to create socket
    if (*sock < 0) {
        debug_printf(LVL_ERROR, NAME"Could not open TCP socket\n");
        return 1;
    }

    debug_printf(LVL_INFO, NAME"TCP socket created successfully\n");
    return 0;
}

static int config_tcp_socket(const int server_socket, uint16_t port) {
    // 1. Make address & port reusable:
    int option = 1;
    if (setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option)) < 0) {
        debug_printf(LVL_ERROR, NAME"Could not make ADDRESS reusable\n");
        return 1;
    }

    if (setsockopt(server_socket, SOL_SOCKET, SO_REUSEPORT, &option, sizeof(option)) < 0) {
        debug_printf(LVL_ERROR, NAME"Could not make PORT reusable\n");
        return 1;
    }

    // 2. Bind the given port
    struct sockaddr_in server_address = {
        .sin_family = PF_INET,
        .sin_port   = htons(port),        /**< Listen on given port */
        .sin_addr   = {htonl(INADDR_ANY)} /**< Listen on all addresses */
    };

    if (bind(server_socket, (struct sockaddr *) &server_address, sizeof(server_address)) < 0) {
        debug_printf(LVL_ERROR, NAME"Could not bind TCP socket to address\n");
        close(server_socket);
        return 1;
    }

    // 3. Config as a listener socket
    if (listen(server_socket, 1) < 0) {
        debug_printf(LVL_ERROR, NAME"Could not configure TCP socket as a listener\n");
        close(server_socket);
        return 1;
    }

    debug_printf(LVL_INFO, NAME"TCP socket configured successfully\n");
    return 0;
}

/**
 * @brief Blocking operation that returns when a connection with a client is accepted
 * @return 1 if error, 0 if successfull
 */
static int connect_to_client(const int server_socket, int * client_socket) {
    struct sockaddr_in dir_cliente;
    unsigned int tam_dir;
    *client_socket = accept(server_socket, (struct sockaddr *) &dir_cliente, &tam_dir);
    if (*client_socket < 0) {
        debug_printf(LVL_ERROR, NAME"Could not accept connection from client\n");
        return 1;
    }
    debug_printf(LVL_INFO, NAME"Connection with client accepted\n");
    return 0;
}

static int recv_tc(const int conn_socket, asn1SccTC_Type * tc) {
    static unsigned char buf [asn1SccTC_Type_REQUIRED_BYTES_FOR_ACN_ENCODING];
    int read_bytes =
        recv(conn_socket, buf,
             asn1SccTC_Type_REQUIRED_BYTES_FOR_ACN_ENCODING,
             MSG_WAITALL);

    debug_printf(LVL_INFO, NAME"Read TC %d bytes\n", read_bytes);

    if (read_bytes < 0) {
        return 1; /**< @todo when? */
    } else if (read_bytes == 0) {
        return 2; /**< disconnected client */
    }

    static BitStream bitstream;
    BitStream_AttachBuffer(&bitstream, buf, asn1SccTC_Type_REQUIRED_BYTES_FOR_ACN_ENCODING);

    int err_code;
    return asn1SccTC_Type_ACN_Decode(tc, &bitstream, &err_code) ? 0 : 3;
}

static void notify_disconnection() {
    // 1. Notify disconnection to the TTC by updating the TMTC status:
    asn1SccTMTC_Mode mute = asn1SccTMTC_Mode_mute;
    vm_communication_if_set_internal_mode(PID_tmtc_internal_mode, (const char *) &mute, sizeof(mute));

    // 2. Notify disconnection to the manager:
    asn1SccBalloon_Events lost_comm = asn1SccBalloon_Events_lost_comm;
    notify_event(&lost_comm);
}

static void * tc_manager_task(void * arg) {
    (void) arg;

    debug_printf(LVL_INFO, NAME"Initializing & config connection...\n");

    int tcp_socket = -1;
    Socket_Status status = SS_NOT_CREATED;
    status = create_tcp_socket(&tcp_socket) == 0
           ? SS_OK : SS_NOT_CREATED;

    status = config_tcp_socket(tcp_socket, TC_PORT) == 0
           ? SS_OK : SS_UNCONFIGURED;

    int gs_socket = -1;
    status = connect_to_client(tcp_socket, &gs_socket) == 0
           ? SS_OK : SS_DISCONNECTED;

    int disconnection_counts = 0;

    // For timing:
    struct timespec miat = seconds_to_timespec(TC_TASK_MIAT);
    struct timespec last;

    // Sporadic activity:
    while (true) {
        // 1. If the socket could not be created nor configured, retry it!
        switch (status) {
            case SS_NOT_CREATED:
                status = create_tcp_socket(&tcp_socket) == 0
                       ? SS_OK : SS_NOT_CREATED;
                // pass through and configure it!
            case SS_UNCONFIGURED:
                status = config_tcp_socket(tcp_socket, TC_PORT) == 0
                       ? SS_OK : SS_UNCONFIGURED;
                // pass through and connect to the client!
            case SS_DISCONNECTED:
                status = connect_to_client(tcp_socket, &gs_socket) == 0
                       ? SS_OK : SS_DISCONNECTED;
                break;
            default:
                break;
        }

        // 2. BLOCKING CALL that waits for a TC reception
        asn1SccTC_Type tc;
        int recv_err = recv_tc(gs_socket, &tc);

        /// Gets the time when this task was awaken
        if (clock_gettime(CLOCK_MONOTONIC, &last) < 0) {
            perror(NAME"clock_gettime");
        }

        // 3. Process possible errors while receiving a TC
        bool possible_disconnection = recv_err == 1 || recv_err == 2;
        if (possible_disconnection) {
            if (disconnection_counts < 5) {
                disconnection_counts++;
            } else if (status != SS_DISCONNECTED) {
                close(gs_socket);         ///< close connection with client
                status = SS_DISCONNECTED; ///< update the local status to disconnected
                notify_disconnection();
            }
        } else { /**< TC received successfully */
        // 4. Send received TC to the Manager for its processing
            status = SS_OK;
            disconnection_counts = 0;
            process_tc(&tc);
        }

        /// @addtogroup Sleeps for remining time
        /// @{
        timespec_add(&last, &miat, &last);
        if (clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &last, 0) < 0) {
            perror(NAME"clock_nanosleep");
        }
        /// }@
    }
}


/*******************************************************************************
 * Provided Interfaces (Public operations)
 ******************************************************************************/

bool tc_receiver_initialize() {
    pthread_attr_t attr;
    pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);

    pthread_t tid;
    int err = pthread_create(&tid, NULL, (void *(*)(void *)) tc_manager_task, NULL);
    if (err != 0) {
        perror("pthread_create");
        return false;
    }

    struct sched_param param;
    param.sched_priority = TC_TASK_PRIO;
    if (pthread_setschedparam(tid, SCHED_FIFO, &param) != 0) {
        perror(NAME"pthread_setschedparam");
    }

    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(TC_TASK_CORE, &cpuset);
    if (pthread_setaffinity_np(tid, sizeof(cpuset), &cpuset) != 0) {
        perror(NAME"pthread_setaffinity");
    }

    return true;
}
