/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                              COMMUNICATION_IF                              --
--                                                                            --
--                              TM_Sender Source                              --
--                                                                            --
--            Copyright (C) 2023 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "ft-udp-clt-cwrapper.h"

#include "ft-udp-clt.h"     // FT_UDP_Client

// TASTE headers:

#include "dataview-uniq.h"  // DV
#include "debug_messages.h" // debug_printf

// Linux headers:

#include <time.h>           // time

extern "C" {

/*******************************************************************************
 * Local constants: Connection configuration, etc.
 ******************************************************************************/

#define NAME "[COMM-IF - TM Sender - UDP] "

/*******************************************************************************
 * Internal state
 ******************************************************************************/

namespace {
    FT_UDP_Client udp_client;
}

/*******************************************************************************
 * Provided Interfaces (Public operations)
 ******************************************************************************/

bool udp_clt_initialize(const char *GS_IP_ADDRESS, int GS_TM_PORT) {
    static bool initialized = false;
    if (!initialized) {
        initialized = udp_client.initialize(GS_IP_ADDRESS, GS_TM_PORT);
    }
    return initialized;
}

bool udp_clt_send_tm(const unsigned char* tm, size_t tm_len) {
    auto ret_code = udp_client.send(tm, tm_len);
    switch (ret_code) {
        case FT_UDP_Client::OK:
            break;
        case FT_UDP_Client::SOCKET_NOT_CREATED:
            debug_printf(LVL_ERROR, NAME"SOCKET NOT CREATED\n");
            debug_printf(LVL_INFO,  NAME"RETRYING socket CREATION\n");
            udp_client.create_socket();
            break;
        case FT_UDP_Client::FAILED_TO_SEND:
            debug_printf(LVL_ERROR, NAME"FAILED to SEND TM\n");
            break;
    }
    return ret_code == FT_UDP_Client::OK;
}

}
