/***************************************************************************//**
 *  @file	ft-udp-clt.h
 *  @brief	Fault Tolerant UDP Client
 *
 *  This header file contains all definitions and implementations for the
 *  Fault-Tolerant UDP Client class.
 * 
 *  @date	09/06/2023
 *
 *******************************************************************************
 *  @section License
 *  <b> Copyright (C) 2023 Ángel Grover Pérez Muñoz <agperez@datsi.upm.es> </b>
 *  <b> Copyright (C) 2023 Universidad Politécnica de Madrid </b>
 *******************************************************************************
 *
 *  HERCCULES was developed by the Real-Time Systems Group at  the Universidad
 *  Politécnica de Madrid.
 *                                                                           
 *  HERCCULES is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Soft-
 *  ware Foundation, either version 3 of the License,  or (at your option) any
 *  later version.                                                            
 *                                                                           
 *  HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT-
 *  NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details. You should have received a copy of the GNU General Public License
 *  along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.        
 ******************************************************************************/

#ifndef FT_UDP_CLIENT_H
#define FT_UDP_CLIENT_H

#include <netdb.h>      // gethostbyname, inaddr
#include <netinet/in.h> // socket
#include <stdio.h>
#include <stdlib.h>     // atoi
#include <unistd.h>     // close
#include <errno.h>
#include <arpa/inet.h>  // pton
#include <string.h>     // mem{set,copy}

#include "debug_messages.h" // debug_printf

/**
 * Criteria:
 *  - If send() fails, connection is lost.
 *  - Connection can be also checked invoking is_connected().
 *  - Reconnection MUST be explicitly performed by the user calling reconnect().
 */
class FT_UDP_Client {

public:

    /**********************************************************************//**
     * P U B L I C    O P E R A T I O N S
     *************************************************************************/

    ~FT_UDP_Client() {
        finalize();
    }    

    bool initialize(const char *server_ip, int server_port) {
        _svr_ip   = server_ip;
        _svr_port = server_port;

       bool created  = create_socket();
       bool setup_ok = setup_svr_address();
       return created && setup_ok;
    }

    /**
     * If socket was previously created, nothing is performed.
     */
    bool create_socket() {
        if (_clt_sock == -1) {
            _clt_sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
            if (_clt_sock == -1) {
                perror("[FT UDP Client] ERROR: create_socket");
                return false;
            } else {
                debug_printf(LVL_INFO, "[FT UDP Client] SUCCESS: create_socket");
            }
        }
        return true;
    }

    void finalize() {
        close(_clt_sock);
    }

    enum Send_Return_Code {
        OK,
        SOCKET_NOT_CREATED,
        FAILED_TO_SEND
    };

    Send_Return_Code send(const unsigned char *msg, size_t msg_len) {
        if (_clt_sock != -1) {
            ssize_t sent_bytes = sendto(
                _clt_sock, msg, msg_len, 0,
                (struct sockaddr *) &_svr_address, sizeof(_svr_address)
            );

            if (sent_bytes < 0) {
                debug_printf(LVL_ERROR, "[FT UDP Client] Could not send TM\n");
                return FAILED_TO_SEND;
            }

            debug_printf(LVL_INFO, "[FT UDP Client] Sent %d bytes out of %d bytes\n", sent_bytes, msg_len);
            return OK;
        }
        return SOCKET_NOT_CREATED;
    }

private:

    /**********************************************************************//**
     * P R I V A T E    M E M B E R S    &    T Y P E S
     *************************************************************************/

    int          _clt_sock {-1};
    const char * _svr_ip;
    int          _svr_port {-1};
    sockaddr_in  _svr_address;

    /**********************************************************************//**
     * P R I V A T E    O P E R A T I O N S
     *************************************************************************/

    bool setup_svr_address() {
        memset(&_svr_address.sin_addr , 0, sizeof(_svr_address.sin_addr));
        if (inet_pton(AF_INET, _svr_ip, &(_svr_address.sin_addr)) <= 0) {
            perror("[FT UDP Client] ERROR: Invalid server address");
            return false;
        }
        _svr_address.sin_family = AF_INET;
        _svr_address.sin_port = htons(_svr_port);

        return true;
    }
};

#endif // FT_UDP_CLIENT_H
