/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                              COMMUNICATION_IF                              --
--                                                                            --
--                             TC_Receiver Source                             --
--                                                                            --
--            Copyright (C) 2023 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "ft-tcp-svr-cwrapper.h"

#include "dataview-uniq.h" // DV
#include <stdbool.h>

#include "ft-tcp-svr.h"

extern "C" {

namespace {
    FT_TCP_Server tcp_svr;
}

bool tcp_svr_initialize(int TC_PORT) {
    return tcp_svr.initialize(TC_PORT);
}

bool tcp_svr_recv(unsigned char* tc_raw, size_t len) {
    size_t read_bytes {0U};
    return tcp_svr.recv(tc_raw, len, read_bytes);
}

bool tcp_svr_is_connected() {
    return tcp_svr.is_connected();
}

bool tcp_svr_reconnect() {
    return tcp_svr.reconnect();
}

}
