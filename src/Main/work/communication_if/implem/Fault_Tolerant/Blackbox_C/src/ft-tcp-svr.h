/***************************************************************************//**
 *  @file	ft-tcp-svr.h
 *  @brief	Fault Tolerant TCP Server
 * 
 *  This header file contains all definitions and implementations for the
 *  Fault-Tolerant TCP Server class.
 *
 *  @date	09/06/2023
 *
 *******************************************************************************
 *  @section License
 *  <b> Copyright (C) 2023 Ángel Grover Pérez Muñoz <agperez@datsi.upm.es> </b>
 *  <b> Copyright (C) 2023 Universidad Politécnica de Madrid </b>
 *******************************************************************************
 *
 *  HERCCULES was developed by the Real-Time Systems Group at  the Universidad
 *  Politécnica de Madrid.
 *                                                                           
 *  HERCCULES is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Soft-
 *  ware Foundation, either version 3 of the License,  or (at your option) any
 *  later version.                                                            
 *                                                                           
 *  HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT-
 *  NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details. You should have received a copy of the GNU General Public License
 *  along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.        
 ******************************************************************************/

#ifndef FT_TCP_SERVER_H
#define FT_TCP_SERVER_H

#include <netdb.h>  // inaddr
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h> // atoi
#include <unistd.h> // close
#include <errno.h>
#include <string.h>
#include "debug_messages.h" //debug_printf

#define NAME "[FT TCP Server] "

/**
 * Criteria:
 *  - If recv() or recv_line() fails, connection is lost.
 *  - Connection can be also checked invoking is_connected().
 *  - Reconnection MUST be explecitely performed by the user calling reconnect().
 */
class FT_TCP_Server {

public:

    /**********************************************************************//**
     * P U B L I C    O P E R A T I O N S
     *************************************************************************/

    bool initialize(int server_port) {
        _svr_port = server_port;
       setup_socket();
       return is_connected();
    }

    ~FT_TCP_Server() {
        finalize();
    }

    bool finalize() {
        disconnect_from_client();

        if (!_srv_status.is_created) {
            close(_svr_sock);
            _srv_status.is_created = false;
            _srv_status.is_binded  = false;
            _srv_status.can_listen = false;
        }
        
        return true;
    }

    bool is_connected() {
        if (is_physicall_connection_up()) {
            puts(NAME"Phyisical connection is up");
        } else {
            puts(NAME"Phyisical connection is down");
        }
        return _srv_status.is_connected && is_physicall_connection_up();
    }

    /**
     * This function works only if connection with client is available.
     * In such case: it performs a timed blocking call that receives a fixed-length message.
     *               The call is blocked up to RECV_TIMEOUT_SECONDS seconds.
     * @param[out] msg
     *   The received message as output parameter.
     * @param[in]  msg_len
     *   The requested message length in bytes to read.
     * @param[out] read_bytes
     *   The number of received bytes.
     * @return
     *   True if successful, false otherwise.
     */
    bool recv(unsigned char *msg, size_t msg_len, size_t &read_bytes_size) {
        int read_bytes = ::recv(_clt_sock, msg, msg_len, MSG_WAITALL);
        read_bytes_size = read_bytes;
        printf(NAME"%d\n", read_bytes);
        if (read_bytes == msg_len) {
            puts(NAME"OK!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            return true;
        } else if (read_bytes == 0) {
            puts(NAME"FIN!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            disconnect_from_client();
        } else if (read_bytes == -1) {
            puts(NAME"TIMEOUT!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }
        return false;
    }

    bool reconnect() {
        disconnect_from_client();
        connect_to_client();
        return is_connected();
    }

private:

    /**********************************************************************//**
     * P R I V A T E    M E M B E R S    &    T Y P E S
     *************************************************************************/

    int _svr_sock {-1};
    int _svr_port { 0};
    int _clt_sock {-1};

    struct ServerStatus {
        bool is_created   = false;
        bool is_binded    = false;
        bool can_listen   = false;
        bool is_connected = false;
    };
    ServerStatus _srv_status;

    const __time_t RECV_TIMEOUT_SECONDS {10};

    /**********************************************************************//**
     * P R I V A T E    O P E R A T I O N S
     *************************************************************************/
    
    void setup_socket() {
        create_socket();
        config_socket();
        connect_to_client();
    }

    /**
     * If socket was previously created, nothing is performed.
     */
    void create_socket() {
        if (!_srv_status.is_created) {
            /// Close if opened:
            if (_svr_sock != -1) {
                close(_svr_sock);
                _svr_sock = -1;
            }

            /// Try to create it:
            _svr_sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
            if (_svr_sock < 0) {
                _srv_status.is_created = false;
                perror(NAME"ERROR: create_socket");
            } else {
                _srv_status.is_created = true;
            }
        }
    }    

    /**
     * Configures the server socket.
     */
    void config_socket() {
        /// 1. Make address & port reusable
        #ifdef SO_REUSEADDR
        int option = 1;
        if (setsockopt(_svr_sock, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option)) < 0) {
            debug_printf(LVL_ERROR, NAME"Could not make ADDRESS reusable\n");
        }
        #endif

        #ifdef SO_REUSEPORT
        if (setsockopt(_svr_sock, SOL_SOCKET, SO_REUSEPORT, &option, sizeof(option)) < 0) {
            debug_printf(LVL_ERROR, NAME"Could not make PORT reusable\n");
        }
        #endif

        /// 2. Bind the given port
        if (!_srv_status.is_binded) {
            struct sockaddr_in server_address;
            server_address.sin_family      = PF_INET;
            server_address.sin_port        = htons(_svr_port);  ///< Listen on given port
            server_address.sin_addr.s_addr = htonl(INADDR_ANY); ///< Listen on all addresses
            
            if (bind(_svr_sock, (struct sockaddr *) &server_address, sizeof(server_address)) < 0) {
                debug_printf(LVL_ERROR, NAME"Could not bind TCP socket to address\n");
                _srv_status.is_binded = false;
            } else {
                _srv_status.is_binded = true;
            }
        }

        /// 3. Config as a listener socket
        if (!_srv_status.can_listen) {
            if (listen(_svr_sock, 3) < 0) {
                perror(NAME"listen");
                debug_printf(LVL_ERROR, NAME"Could not configure TCP socket as a listener\n");
                _srv_status.can_listen = false;
            } else {
                _srv_status.can_listen = true;
            }
        }

        debug_printf(LVL_INFO, NAME"TCP socket configured successfully\n");
    }

    /**
     * If connection was previously established, nothing is performed.
     */
    void connect_to_client() {
        if (!_srv_status.is_connected && is_physicall_connection_up()) {
            debug_printf(LVL_INFO, NAME"Trying to connect to client\n");
            struct sockaddr dir_client;
            socklen_t       dir_size = sizeof(dir_client);
            _clt_sock = accept(_svr_sock, &dir_client, &dir_size);
            perror(NAME"accept");
            if (_clt_sock < 0) {
                debug_printf(LVL_ERROR, NAME"Could not accept connection from client\n");
                _srv_status.is_connected = false;
            } else {
                debug_printf(LVL_INFO, NAME"Connection with client accepted\n");
                _srv_status.is_connected = true;
                // Set receive timeout
                struct timeval timeout {
                    .tv_sec = RECV_TIMEOUT_SECONDS,
                    .tv_usec = 0
                };
                setsockopt(_clt_sock, SOL_SOCKET, SO_RCVTIMEO,
                           (char *)&timeout, sizeof(timeout));
            }
        }
    }

    void disconnect_from_client() {
        if (_clt_sock > 0) {
            close(_clt_sock);
            _clt_sock = -1;
            _srv_status.is_connected = false;
        }
    }

    bool is_physicall_connection_up() {
        const char* file_path = "/sys/class/net/eth0/operstate";

        const size_t buffer_size = 81U;
        char buffer[buffer_size];

        FILE* file = fopen(file_path, "r");
        if (file == NULL) {
            perror("Failed to open file");
            return false;
        }

        fgets(buffer, buffer_size, file);
        fclose(file);

        // Remove the newline character from the buffer
        buffer[strcspn(buffer, "\n")] = '\0';

        const char* up_state = "up";
        return (strcmp(buffer, up_state) == 0);
    }
};

#endif // FT_TCP_SERVER_H
