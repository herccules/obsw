/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                              COMMUNICATION_IF                              --
--                                                                            --
--                              TM_Sender Source                              --
--                                                                            --
--            Copyright (C) 2023 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "communication_if_tm_sender.h"

// HERCCULES headers:

#include "ft-udp-clt-cwrapper.h"

// TASTE headers:

#include "dataview-uniq.h"    // DV
#include "debug_messages.h"   // debug_printf
#include "PrintTypesAsASN1.h" // PrintASN1*
#include "Context-communication-if.h"

// Linux headers:

#include <time.h>       // time
#include <stdio.h>      // printf

/*******************************************************************************
 * Local constants: Connection configuration, etc.
 ******************************************************************************/

#define GS_TM_PORT    ( communication_if_ctxt.gs_tm_port )
#define GS_IP_ADDRESS ((communication_if_ctxt.testing) ? (communication_if_ctxt.gs_ip_address_test) : (communication_if_ctxt.gs_ip_address))

#define NAME "[COMM-IF - TM Sender - FT] "

/*******************************************************************************
 * Provided Interfaces (Public operations)
 ******************************************************************************/

bool tm_sender_initialize(void) {    
    static bool initialized = false;
    if (!initialized) {
        printf(NAME"Initializing\n");
        initialized = udp_clt_initialize(GS_IP_ADDRESS, GS_TM_PORT);
        if (initialized) {
            printf(NAME"Initialized!!!\n");
        };
    }
    return initialized;
}

bool tm_sender_send_hk_tm(const char * hk_tm)
{
    tm_sender_initialize();

    static unsigned char buffer [asn1SccHK_TM_Type_REQUIRED_BYTES_FOR_ACN_ENCODING];

    BitStream bitstream;
    BitStream_Init(&bitstream, buffer, asn1SccHK_TM_Type_REQUIRED_BYTES_FOR_ACN_ENCODING);

    int err = 0;
    asn1SccHK_TM_Type *hk = (asn1SccHK_TM_Type *) hk_tm;
    hk->timestamp_secs = time(NULL);

    printf(NAME"Sending HK TM # %d\n", (int)(hk->sequence_number));

    asn1SccHK_TM_Type_ACN_Encode(hk, &bitstream, &err, true);
    return udp_clt_send_tm(buffer, asn1SccHK_TM_Type_REQUIRED_BYTES_FOR_ACN_ENCODING);
}

bool tm_sender_send_sc_tm(const char * sc_tm)
{
    tm_sender_initialize();

    static unsigned char buffer [asn1SccSC_TM_Type_REQUIRED_BYTES_FOR_ACN_ENCODING];

    BitStream bitstream;
    BitStream_Init(&bitstream, buffer, asn1SccSC_TM_Type_REQUIRED_BYTES_FOR_ACN_ENCODING);

    int err = 0;
    asn1SccSC_TM_Type *sc = (asn1SccSC_TM_Type *) sc_tm;
    sc->timestamp_secs = time(NULL);

    printf(NAME"Sending SC TM # %d\n", (int)(sc->sequence_number));

    asn1SccSC_TM_Type_ACN_Encode(sc, &bitstream, &err, true);
    return udp_clt_send_tm(buffer, asn1SccSC_TM_Type_REQUIRED_BYTES_FOR_ACN_ENCODING);
}
