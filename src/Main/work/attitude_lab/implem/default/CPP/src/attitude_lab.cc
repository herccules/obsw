/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                             Attitude_Lab Source                            --
--                                                                            --
--            Copyright (C) 2023 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

// C++ body file for function Attitude_Lab

#include "attitude_lab.h"
#include "attitude_lab_state.h"

#include "String.h" // String
#include "Images.h" // Image

#include "CsvWriter.h"

#include "Time_Management.h"

#include "debug_messages.h"

/*******************************************************************************
 * Private variables and constants:
 ******************************************************************************/

namespace {
    // Define and use function state inside this context structure
    // avoid defining global/static variable elsewhere
    attitude_lab_state ctxt;
}

/*******************************************************************************
 * Private functions:
 ******************************************************************************/

namespace {

    void update_time() {
        auto && [secs, usecs]   = time_management::absolute_time();
        ctxt.data.snapshot_time = {secs, usecs};
        ctxt.data.mission_time  = time_management::mission_time();
    }

    void update_measurements (const asn1SccAtt_Lab_Data_Measurements * measurements) {
        constexpr auto n_photodiodes {4U};
        for (size_t i = 0U; i < n_photodiodes; ++i) {
            ctxt.data.payload.photodiodes.arr[i] = measurements->photodiodes.arr[i];
        }

        constexpr auto n_thermistors {2U};
        for (size_t i = 0U; i < n_thermistors; ++i) {
            ctxt.data.payload.thermistors.arr[i] = measurements->thermistors.arr[i];
        }
    }

    /**
     * Saves the logged data to disk (uSD) flushing internal buffers.
     * This activity is performed every 60 cycles,
     * and 1 cycle has a period of 1 second.
     * Thus, data is safeguarded every 60 seconds.
     */
    void safeguard_data() {
        static int8_t cycle {1U};
        if (cycle == 60U) {
            ctxt.logfile.save_file();
            cycle = 1U;
        }
        cycle++;
    }

    void store_data() {
        using data_storage::basic_images::image;
        ctxt.logfile.add_line({
            // == Timestamps & mode (4 cols) ==
            image(static_cast<int32_t>(ctxt.data.snapshot_time.secs)),
            image(static_cast<int32_t>(ctxt.data.snapshot_time.usecs)),
            image(static_cast<float>(ctxt.data.mission_time)),
            attitude_lab_state::
            image(ctxt.data.mode),

            // == Photodiodes (4 cols) ==
            image(static_cast<int16_t>(ctxt.data.payload.photodiodes.arr[0])),
            image(static_cast<int16_t>(ctxt.data.payload.photodiodes.arr[1])),
            image(static_cast<int16_t>(ctxt.data.payload.photodiodes.arr[2])),
            image(static_cast<int16_t>(ctxt.data.payload.photodiodes.arr[3])),

            // == Thermistors (2 cols) ==
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[0])),
            image(static_cast<int16_t>(ctxt.data.payload.thermistors.arr[1]))
        });

        safeguard_data();
    }

    /**
     * @todo invoke Juan B., Marina M.'s Nadir sensor algorithm
     */
    void compute_nadir_algorithm() {
        double julian_day {
            (time_management::absolute_time_secs() / 86400.0) + 2440587.5
        };
        // TODO
    }

    inline void publish_data() {
        attitude_lab_RI_Put_ATL_Data(&ctxt.data);
    }

    inline void finalize() {
        static bool finalized {false};
        if (!finalized) {
            ctxt.logfile.save_file();

            // Notify the Manager that this subsystem has finished:
            static asn1SccBalloon_Events finish_event {asn1SccBalloon_Events_atl_finished};
            attitude_lab_RI_Notify_Event(&finish_event);

            finalized = true;
        }
    }

    inline void send_basic_info() {
        update_time();
        publish_data();
    }
}

/*******************************************************************************
 * Public functions:
 ******************************************************************************/

void attitude_lab_startup(void) {    
    asn1SccAtt_Lab_Data_Initialize(&ctxt.data);
    debug_printf(LVL_INFO, "[Attitude Lab] Initialized!\n");
}

/**
 * Sets OUT_needs_measurements to true if this subsystems requires SDPU's measurements
 *
 * The decision is based on the current balloon mode (IN_balloon_mode).
 *
 * @param IN_balloon_mode Current balloon mode.
 * @param OUT_needs_measurements True if measurements are required.
 */
void attitude_lab_PI_ATL_Needs_Measurements
    (const asn1SccBalloon_Mode *IN_new_mode,
           asn1SccBoolean_Type *OUT_needs_measurement)
{
    switch (*IN_new_mode) {
        case asn1SccBalloon_Mode_ground_await:
            ctxt.data.mode = asn1SccAtt_Lab_Mode_off;
            send_basic_info();
            break;
        case asn1SccBalloon_Mode_ground_pre_launch:
        case asn1SccBalloon_Mode_flight_ascent:
        case asn1SccBalloon_Mode_flight_float:
            ctxt.data.mode = asn1SccAtt_Lab_Mode_on;
            break;
        case asn1SccBalloon_Mode_off:
            ctxt.data.mode = asn1SccAtt_Lab_Mode_off;
            ctxt.end_of_mission = true;
            send_basic_info();
            finalize();
            break;
    }

    *OUT_needs_measurement = (ctxt.data.mode == asn1SccAtt_Lab_Mode_on);
}

/**
 * Receives SDPU's measurements.
 *
 * @note SDPU's measurements SHOULD be received ONLY when needed.
 *
 * Therefore, the caller needs to invoke the "Needs_Measurements" operation
 * before invoking this one.
 */
void attitude_lab_PI_Send_ATL_Measurements
    (const asn1SccAtt_Lab_Data_Measurements *IN_measurements)
{
    if (!ctxt.end_of_mission) {        
        update_time();
        update_measurements(IN_measurements);
        store_data();
        attitude_lab_RI_Put_ATL_Data(&ctxt.data);
        compute_nadir_algorithm();
        publish_data();
    }
}
