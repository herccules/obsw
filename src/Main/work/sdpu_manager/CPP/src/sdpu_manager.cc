/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                            SDPU_Manager Source                             --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

// C++ body file for function SDPU_Manager

#include "sdpu_manager.h"
#include "sdpu_manager_state.h"

#include "SDPU.h" // Hardware I/F for SDPU board

#include "debug_messages.h"

using namespace board_support;

/*******************************************************************************
 * Private variables and constants:
 ******************************************************************************/

#define NAME "[SDPU Manager] "

namespace {
    sdpu_manager_state ctxt;
}

/*******************************************************************************
 * Private functions:
 ******************************************************************************/

namespace {

using namespace board_support::sdpu;

    inline void read_abs_baroms
        (PressureSensor IN_ps_ID,
         asn1SccAbsolute_Barometer &OUT_pressure)
    {
        if (BarometerReading br {}; getPressureFrom(IN_ps_ID, br)) {
            OUT_pressure.pressure_raw = br.pressure_raw;
            OUT_pressure.pressure_mbar = br.pressure_milliBar;
            OUT_pressure.temperature_raw = br.temperature_raw;
            OUT_pressure.temperature_celsius = br.temperature_celsius;
        }
    }

    // PYRG DOWN -- THERM 1
    // PYRA DOWN -- THERM 2
    // PYRG UPW  -- THERM 3
    // PYRA UPW  -- THERM 4
    inline void distribute_to_ATL_and_EL()
    {
        static asn1SccAtt_Lab_Data_Measurements atl_data;
        static asn1SccEnvLab_Experiment_Data_Sensors uel;
        static asn1SccEnvLab_Experiment_Data_Sensors del;
        static asn1SccEnvLab_Pressure_Data pressure;

        /**
         * Reads all analogue lines from all channels
         * where valid signals are present
         *
         * @{
         */
        do {
            auto reading {readAllADCChannels(MUXChannel::CH0, ctxt.n_retries_adc)};
            del.pyranometer_reading        = reading[0];
            if (reading[0] == -1) break;
            del.pyrgeometer_temperature    = reading[1];
            if (reading[1] == -1) break;
            pressure.dif_barometers.arr[0] = reading[2];
            if (reading[2] == -1) break;
            atl_data.photodiodes.arr[0]    = reading[3];
            if (reading[3] == -1) break;

            reading = readAllADCChannels(MUXChannel::CH1, ctxt.n_retries_adc);
            uel.pyranometer_reading        = reading[0];
            if (reading[0] == -1) break;
            del.pyranometer_temperature    = reading[1];
            if (reading[1] == -1) break;
            pressure.dif_barometers.arr[1] = reading[2];
            if (reading[2] == -1) break;
            atl_data.photodiodes.arr[1]    = reading[3];
            if (reading[3] == -1) break;

            reading = readAllADCChannels(MUXChannel::CH2, ctxt.n_retries_adc);
            del.pyrgeometer_reading        = reading[0];
            if (reading[0] == -1) break;
            uel.pyrgeometer_temperature    = reading[1];
            if (reading[1] == -1) break;
            pressure.dif_barometers.arr[2] = reading[2];
            if (reading[2] == -1) break;
            atl_data.photodiodes.arr[2]    = reading[3];
            if (reading[3] == -1) break;


            reading = readAllADCChannels(MUXChannel::CH3, ctxt.n_retries_adc);
            uel.pyrgeometer_reading        = reading[0];
            if (reading[0] == -1) break;
            uel.pyranometer_temperature    = reading[1];
            if (reading[1] == -1) break;
            pressure.dif_barometers.arr[3] = reading[2];
            if (reading[2] == -1) break;
            atl_data.photodiodes.arr[3]    = reading[3];
            if (reading[3] == -1) break;


            atl_data.thermistors.arr[0] = readRawFrom(PT1000_1, ctxt.n_retries_adc);
            if (atl_data.thermistors.arr[0] == -1) break;
            atl_data.thermistors.arr[1] = readRawFrom(PT1000_2, ctxt.n_retries_adc);
            if (atl_data.thermistors.arr[1] == -1) break;
        } while (false);
        /** }@ */

        /**
         * Reads all absolute barometers:
         * @{
         */
        read_abs_baroms(PS1, pressure.abs_barometers.arr[0]);
        read_abs_baroms(PS2, pressure.abs_barometers.arr[1]);
        /** }@ */

        sdpu_manager_RI_Send_Env_Lab_Measurements(&uel, &del, &pressure);
        sdpu_manager_RI_Send_ATL_Measurements(&atl_data);
    }

    inline void distribute_only_to_EL()
    {
        static asn1SccEnvLab_Experiment_Data_Sensors uel;
        static asn1SccEnvLab_Experiment_Data_Sensors del;
        static asn1SccEnvLab_Pressure_Data pressure;

        /**
         * Reads all analogue lines from all channels
         * where valid signals FROM the EL are present
         *
         * @{
         */
        do {
            uel.pyranometer_reading = readRawFrom(UP_PYRANOMETER, ctxt.n_retries_adc);
            if (uel.pyranometer_reading == -1) break;
            uel.pyranometer_temperature = readRawFrom(THERM_1, ctxt.n_retries_adc);
            if (uel.pyranometer_temperature == -1) break;
            pressure.dif_barometers.arr[0] = readRawFrom(DIFF_BAROM_1, ctxt.n_retries_adc);
            if (pressure.dif_barometers.arr[0] == -1) break;

            del.pyranometer_reading = readRawFrom(DOWN_PYRANOMETER, ctxt.n_retries_adc);
            if (del.pyranometer_reading == -1) break;
            del.pyranometer_temperature = readRawFrom(THERM_2, ctxt.n_retries_adc);
            if (del.pyranometer_temperature == -1) break;
            pressure.dif_barometers.arr[1] = readRawFrom(DIFF_BAROM_2, ctxt.n_retries_adc);
            if (pressure.dif_barometers.arr[1] == -1) break;

            uel.pyrgeometer_reading = readRawFrom(UP_PYRGEOMETER, ctxt.n_retries_adc);
            if (uel.pyrgeometer_reading == -1) break;
            uel.pyrgeometer_temperature = readRawFrom(THERM_3, ctxt.n_retries_adc);
            if (uel.pyrgeometer_temperature == -1) break;
            pressure.dif_barometers.arr[2] = readRawFrom(DIFF_BAROM_3, ctxt.n_retries_adc);
            if (pressure.dif_barometers.arr[2] == -1) break;

            del.pyrgeometer_reading = readRawFrom(DOWN_PYRGEOMETER, ctxt.n_retries_adc);
            if (del.pyrgeometer_reading == -1) break;
            del.pyrgeometer_temperature = readRawFrom(THERM_4, ctxt.n_retries_adc);
            if (del.pyrgeometer_temperature == -1) break;
            pressure.dif_barometers.arr[3] = readRawFrom(DIFF_BAROM_4, ctxt.n_retries_adc);
            if (pressure.dif_barometers.arr[3] == -1) break;
        } while (false);
        /** @} */

        /**
         * Reads all absolute barometers:
         * @{
         */
        read_abs_baroms(PS1, pressure.abs_barometers.arr[0]);
        read_abs_baroms(PS2, pressure.abs_barometers.arr[1]);
        /** @} */

        sdpu_manager_RI_Send_Env_Lab_Measurements(&uel, &del, &pressure);
    }

    inline void distribute_only_to_ATL() {
        static asn1SccAtt_Lab_Data_Measurements atl_data {};

        /**
         * Reads all analogue lines from all channels
         * where valid signals FROM the ATL are present
         *
         * If one of them fails, we will not keep reading other channels.
         * @{
         */
        atl_data.photodiodes.arr[0] = readRawFrom(PHOTODIODE_1, ctxt.n_retries_adc);
        if (atl_data.photodiodes.arr[0] == -1) {return;}

        atl_data.photodiodes.arr[1] = readRawFrom(PHOTODIODE_2, ctxt.n_retries_adc);
        if (atl_data.photodiodes.arr[1] == -1) {return;}

        atl_data.photodiodes.arr[2] = readRawFrom(PHOTODIODE_3, ctxt.n_retries_adc);
        if (atl_data.photodiodes.arr[2] == -1) {return;}

        atl_data.photodiodes.arr[3] = readRawFrom(PHOTODIODE_4, ctxt.n_retries_adc);
        if (atl_data.photodiodes.arr[3] == -1) {return;}

        atl_data.thermistors.arr[0] = readRawFrom(PT1000_1, ctxt.n_retries_adc);
        if (atl_data.thermistors.arr[0] == -1) {return;}

        atl_data.thermistors.arr[1] = readRawFrom(PT1000_2, ctxt.n_retries_adc);
        if (atl_data.thermistors.arr[1] == -1) {return;}
        /** @} */

        sdpu_manager_RI_Send_ATL_Measurements(&atl_data);
    }
}

/*******************************************************************************
 * Public Operations:
 ******************************************************************************/

/**
 * @note SDPU initialization depends on the ATL and EL needs, thus,
 * it is initialized and configured in the Read_SDPU_Measurements function.
 */
void sdpu_manager_startup(void) {
    debug_printf(LVL_INFO, NAME"Initialized");
}

/**
 * @brief Reads measurements from SDPU based on NADS & ATL needs.
 */
void sdpu_manager_PI_Read_SDPU_Measurements(void) {
    asn1SccBalloon_Mode balloon_mode;
    sdpu_manager_RI_Get_Mode(&balloon_mode);

    bool EL_needs_measurements {false};
    sdpu_manager_RI_EL_Needs_Measurements(&balloon_mode, &EL_needs_measurements);
    bool ATL_needs_measurements {false};
    sdpu_manager_RI_ATL_Needs_Measurements(&balloon_mode, &ATL_needs_measurements);

    if (EL_needs_measurements && ATL_needs_measurements) {
        if (sdpu::initialize()) {
            debug_printf(LVL_INFO, NAME"SDPU initialized for EL and ATL\n");
            distribute_to_ATL_and_EL();
        } else {
            debug_printf(LVL_ERROR, NAME"FAILED to initialize SDPU board\n");
        }
    } else if (EL_needs_measurements) {
        if (sdpu::initialize()) {
            debug_printf(LVL_INFO, NAME"SDPU initialized for EL\n");
            distribute_only_to_EL();
        } else {
            debug_printf(LVL_ERROR, NAME"FAILED to initialize SDPU board\n");
        }
    } else if (ATL_needs_measurements) {
        if (sdpu::initialize()) {
            debug_printf(LVL_INFO, NAME"SDPU initialized for ATL\n");
            distribute_only_to_ATL();
        } else {
            debug_printf(LVL_ERROR, NAME"FAILED to initialize SDPU board\n");
        }
    } else {
        debug_printf(LVL_INFO, NAME"SDPU Mode-OFF\n");
        sdpu::finalize();
    }
}

void sdpu_manager_PI_Reset_SDPU_I2C_Device
    (const asn1SccRestartable_Device_ID *device_id)
{
    switch (*device_id) {
    case asn1SccRestartable_Device_ID_sdpu_adc:
        board_support::sdpu::resetADC();
        break;
    case asn1SccRestartable_Device_ID_sdpu_absolute_barometer_1:
        board_support::sdpu::resetPressureSensor(PS1);
        break;
    case asn1SccRestartable_Device_ID_sdpu_absolute_barometer_2:
        board_support::sdpu::resetPressureSensor(PS2);
        break;
    default:
        break;
    }
}
