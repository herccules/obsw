// Fill in this class with your context data (internal state):
// list all the variables you want global (per function instance)
#include "dataview-uniq.h"

class tmtc_internal_mode_state {
public:
    asn1SccTMTC_Mode mode {asn1SccTMTC_Mode_mute};
};
