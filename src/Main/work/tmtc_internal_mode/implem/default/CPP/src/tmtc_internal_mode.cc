// C+++ body file for function TMTC_Internal_Mode

#include "tmtc_internal_mode.h"
#include "tmtc_internal_mode_state.h"

// Define and use function state inside this context structure
// avoid defining global/static variable elsewhere
namespace {
    tmtc_internal_mode_state ctxt_tmtc_internal_mode;
}

void tmtc_internal_mode_startup(void) {
    ctxt_tmtc_internal_mode.mode = asn1SccTMTC_Mode_mute;
}

void tmtc_internal_mode_PI_Get_Internal_Mode
    (asn1SccTMTC_Mode *OUT_tmtc_mode)
{
    *OUT_tmtc_mode = ctxt_tmtc_internal_mode.mode;
}

void tmtc_internal_mode_PI_Set_Internal_Mode
    (const asn1SccTMTC_Mode *IN_tmtc_mode)
{
    ctxt_tmtc_internal_mode.mode = *IN_tmtc_mode;
}
