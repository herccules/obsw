/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                             NADS_Manager Source                            --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "nads_manager.h"
#include "nads_manager_state.h"
#include "Context-nads-manager.h" // Context Parameters defined in the Interface View

#include "CsvWriter.h"
#include "String.h"
#include "Images.h"

#include "Time_Management.h"
#include "GpsHandler.h"

#include "debug_messages.h"

#include <cmath> // isfinite

#include <iostream>

// --  Local variables  --------------------------------------------------------

#define GPS_SERIAL_PORT (nads_manager_ctxt.gps_serial_port)

#define NAME "[NADS Manager] "

namespace {
    nads_manager_state ctxt;
}

// --  Internal operations  ----------------------------------------------------

/*******************************************************************************
 * IMU related operations
 ******************************************************************************/

namespace {
    // --------------------------
    // -- Process balloon mode --
    // --------------------------

    void nads_process_balloon_mode()
    {
        asn1SccBalloon_Mode mode;
        nads_manager_RI_Get_Mode(&mode);

        switch (mode) {
            case asn1SccBalloon_Mode_ground_await:
                if (ctxt.nads_data.mode == asn1SccNADS_Mode_on) {
                    debug_printf(LVL_INFO, NAME"GPS To Mode-OFF ------------------->\n");
                    equipment_handlers::gps_handler::setOperatingMode(equipment_handlers::gps_handler::INACTIVE);
                }
                ctxt.nads_data.mode = asn1SccNADS_Mode_off;
                break;
            case asn1SccBalloon_Mode_ground_pre_launch :
            case asn1SccBalloon_Mode_flight_ascent     :
            case asn1SccBalloon_Mode_flight_float      :
                if (ctxt.nads_data.mode == asn1SccNADS_Mode_off) {
                    debug_printf(LVL_INFO, NAME"GPS To Mode-ON  ------------------>\n");
                    equipment_handlers::gps_handler::setOperatingMode(equipment_handlers::gps_handler::ACTIVE);
                }
                ctxt.nads_data.mode = asn1SccNADS_Mode_on;
                break;
            case asn1SccBalloon_Mode_off:
                debug_printf(LVL_INFO, NAME"To Mode-OFF (End Of Mission) --------->\n");
                ctxt.nads_data.mode = asn1SccNADS_Mode_off;
                ctxt.end_of_mission = true;
                equipment_handlers::gps_handler::setOperatingMode(equipment_handlers::gps_handler::INACTIVE);
                break;
        }
    }

    // -------------------------------------------
    // -- Handle data & its auxiliary functions --
    // -------------------------------------------

    // -- Store data:

    /**
     * Stores the 100 measurements each second.
     */
    void nads_manager_store_data() {
        const auto & imu = ctxt.nads_data.payload.imu;

        using data_storage::basic_images::image;

        ctxt.logfile.add_line({
            image(static_cast<int32_t>(ctxt.nads_data.snapshot_time.secs)),
            image(static_cast<int32_t>(ctxt.nads_data.snapshot_time.usecs)),
            image(static_cast<float>  (ctxt.nads_data.mission_time)),
            nads_manager_state::
            image(static_cast<asn1SccNADS_Mode>(ctxt.nads_data.mode)),

            image(static_cast<float>(imu.sensors_data.acceleration.x)),
            image(static_cast<float>(imu.sensors_data.acceleration.y)),
            image(static_cast<float>(imu.sensors_data.acceleration.z)),
            image(static_cast<float>(imu.sensors_data.angular_velocity.x)),
            image(static_cast<float>(imu.sensors_data.angular_velocity.y)),
            image(static_cast<float>(imu.sensors_data.angular_velocity.z)),
            image(static_cast<float>(imu.sensors_data.mag_field.x)),
            image(static_cast<float>(imu.sensors_data.mag_field.y)),
            image(static_cast<float>(imu.sensors_data.mag_field.z)),

            image(static_cast<float>(imu.fusion_data.euler_orientation.x)),
            image(static_cast<float>(imu.fusion_data.euler_orientation.y)),
            image(static_cast<float>(imu.fusion_data.euler_orientation.z)),
            image(static_cast<float>(imu.fusion_data.liner_acceleration.x)),
            image(static_cast<float>(imu.fusion_data.liner_acceleration.y)),
            image(static_cast<float>(imu.fusion_data.liner_acceleration.z)),
            image(static_cast<float>(imu.fusion_data.gravity.x)),
            image(static_cast<float>(imu.fusion_data.gravity.y)),
            image(static_cast<float>(imu.fusion_data.gravity.z)),
            image(static_cast<float>(imu.fusion_data.quaternion_orientation.w)),
            image(static_cast<float>(imu.fusion_data.quaternion_orientation.x)),
            image(static_cast<float>(imu.fusion_data.quaternion_orientation.y)),
            image(static_cast<float>(imu.fusion_data.quaternion_orientation.z)),

            image(static_cast<std::int8_t>(imu.temperatures.temperature_accel)),
            image(static_cast<std::int8_t>(imu.temperatures.temperature_gyro)),
        });

        if (ctxt.cycle == 50U) {
            ctxt.logfile.save_file();
        }
    }

    // -- Publish data

    /**
     * @brief Updates data pool at 1 Hz frequency
     */
    void nads_publish_data() {
        if (ctxt.cycle == 100U) {
            asn1SccNADS_Data old_nads_data;
            nads_manager_RI_Get_NADS_Data(&old_nads_data);
            ctxt.nads_data.payload.gps = old_nads_data.payload.gps;
            nads_manager_RI_Update_NADS_Data(&ctxt.nads_data);
        }
    }

    // -- Handle data

    /**
     * @brief Updates the cycle, each increment represents a 10ms lapse
     */
    constexpr void update_cycle(uint8_t &cycle) {
        cycle = cycle < 100U
              ? cycle + 1U
              : 1U;
    }

    /**
     * Acquires and stores data regarding the NADS mode.
     */
    void nads_handle_data() {
        // Get time:
        auto && [secs, usecs] = time_management::absolute_time();
        ctxt.nads_data.snapshot_time = {secs, usecs};
        ctxt.nads_data.mission_time  = time_management::mission_time();

        // Initialize & read & store IMU measurements:
        if (ctxt.nads_data.mode == asn1SccNADS_Mode_on) {            
            asn1SccBoolean_Type imu_initialized {false};
            nads_manager_RI_Initialize_IMU(&imu_initialized);

            if (imu_initialized) {
                auto cycle {static_cast<asn1SccUINT8_Type>(ctxt.cycle)};
                nads_manager_RI_Read_IMU_Data(&cycle, &ctxt.nads_data.payload.imu);
                nads_manager_store_data();
            } else {
                debug_printf(LVL_ERROR, NAME"FAILED to initialize IMU\n");
            }

        } else { // Deactivate IMU:
            nads_manager_RI_Finalize_IMU();
        }

        nads_publish_data();

        update_cycle(ctxt.cycle);
    }
}

/*******************************************************************************
 * GPS related operations
 ******************************************************************************/
namespace {
    using GPS_Logfile_type = data_storage::CsvWriter<7U>;
    GPS_Logfile_type::Line GPS_header {
        "time [secs]" , "time [nsecs]",
        "latitude [deg]", "longitude [deg]", "atltitude[m]",
        "speed over gnd [knots]", "course over ground [deg]"
    };
    GPS_Logfile_type GPS_logfile {"gps-log.csv", ';', GPS_header};

    void gps_record_data(equipment_handlers::gps_handler::gps_data_t &IN_gps_data) {
        using data_storage::basic_images::image;

        GPS_logfile.add_line({
            image(static_cast<int32_t>(IN_gps_data.unix_time.tv_sec)),
            image(static_cast<int32_t>(IN_gps_data.unix_time.tv_nsec)),
            image(IN_gps_data.latitude_deg),
            image(IN_gps_data.longitude_deg),
            image(IN_gps_data.altitude_m),
            image(IN_gps_data.sog_knots),
            image(IN_gps_data.cog_deg)
        });

        static uint32_t count = 1U;
        if (count == 10U) {
            GPS_logfile.save_file();
            count = 0U;
        }
        count++;
    }

    void gps_safeguard_data() {
        GPS_logfile.save_file();
    }

    template <typename T>
    T zero_if_nan(T x) {
        return std::isfinite(x) ? x : T(0.0);
    }

    void gps_to_asn1SccGPS_Data(equipment_handlers::gps_handler::gps_data_t &IN_gps_data,
                                asn1SccGPS_Data &OUT_asn1_data) {
        OUT_asn1_data.time_secs     = zero_if_nan(IN_gps_data.unix_time.tv_sec);
        OUT_asn1_data.time_nsecs    = zero_if_nan(IN_gps_data.unix_time.tv_nsec);
        OUT_asn1_data.latitude_deg  = zero_if_nan(IN_gps_data.latitude_deg);
        OUT_asn1_data.longitude_deg = zero_if_nan(IN_gps_data.longitude_deg);
        OUT_asn1_data.altitude_m    = zero_if_nan(IN_gps_data.altitude_m);
        OUT_asn1_data.sog_knots     = zero_if_nan(IN_gps_data.sog_knots);
        OUT_asn1_data.cog_deg       = zero_if_nan(IN_gps_data.cog_deg);
    }

    void gps_update_datapool(asn1SccGPS_Data &IN_gps_data) {
        asn1SccNADS_Data nads_data {};
        nads_manager_RI_Get_NADS_Data(&nads_data);

        auto && [secs, usecs] = time_management::absolute_time();
        ctxt.nads_data.snapshot_time = {secs, usecs};
        nads_data.mission_time = time_management::mission_time();
        nads_data.payload.gps = IN_gps_data;

        nads_manager_RI_Update_NADS_Data(&nads_data);
    }

    void gps_handler_task (equipment_handlers::gps_handler::gps_data_t gps_data) {        
        std::cout << "\n"
             "------------GPS------------" <<
             "\n- secs: " << gps_data.unix_time.tv_sec << " nsecs: " << gps_data.unix_time.tv_nsec <<
             "\n- lat: "  << gps_data.latitude_deg <<
             "\n- lon: "  << gps_data.longitude_deg <<
             "\n- alt: "  << gps_data.altitude_m <<
             "\n- sog: "  << gps_data.sog_knots <<
             "\n- cog: "  << gps_data.cog_deg <<
             "---------------------------\n\n";

        /// @addtogroup Sets up the system time. This is done only once.
        /// @{
        static bool time_is_configured {false};
        bool        time_is_valid = gps_data.unix_time.tv_sec > 0;
        if (!time_is_configured && time_is_valid) {
            std::cout << ">>>> Configuring the T I M E <<<<\n";
            time_is_configured = time_management::configure_absolute_time(
                gps_data.unix_time.tv_sec,
                gps_data.unix_time.tv_nsec * 1000);
        }
        /// @}

        asn1SccBalloon_Mode mode;
        nads_manager_RI_Get_Mode(&mode);

        switch (mode) {
            case asn1SccBalloon_Mode_ground_await:
                break;
            case asn1SccBalloon_Mode_ground_pre_launch :
            case asn1SccBalloon_Mode_flight_ascent     :
            case asn1SccBalloon_Mode_flight_float      : {
                asn1SccGPS_Data gps_dp {};
                gps_to_asn1SccGPS_Data(gps_data, gps_dp);
                gps_update_datapool(gps_dp);
                gps_record_data(gps_data);
            }   break;
            case asn1SccBalloon_Mode_off: {
                gps_safeguard_data();
            }   break;
        }
    }
}

// --  Provided Interface  -----------------------------------------------------

// ------------------------
// -- Startup & Shutdown --
// ------------------------

/**
 * @note This startup routine is invoked by the TASTE runtime,
 * which guarantees that it's executed only once, even if it was invoked multiple times.
 * The IMU is initialized when required (during ON mode).
 */
void nads_manager_startup(void)
{
    ctxt.nads_data.mode = asn1SccNADS_Mode_off;
    equipment_handlers::gps_handler::initialize(GPS_SERIAL_PORT);
    equipment_handlers::gps_handler::startDataAcquisition(gps_handler_task);
    debug_printf(LVL_INFO, "[NADS-Manager] Initialized\n");
}

/**
 * This is the shutdown routine that shall be invoked when "end of mission" is reached.
 */
static void nads_shutdown(void) {
    debug_printf(LVL_INFO, "[NADS-Manager] Shutdown\n");
    ctxt.logfile.save_file();

    nads_manager_RI_Finalize_IMU();

    // Update Data Pool with latest info:
    nads_manager_RI_Update_NADS_Data(&ctxt.nads_data);

    // Notify the Manager that this subsystem has finished:
    constexpr asn1SccBalloon_Events finish_event {asn1SccBalloon_Events_nads_finished};
    nads_manager_RI_Notify_Event(&finish_event);
}

// -------------------------------
// -- Measure And Data Handling --
// -------------------------------

void nads_manager_PI_Measure_And_Handle(void) {
    if ( ! ctxt.end_of_mission ) {
        // 1.  Nominal activity:
        nads_process_balloon_mode();
        nads_handle_data();

        // 2. Shutdown if end of mission:
        if (ctxt.end_of_mission) {
            nads_shutdown();
        }
    }
}

// ----------------------
// -- Reset I2C Device --
// ----------------------

void nads_manager_PI_Reset_I2C_Device
    (const asn1SccRestartable_Device_ID * IN_device_id)
{
    switch (*IN_device_id) {
    case asn1SccRestartable_Device_ID_nads_imu:
        nads_manager_RI_Reset_IMU();
        break;
    case asn1SccRestartable_Device_ID_nads_gps:
        // debug_printf(LVL_WARN, "[NADS-Manager] TODO: Restart GPS is not implemented\n");
        break;
    default:
        debug_printf(LVL_WARN, "[NADS-Manager] Reset_I2C_Device: Unknown device_id\n");
        break;
    }
}
