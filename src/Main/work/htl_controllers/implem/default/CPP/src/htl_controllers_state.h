// Fill in this class with your context data (internal state):
// list all the variables you want global (per function instance)
#include "dataview-uniq.h"

class htl_controllers_state {
public:
    asn1SccActuator_Control_Mode heaters_mode [4U] = {
        asn1SccActuator_Control_Mode_autonomous_control,
        asn1SccActuator_Control_Mode_autonomous_control,
        asn1SccActuator_Control_Mode_autonomous_control,
        asn1SccActuator_Control_Mode_autonomous_control
    };
};
