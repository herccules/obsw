/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                           HTL_Controllers Source                           --
--                                                                            --
--            Copyright (C) 2023 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

// C++ body file for function HTL_Controllers

#include "htl_controllers.h"
#include "htl_controllers_state.h"

#include "debug_messages.h"

#include <math.h> // sqrt

#include "PCU.h"  // Hardware I/F for PCU

/*******************************************************************************
 * Private variables and constants:
 ******************************************************************************/

namespace {
    /**
     * Define and use function state inside this context structure
     * avoid defining global/static variable elsewhere
     */
    htl_controllers_state ctxt;
}

/*******************************************************************************
 * Private functions:
 ******************************************************************************/

namespace {

    constexpr float v_in_100 [4U] = {
        12.0F, 12.0F, 12.0F, 12.0F
    };

    constexpr float heater_ohm [4U] = { // R[Ohm] = V^2 / P
        126.95F, 127.17F, 128.53F, 25.21F
    };

    inline asn1SccHeater_Power_Type power(const asn1SccHTL_Heater_ID IN_heater_id,
                                          const float pwm_duty_cycle) {        
        float voltage {v_in_100[IN_heater_id] * pwm_duty_cycle / 100.0F};
        float watts {voltage * voltage / heater_ohm[IN_heater_id]};
        return watts > 5.0 ? 5.0 : watts;
    }

    /**
     *       _  .------.
     *   V =  \/ P * R '
     */
    inline float pwm_duty_cycle(const asn1SccHTL_Heater_ID     IN_heater_id,
                                const asn1SccHeater_Power_Type IN_power) {
        float v_in_required {sqrt(static_cast<float>(IN_power) * heater_ohm[IN_heater_id])};
        float duty_cycle {(v_in_required * 100.0F) / v_in_100[IN_heater_id]};
        return duty_cycle > 100.0 ? 100.0 : duty_cycle;
    }

    inline void control_heater(const asn1SccHTL_Heater_ID     IN_heater_id,
                               const asn1SccHeater_Power_Type IN_power)
    {
        auto heater {static_cast<board_support::pcu::HTLPWMHeater> (IN_heater_id)};
        auto pwm {pwm_duty_cycle(IN_heater_id, IN_power)};

        auto success = board_support::pcu::setHTLPWMDutyCycle(heater, pwm);
        if (success) {
            debug_printf(LVL_INFO, "[HTL_Heaters] CONTROL Heater OK\n");
        } else {
            debug_printf(LVL_ERROR, "[HTL_Heaters] FAILED to set PWM\n");
        }
    }
}

/*******************************************************************************
 * Public functions:
 ******************************************************************************/

void htl_controllers_startup(void) {
   if (board_support::pcu::initialize_switches()) {
       debug_printf(LVL_INFO, "[HTL-Controllers] Initialized\n");
   }
}

void htl_controllers_PI_Control_Heater_Auto
    (const asn1SccHTL_Heater_ID     *IN_heater_id,
     const asn1SccHTL_Mode          *IN_htl_mode)
{    
    if (ctxt.heaters_mode[*IN_heater_id] == asn1SccActuator_Control_Mode_autonomous_control) {
        constexpr float Q_01 [4] {1.0, 0.5, 1.5, 3.5}; // @todo: TBC
        float power {0.0F};
        switch (IN_htl_mode->mode) {
            case asn1SccHTL_Mode_mode_ascent_mode1:
                power = Q_01[*IN_heater_id];
                break;
            case asn1SccHTL_Mode_mode_ascent_mode2:
                power = Q_01[*IN_heater_id] / 2.0;  // @bug LATE!
                break;
            case asn1SccHTL_Mode_mode_float_mode1:
                power = Q_01[*IN_heater_id] * 0.5;
                break;
            case asn1SccHTL_Mode_mode_float_mode2:
                power = Q_01[*IN_heater_id] * 0.75; // @bug never!
                break;
            case asn1SccHTL_Mode_mode_off:
            case asn1SccHTL_Mode_mode_pre_launch:
            default:
                power = 0.0F;
                break;
        }

        control_heater(*IN_heater_id, power);
    }
}


void htl_controllers_PI_Control_Heater_Manual
    (const asn1SccHTL_Heater_ID     *IN_heater_id,
     const asn1SccHeater_Power_Type *IN_heater_power)
{
    if (ctxt.heaters_mode[*IN_heater_id] == asn1SccActuator_Control_Mode_manual_control) {
        control_heater(*IN_heater_id, *IN_heater_power);
    } else {
        debug_printf(LVL_WARN, "[HTL_Heaters] Can't control heater in AUTONOMOUS mode\n");
    }
}


void htl_controllers_PI_Get_Heaters_Status
    (asn1SccHeater_Power_Type *h1_power,
     asn1SccHeater_Power_Type *h2_power,
     asn1SccHeater_Power_Type *h3_power,
     asn1SccHeater_Power_Type *h4_power,
     asn1SccHTL_Heaters_Mode  *heaters_mode)
{
    float h1_, h2_, h3_, h4_;
    board_support::pcu::getHTLPWMDutyCycle(h1_, h2_, h3_, h4_);
    *h1_power = power(HTL_Heater_ID_experiment1_heater, h1_);
    *h2_power = power(HTL_Heater_ID_experiment2_heater, h2_);
    *h3_power = power(HTL_Heater_ID_experiment3_heater, h3_);
    *h4_power = power(HTL_Heater_ID_experiment4_heater, h4_);

    heaters_mode->experiment_1 = ctxt.heaters_mode[HTL_Heater_ID_experiment1_heater];
    heaters_mode->experiment_2 = ctxt.heaters_mode[HTL_Heater_ID_experiment2_heater];
    heaters_mode->experiment_3 = ctxt.heaters_mode[HTL_Heater_ID_experiment3_heater];
    heaters_mode->experiment_4 = ctxt.heaters_mode[HTL_Heater_ID_experiment4_heater];
}


void htl_controllers_PI_Set_HTL_Heaters_Mode
   (const asn1SccHTL_Heaters_Mode *IN_htl_heaters_mode)
{
   ctxt.heaters_mode[HTL_Heater_ID_experiment1_heater] = IN_htl_heaters_mode->experiment_1;
   ctxt.heaters_mode[HTL_Heater_ID_experiment2_heater] = IN_htl_heaters_mode->experiment_2;
   ctxt.heaters_mode[HTL_Heater_ID_experiment3_heater] = IN_htl_heaters_mode->experiment_3;
   ctxt.heaters_mode[HTL_Heater_ID_experiment4_heater] = IN_htl_heaters_mode->experiment_4;
}


void htl_controllers_PI_Set_Single_HTL_Heater_Mode
    (const asn1SccHTL_Heater_ID         *IN_heater_id,
     const asn1SccActuator_Control_Mode *IN_heater_mode)
{
   ctxt.heaters_mode[*IN_heater_id] = *IN_heater_mode;
}
