CWD          =$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
SUBDIRS      =src/
BUILD_DIRS   =$(SUBDIRS:%=build-%)
CLEAN_DIRS   =$(SUBDIRS:%=clean-%)
INSTALL_DIRS =$(SUBDIRS:%=install-%)

all: $(SUBDIRS)
$(SUBDIRS): $(BUILD_DIRS)
$(BUILD_DIRS):
	$(MAKE) -C $(@:build-%=%) all

clean: $(CLEAN_DIRS)
$(CLEAN_DIRS):
	$(MAKE) -C $(@:clean-%=%) clean

install:$(INSTALL_DIRS)
$(INSTALL_DIRS):
	$(MAKE) -C $(@:install-%=%) install


.PHONY: $(SUBDIRS) all $(BUILD_DIRS) clean $(CLEAN_DIRS) install $(INSTALL_DIRS)

# To declare dependencies write the following rule
# dependent_module: independent_module
