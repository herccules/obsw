#!/bin/bash

#                          H  E  R  C  C  U  L  E  S
#
# Purpose:
#    This script uploads the static libraries, binaries, and headers into the
#    RPi via SSH. If these were already installed and the file did not change,
#    they are not uploaded (thanks to rsync).

#                 Copyright (C) 2022 STRAST-UPM research group
#   This work was developed in the context of the AURORA and HERCCULES project


MY_NAME=$(basename "${0}")
MY_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

NC='\033[0m' # No Color
RED='\033[0;31m'
BLUE='\033[0;34m'
GREEN='\033[0;32m'

##  Check arguments format and process input ###################################

if [ "$#" -eq 0 ]; then
    echo -e "${BLUE}[${MY_NAME}] Introduce the target's user [pi]${NC}"
    read -ep "" USER
    USER=${USER:-pi}

    echo -e "${BLUE}[${MY_NAME}] Introduce the target's IP address [172.20.10.6]${NC}"
    read -ep "" IP
    IP=${IP:-172.20.10.3}
elif [ "$#" -eq 1 ]; then
    IP=$1
else
    echo -e "${RED}[${MY_NAME}] Error Illegal number of parameters"
    echo -e "[${MY_NAME}] Usage: ${MY_NAME} [RPi IP address]${NC}"
    exit 22
fi

##  Build the project  #########################################################

pushd ${MY_DIR}/../
    make install
popd

##  Copy from host to RPi via rsync  ###########################################

function build_dir() {
    echo "${MY_DIR}/../src/build/rpi/./${1}/"
}

dirs=(
"$(build_dir "debug")/include/"
"$(build_dir "debug")/bin/"
"$(build_dir "debug")/lib/"
"$(build_dir "release")/include/"
"$(build_dir "release")/bin/"
"$(build_dir "release")/lib/"
"./Main/work/binaries"
)

if rsync -Rav "${dirs[@]}" ${USER}@"${IP}":~/herccules/; then
    echo -e "${GREEN}[${MY_NAME}] Uploaded!${NC}"
else
    echo -e "${RED}[${MY_NAME}] Error, could not upload!${NC}"
fi
