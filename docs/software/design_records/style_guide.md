# Coding guidelines

Here are some rules that I have applied consciously and unconsciously
throughout the software development of the HERCCULES project.

We have learned most of them from different open-source guidelines
and source code from other projects and libraries.

The rules' contents describe generic conventions to achieve systems and code with *good taste*.
That is why, such rules are more focused on high-level issues
rather than low-level ones like indenting, line lengths, etc.
Although, they play an important role for readability.

The rules can be applied in any project,
independent of the programming language.
However, the code excerpts and some rules are valid only for *C* and *C++*.

## Functions

Functions are the most basic container for the source code of the system,
therefore, we must design functions well.
The main and common ideas behind these rules are to
make functions readable and easy-to-understand by anyone.

1.  A function should do **only 1** thing, and it should do it *well*.
    What does *1 thing* mean?
    It means that you have to extract functions from functions
    until you get enough.

1.  Functions should be small, how big should a function be?
    -   Uncle Bob recommends **6 lines** at most, 10 is too much!
    -   LKCS recommends **24 lines** at most, suitable for embedded systems
        (inline functions if it is performance-critical).
    -   In RTES the function stack overhead is a concern (ms instead of ns).
        > **Note**: We should worry only in the innermost loops,
        > since they are called multiple times.
        > For instance, inline helper-methods in the outermost
        > loop where function's overhead is minuscule.
    -   Exceptions to the rule:
        +   Scientific/Mathematical functions are allowed to be larger
            since they implement complex algorithms.
        +   If you have a large but simple function (e.g.: case-statement)
            where you do a lot of small things, it is also allowed to be long.

1.  Naming for functions:
    -   Functions in a small scope should have nice long descriptive names,
        so your code will read like prose.
    -   Functions in a global scope should have short names
        since they are well known.
    -   Procedures: (void return functions),
        manipulate data and are action verbs or imperative commands.
        E.g.: `void get_next_token(Token *t)` or `void create(Token *t)`
        use transitive verbs.
    -   Functions: (non-void return functions):
        +   Boolean functions: Use predicates. E.g.: `bool is_empty()` or `bool has_members()`.
        +   Non-boolean functions: Use nouns. E.g.: `size_t length()` instead of `size_t get_length()`.
    - Exceptions to the rule:
        +   Procedures are allowed to return "status values" or "return codes"
            to mark success/failure.
            Use action verbs or imperative commands to name them.
            E.g.: `boolean get_next_token(Token *t)` or `boolean initialize()`, both return "succeeded boolean".
    - Common preffixes for boolean function naming: *is/was/will*, *has* instead of *are_present*, *does*, *should*.

1.  Non-Boolean functions whose return value are the result of the computation,
    are allowed to indicate failure returning out-of-range results.
    E.g.: return `null`, `-1`, or `ERR_PTR` to report a failure.

1.  Procedures that return "status values" should return 0 in success,
    and any other value to indicate a specific failure.
    Although, in `C/C++` 0 means `False` and any other value (1, 2, ...) `True`.

1.  *Command Query Separation (CQS)*: Do not combine *command* and *query* in a single function.
    -  *Command* subprograms are procedures that change/update the state of the system, i.e. they have side effects!
    -  *Query* subprograms are functions that return the value of a computation or the state of the system.

1.  Extract a module (class, package, namespace, static class, ...) from a function when
    there is a long function that has long functional areas,
    which use common variables.
    Long functions are where the classes go to hide.

1.  It is much better for maintainability and readability to depend on arguments,
    rather than on global or member variables.

1.  Multiple return statements. This is recommended in MISRA C R 15.5,
    but sometimes this rule does not make sense.
    -   We can respect the *single entry/exit rule* even with multiple return statements.
        Early returns and guard returns takes us to the end of the function,
        where the unique exit lives.
        In addition, it improves readability!

    -   However, it is better to avoid returns and breaks inside **complicated** loops
        since it makes the code harder to read.

1. It is better to separate handling error code from the function' logic implementation.
   Exceptions are an asset in this case.

1.  The name of an exception thrown by a function should be scoped to the function's module and
    indicate the kind of the problem the exception represents.
     > **Note**: Checked exceptions are a headache in OOP since they create a reverse dependency,
     >           a dependency of children to parent. Use unchecked exceptions!

1.  A function that handles an exception should be in charge only of the exception handling.
    Since a function should only one thing,
    the basic structure for a function that handle errors is:
    ```C++
    void function_name (args ...) {
        try {
            // there must be only one statement
            actual_logic();
        } catch blocks {
            // error handling
        }   // Nothing follows the end of the catch blocks
    }
    ```

## Data Types

1.  Use **singular and general nouns** as type identifiers and data structures' components.
    When possible, use identifiers that describe one of the type's values. E.g.:

    ```C
    enum Day { // in preference of Days or DayType
        Monday, Tuesday, Wednesday, Thursday, Friday,
        Saturday, Sunday
    };

    Day today;
    Day yesterday;
    ```

1.  Consider using suffixes for type identifiers that help to define related aspects of an abstraction.

1.  Avoid anonymous array types.

1.  Ideally, data structures offer public variables and no methods.
    If they do, these methods are getters and setters that manipulate individual variables.

    ```C++
    struct Employee {
        UUID    id;
        String  name;
        Date    birthday;
    }
    ```

1.  Data structures are not cohesive, since they offer getters and setters only.
    They expose implementation, and don't hide members like classes.

1.  `switch` statements are allowed because they do not use polymorphism like objects.

1.  Data structures protect us against new methods,
    but exposes us from new types.
    If you add a new function you only need a new `switch` statement. E.g.:

    ```
    Functions    SWITCH    Types

         Draw   ---+--->   Circle
                   |
        Erase   ---+--->   Triangle
                   |
       Rotate   ---+--->   Rectangle
                   |
         Drag      +--->   Square
    
    ```
    If you add a new function like Drag,
    would not affect all the other functions,
    nor would it have an effect of the existing data structures like Circle or Triangle. We do not break independent deployability.

1.  If methods are likely to be added, use data structures and switch statements.

1.  Impedance mismatch. Database tables are similar to data structures, but different from objects.

## Objects, classes, and OOP

1.  Use the type naming convention for classes: **singular and general nouns**.

1.  Use **singular and specific nouns** as object identifiers.
    The identifier should describe the object's value during execution.

1.  Give active object's names that imply an **activity** entity.
    E.g.: Manager, handler, orchestrator, supervisor, reader, etc.

1.  Ideally, from the outside looking, a class is just a bag of functions
    that has no variables. Thus, and object has no observable state.
    Classes are the opposite from data structures.

    ```C++
    class IdealClass {
        public:
            void doThing();
            void doAnotherThing();
        private:
            int m_a;
            int m_b;
            // --snip--
    }
    ```

1.  *Tell don't ask*. If objects have no observable state, they should not have getters and setters.
    They are a symptom of bad code. If you want to modify private state,
    just make them public!

1.  Maximum cohesive methods manipulate all its attributes,
    and maximum cohesive classes are composed of maximum cohesive methods.
    E.g.: getters and setters are not cohesive methods.

1.  Use a higher abstraction instead of getters or setters. E.g:
     ```C++
     class Car {
        public:
            float gallonsOfGas() {      // BAD: clients infer a private variable
                return m_gallonsOfGas;  // which is too much for future changes
            }

        private:
            float m_gallonsOfGas;
     }

     class Car {
        public:
            float getPercentageFuel();  // GOOD: Use uncertainty as a tool:
                                        // - it allows polymorphism
                                        // - independent deployability against new types
                                        // - protects client code from the server code
     ```

1.  Switch statements are not allowed in OOP.
    Polymorphism protects client from changes in the server side and objects protect clients from new types.
    However, switch statements expose our changes in types. E.g.:

    ```
    +---+    +--------+
    |   |--->| Type A |
    | S |    +--------+
    | W |
    | I |    +--------+
    | T |--->| Type B |
    | C |    +--------+
    | H |
    |   |  If we want a new type,
    +---+  we would have to update the function containing the switch statement.
    ```

1.  Classes protect us from new types, but expose us from new methods.
    If you modify the base adding a new function,
    you break *independent deployability* because derivatives and clients are affected. They have to be recompiled and redeployed.

1.  If types are likely to be added, use objects and polymorphism.

1.  Remember that classes are the antithesis of data structures.


## Modules and components

1.  Modules and components should use names that imply a higher level of hierarchy than subprograms.
    Generally, they are: **noun phrases**.
    
1.  Software systems are composed of subsystems with crisscrossed boundaries. E.g., The boundary that separates: the main from the application, the views from the model, the experiments from the devices, the database from domain objects, etc.

1.  Ideally, source code dependencies should point away from the concrete side towards the abstract side.

    ```
    +------------------------+
    |    Domain  objects     |  Factory base classes
    +------------------------+
                ^
                |
    +-----------+------------+
    |          Main          |  Full of CONCRETE factories that use
    +-----------+------------+  switch statements
    ```

    ```
    +------------------------+
    |        App &           |
    |    Domain  objects     |  Objects with business rules
    +------------------------+
                ^
                |
    +-----------+------------+  SQL code and data structures with
    |   DB interface layer   |  switch statements that implement
    +-----------+------------+  domain objects abstractions
                |
                v
    +------------------------+
    |           DB           |  DB Schema (tables)
    +------------------------+

    Remember that data structures are similar to DB table rows.
    ```

    ```
    +------------------------+
    |         View           |  Concrete implementations that depend on
    +------------------------+  interfaces required by the domain objects
                |
                v
    +------------------------+
    |      Domain objects    |  They know nothing about the views
    +------------------------+
    ```
    
1.   To ease the changes in the environment (e.g. hardware equipment),
     so that these do not affect the core part of the system,
     we should weaken the relationship between them.

     E.g.: In an *access control* system we use hedge components (`GUI` and `Card reader`)
     in the boundaries to communicate the system with its environment elements
     avoiding `damages` and `scratches`.
     
     ```
                                       +-----------------+
             +------------------------>|       GUI       |<-------------.
             |                         +-----------------+     +--------+-------+
      O      |                                                 | Access Control |
     .+.     |   .-------------.       +-----------------+     +--------+-------+
    / | \ ---+---| Card reader |------>| Card reader I/F |<-------------'
      ^          `-------------´       +-----------------+
    _/ \_
     
     ```
