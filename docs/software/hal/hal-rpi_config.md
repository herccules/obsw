# Documentation for the HAL component

## RPi OS and boot configuration

Configuration performed to the RPi 4B,
per the requirements of the HERCCULES mission.
This configuration depends on the peripherals connected to the RPi,
and involves the setup of the I2C, UART, SPI contollers
and other OS services.

### I2C Bus


### Linux Serial Console

Since our GPS is connected to the primary serial line port,
we need to disable the Linux serial console.
To do this, the kernel command line parameters defined in `/boot/cmdline.txt`
must contain only `console=tty1`,
and the `console=serial0,115200` entry shall be removed.


### Bluetooth

We shall disable bluetooth in our experiment
to avoid disturbances and interferences with the HERCCULES system.
This is done by adding the `disable-bt` DTO (Device Tree Overlay)
in the `/boot/config.txt` file:

```
dtoverlay=disable-bt
```

This will disable the Bluettoth device
and makes the first PL011 (UART0) the primarty UART, instead of mini UART.

> Note that the `/dev/serial0` Linux device will always point to the primary UART.
> In this case PL011 (UART0) is the primary UART and it's represented as `/dev/ttyS0`.

In addition,
we must run the `sudo systemctl disable hciuart` command
to disable the service that initialises and configures Bluetooth Modems connected by UART.


### SPI Bus


[1]: https://www.raspberrypi.com/documentation/computers/raspberry-pi.html
     "RPI hardware documentation"
